function start(varargin)
if isempty(varargin)
    addpath(genpath('./'));
else
    addpath(genpath(varargin{1}));
end