function [USP] = genPoisPattern(matrix3Dsize,USF,degPoisson,CL,ELLIPFLAG)
 
    %%% To modify
    matrix_size = matrix3Dsize;
    if numel(matrix_size) == 3
        sx = matrix_size(1);            % image dimensions
        sy = matrix_size(2);            % image dimensions
        sz = matrix_size(3);            % image dimensions
    elseif numel(matrix_size) == 2
        sx = 1;                         % image dimensions
        sy = matrix_size(1);            % image dimensions
        sz = matrix_size(2);            % image dimensions
    else
        error('NO IGEA!');
    end
    usf = USF;                              % overall undersampling factor
    calibration_lines = CL;                 % number of calibration lines
    acceleration_x = sqrt(usf);             % acceleration along x
    acceleration_y = sqrt(usf);             % acceleration along y
    ellipsoid_flag = ELLIPFLAG;             % ellipsoidal sampling 0,1
    degree_of_vd = degPoisson;              % variable density 0,1,2
    
    %%%% Dont touch
    
    
    USP = vdPoisMex(sy, sz, sy, sz, acceleration_x, acceleration_y, ...
                     calibration_lines , ellipsoid_flag, degree_of_vd);
    
    %filename = strcat('vdPois_',num2str(sy),'x',num2str(sz), ...
    %                  '_ncalib',num2str(calibration_lines),'_USF',num2str(usf),...
    %                  '_ellipflag',num2str(ellipsoid_flag),'_vddegree',num2str(degree_of_vd));
    %save([filename,'_QSMc'],'USP');
    figure, imshow(USP,[]);
    disp(prod(size(USP))/sum(USP(:)))

    USP = squeeze(repmat(permute(USP,[3,1,2]),[sx,1,1]));

end