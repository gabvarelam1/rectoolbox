classdef MatgitObj
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        currentTextFileDirectory
        fileName
        fID
    end
    
    methods
        function obj = MatgitObj(varargin)
            %MATGIT Construct an instance of this class
            %   Detailed explanation goes here
            if isempty(varargin)
                obj.currentTextFileDirectory = [];
                obj.fileName                 = [];
                disp('No file linked to obj');
            else
                obj.fileName = varargin{1};

                if length(varargin) > 1
                    obj.currentTextFileDirectory = varargin{2};
                else
                    obj.currentTextFileDirectory = pwd;
                end
                %obj.fID = fopen([obj.currentTextFileDirectory,'/',obj.fileName],'r');
                disp('MATGITObj was linked to a text file');
                obj.read()
                disp('closing file in 3 secs');
                pause(3);
                close;
            end
        end
        
        function [] = read(obj)
             f = figure('menu','none','toolbar','none');
            textfile = [obj.currentTextFileDirectory,'/',obj.fileName];
            fid = fopen(textfile);
            ph = uipanel(f,'Units','normalized','position',[0.05 0.05 0.9 0.9],'title',...
                strcat('text from '," ",textfile));
            lbh = uicontrol(ph,'style','listbox','Units','normalized','position',...
                [0 0 1 1],'FontSize',9);
            indic = 1;
            while 1
                 tline = fgetl(fid);
                 if ~ischar(tline), 
                     break
                 end
                 strings{indic}=tline; 
                 indic = indic + 1;
            end
            fclose(fid);
            set(lbh,'string',strings);
            set(lbh,'Value',1);
            set(lbh,'Selected','on');
        end

        function [] = write(obj,textmsg)

        end

        function [] = erase(obj,varargin)
        end

        function [] = exit(obj)
            fclose(obj.fID);
        end 
    end
end



