f = figure('menu','none','toolbar','none');
textfile = 'helloworld.txt';
fid = fopen(textfile);
ph = uipanel(f,'Units','normalized','position',[0.05 0.05 0.9 0.9],'title',...
    strcat('text from '," ",textfile));
lbh = uicontrol(ph,'style','listbox','Units','normalized','position',...
    [0 0 1 1],'FontSize',9);
indic = 1;
while 1
     tline = fgetl(fid);
     if ~ischar(tline), 
         break
     end
     strings{indic}=tline; 
     indic = indic + 1;
end
fclose(fid);
set(lbh,'string',strings);
set(lbh,'Value',1);
set(lbh,'Selected','on');