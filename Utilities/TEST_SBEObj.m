clear all; close all; clc;
%% TEST: 1:

s = SBEObj();
s.reconToolboxPath
s.dataPath
s.resultsPath

%% TEST 2:

recontoolbox = '/Users/gabrielvarela/Documents/MATLAB/myToolboxes/ReconToolbox';
datapath = '/Users/gabrielvarela/Documents/MATLAB/House';
respath = '/Users/gabrielvarela/Documents/MATLAB/Village';

s = SBEObj(recontoolbox,datapath,respath);
s.reconToolboxPath
s.dataPath
s.resultsPath

%% TEST 3: 

s = SBEObj();
s.reconToolboxPath
s.dataPath
s.resultsPath

s.setResultsPath(respath);
s.setDataPath(respath);

s.reconToolboxPath
s.dataPath
s.resultsPath