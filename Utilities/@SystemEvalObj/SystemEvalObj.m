classdef SystemEvalObj < handle
    %SYSTEMEVAL is a class to evaluate the hardware of the system in order
    %to allow other class to fully take advantage of resources. Essentially
    %pseaking, GPU and CPU parallel computing.

    % SYSTEMEVAL IS SILENTLY INCORPORATED IN CLASSES THAT MIGHT BENEFIT
    % FROM IT.

    % TODO: 
    %       1. Compute/Detect MEMORY LIMITATIONS
    %       2. ...
    
    properties (Access = protected)
        OS              = [];
        GPUFlag         = [];
        parallelFlag    = [];
        parforArg       = [];
        bk              = [];
    end
    properties (Access = public, Constant)
        bksothers   = '/';
        bkswin      = '\';
    end
    % Think About this ...
    % @OrganizerObj    : to save reconstructions
    % @FormatObj       : to save data converted to .mat
    % @
    
    methods (Access = protected, Sealed = true)
        function obj = SystemEvalObj()
            %SYSTEMEVAL Constructor

            % PLEASE MODIFY HERE TO ADD ADDITIONAL OPERATIVE SYSTEMS
            % check operative system for backlashes
            if ismac
                obj.OS = 'MAC';
                obj.bk = obj.bksothers;
            elseif isunix
                obj.OS = 'UNIX';
                obj.bk = obj.bksothers;
            elseif ispc
                obj.OS = 'PC';
                obj.bk = obj.bkswin;
            else
                error('What Operative System is this one?...');
            end
            obj.GPUFlag = obj.checkGPU();
            obj.parallelFlag = obj.checkParallel();
            obj.setParForArg();
        end

        function [res] = printOS(obj)
            res = obj.OS;
        end

        function [res] = gpuFlag(obj)
            res = obj.GPUFlag;
        end

        function [res] = parFlag(obj)
            res = obj.parallelFlag;
        end

    end
    methods (Access = public)

        function printSystem(obj)
            disp(['System OS:',             num2str(obj.printOS())]);
            disp(['System GPU flag:',       num2str(obj.gpuFlag())]);
            disp(['System Parallel flag:',  num2str(obj.parFlag())]);
            disp(['System number of workers:', num2str(obj.parforArg)]);
        end

        %%% These functions are useful only for testing purposes 
        %%% (i.e, testing improvement from GPU and CPU computing).
        %%% Otherwise, it does not make sense to use them.

        function switchGPU(obj)
            if obj.checkGPU()
                obj.GPUFlag = xor(obj.GPUFlag,1);
                obj.printSystem();
            else
                warning('No GPUs detected for update');
            end
        end

        function switchParallel(obj)
            if obj.checkParallel()
                obj.parallelFlag = xor(obj.parallelFlag,1);
                obj.printSystem();
                obj.setParForArg();
            else
                warning('No parallel computing toolbox detected for update');
            end
        end

        function device = selectGPU(obj,gpuID)
            % use a specific GPU
            if obj.GPUFlag
                try
                    device = gpuDevice(gpuID);
                catch
                    error('GPU unavailable');
                end
            else
                warning('no pgus detected');
            end
        end

    end

    methods (Access = private, Sealed = true)
        
        function flag = checkGPU(~)
            if ~isempty(ver('parallel'))
                if gpuDeviceCount > 1
                    flag = 1;
                else
                    flag = 0;
                end
            else
                flag = 0;
            end
        end

        function [flag] = checkParallel(~)
            if ~isempty(ver('parallel'))
                flag = 1;
            else
                flag = 0;
            end
        end

        function setParForArg(obj)
            if obj.parallelFlag
                obj.parforArg = Inf; % all available workers
            else
                % run serial
                obj.parforArg = 0;
            end
        end
    end
end

