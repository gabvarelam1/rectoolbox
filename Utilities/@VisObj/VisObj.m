classdef VisObj
    %VISOBJ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        m = @(im) abs(im);
        p = @(im) angle(im);
    end
    
    methods
        function obj = VisObj()
        end
        
        function [resm,resp] = mtimes(obj,inputArg,varargin)
            ss = size(inputArg);
            mid = ceil((ss+1)/2);

            sqx = @(im) squeeze(im(mid(1),:,:));
            sqy = @(im) squeeze(im(:,mid(2),:));
            sqz = @(im) squeeze(im(:,:,mid(3)));

            switch length(mid)
                case 1

                case 2
                    resm = obj.m(inputArg);
                    resp = obj.p(inputArg);
                    figure, imshow([resm],[]);
                    figure, imshow([resp],[-pi,pi]);
                case 3
                    maxSize = max(ss);
                    toPad = maxSize - ss;

                    zpad = padarray(inputArg,[toPad/2]);

                    resm = obj.m(zpad);
                    resp = obj.p(zpad);

                    figure, imshow([sqx(resm),sqy(resm),sqz(resm)],[]);
                    figure, imshow([sqx(resp),sqy(resp),sqz(resp)],[-pi,pi])
                case 4
                    maxSize = max(ss(1:3));
                    toPad = [maxSize - ss(1:3),0];

                    zpad = padarray(inputArg,[toPad/2]);

                    resm = obj.m(zpad);
                    resp = obj.p(zpad);

                    for i = 1 : ss(4)
                        sqx = @(im) squeeze(im(mid(1),:,:,i));
                        sqy = @(im) squeeze(im(:,mid(2),:,i));
                        sqz = @(im) squeeze(im(:,:,mid(3),i));
    
                        figure, imshow([sqx(resm),sqy(resm),sqz(resm)],[]);
                        figure, imshow([sqx(resp),sqy(resp),sqz(resp)],[-pi,pi])
                    end
                otherwise
            end

            
        end
    end
end

