classdef TODO
    %TODO is a superclass that writes down ideas for changes and improvements
    % for an specific subclass. Completely dedicated to text files.
    %   Detailed explanation goes here
    
    properties
        notebook
    end
    
    methods
        function obj = TODO(inputArg1,inputArg2)
            %TODO Construct an instance of this class
            %   Detailed explanation goes here
            obj.notebook = inputArg1 + inputArg2;
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.notebook + inputArg;
        end
        function outputArg = commit(obj)
        end
        function outputArg = delete(obj,string_)
        end
        function outputArg = addLine(obj)
        end
        function outputArg = removeLine(obj)
        end
        function outputArg = lookDate(obj)
        end

    end
end

