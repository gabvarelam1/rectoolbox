classdef SBEObj < SystemEvalObj & handle
    %SBEOBJ stands for stupid but effective class. This superclass contains methods
    % that appear to be necessary more often than not.
    properties (Access = public)
        reconToolboxPath    = '/Users/gabrielvarela/Documents/MATLAB/myToolboxes/ReconToolbox/';
        %%% common properties only
        printFlag           = 0;
        saveFlag            = 0;
        dataPath            = [];
        resultsPath         = [];
    end


    properties (Constant)
        
    end
    
    methods (Access = public)
        function obj = SBEObj(varargin)
            % Constructor
            if isempty(varargin)
                if obj.checkdir(obj.reconToolboxPath)
                    obj.dataPath = strcat(obj.reconToolboxPath,'Data');
                    obj.checkAndcreateDir(obj.dataPath);
                    obj.resultsPath = strcat(obj.reconToolboxPath,'Results');
                    obj.checkAndcreateDir(obj.resultsPath);
                else
                    obj.reconToolboxPath    = [];
                    obj.dataPath            = [];
                    obj.resultsPath         = [];
                end
            else
                switch length(varargin{1})
                    case 1
                        obj.checkdir(varargin{1},1);
                        obj.reconToolboxPath = varargin{1};
                        obj.dataPath = strcat(obj.reconToolboxPath,'Data');
                        obj.checkAndcreateDir(obj.dataPath);
                        obj.resultsPath = strcat(obj.reconToolboxPath,'Results');
                        obj.checkAndcreateDir(obj.resultsPath);
                    case 2
                        obj.checkdir(varargin{1},1);
                        obj.reconToolboxPath = varargin{1};
                        obj.dataPath = varargin{2};
                        obj.checkAndcreateDir(obj.dataPath);
                        obj.resultsPath = strcat(obj.reconToolboxPath,'Results');
                        obj.checkAndcreateDir(obj.resultsPath);
                    case 3
                        obj.checkdir(varargin{1},1);
                        obj.reconToolboxPath = varargin{1};
                        obj.dataPath = varargin{2};
                        obj.checkAndcreateDir(obj.dataPath);
                        obj.resultsPath = varargin{3};
                        obj.checkAndcreateDir(obj.resultsPath);
                    otherwise
                        warning('working with the first 3 parameters only');
                end
            end
        end

        function firstPos = findFirstChar(~,path_,character_)
            firstPos = strfind(path_,character_);
            firstPos = firstPos(1);
        end

        function lastPos = findLastChar(~,path_,character_)
            lastPos = strfind(path_,character_);
            lastPos = lastPos(end);
        end

        function lastPos1 = findLast1(~,data)
            % assumes a vector
            lastPos1 = find(data(:) == 1, 1 , 'last');
        end

        function lastPos1 = findLastDiff1(~,data)
            % assumes a vector
            lastPos1 = find(data(:) ~= 1, 1 , 'last');
        end
        function string_ = genRepString(~,characters,ntimes)
            b = characters;
            string_ = b;
            for i = 2 : ntimes
                string_ = strcat(string_ , b);
            end
            string_(end) = [];
        end
        function flags = isuppercase(~,string_)
            flags = isstrprop(string_,'upper');
        end
        function flags = islowercase(~,string_)
            flags = isstrprop(string_,'lower');
        end
        function flags = istransp(~,string_)
            pos = strfind(string_,"'");
            flags = zeros(size(string_));
            for i = 1 : length(pos)
                flags(pos(i)) = 1;
            end
        end

        function flags = isEven(~,nums)
            flags = (mod(nums,2) == 0); 
        end
        function datapad = zeropad(~,data,paddingVector)
            datapad = padarray(data,paddingVector);
        end
        function datapad = zeropad2Even(~,data,dims2makeEven)
            % toolboxes like Bart fail when dataSize is odd. Make it even
            % by simply adding a set of zeroes in the desired dimension (up to 3D).

            if length(dims2makeEven) > 3
                error('spatial dimensions only');
            else
                sz = size(data);
                datapad = padarray(data,[dims2makeEven,zeros(1,length(sz)-3)],'post');
            end
        end
        function flag = isdec(~,num)
            flag = 0; if mod(num,1) ~= 0; flag = 1; end
        end
        function flag = geq(~,num,ref)
            % greater or equal to
            flag = 0; if num >= ref; flag = 1; end
        end
        function flag = inworkspace(~,nameVar)
            flag = 0;
            if exist(nameVar,'var') flag = 1; end
        end
        function string_ = vec2str(~,vec_)
            string_ = '[';
            for j = 1 : length(vec_)
                string_ = strcat(string_,num2str(vec_(j)),',');
            end
            string_(end) = [];
            string_ = strcat(string_,']');
        end
    
        function cellList = getFields(~,struct_)
            cellList = fieldnames(struct_);
        end

        function newCellList = removeFromList(~,list,word)
            flags = strcmp(list,word);
            n = length(list)-sum(flags);
            newCellList = cell(n,1);
            cc = 1;
            for j = 1 : length(list)
                if flags(j) == 0
                    newCellList{cc} = list{j};
                    cc = cc+1;
                end
            end
        end

        function vec_ = repeatNum(~, number_ ,numReps)
            vec_ = ones(1,numReps) * number_;
        end

        function newString = replaceInString(~,string_,find_,replacewith_)
            initpos = strfind(string_ , find_);
            endpos = length(find_);
            if initpos == 1
                newString = strcat(replacewith_,string_(endpos+1 : end));
            elseif initpos > 1
                newString = strcat(string_(1:initpos-1),replacewith_,string_(initpos+endpos:end));
            else
                % need to think about more possible cases
            end

        end
        function strDims = createStringDimensions(~,reconDims)
            strDims = '';
            for i = 1 : length(reconDims)
                strDims = strcat(strDims,':,');
            end
            strDims(end) = [];
        end

        function res = toSingle(~,data)
            if isnumeric(data)
                res = single(data);
            else
                error('Expected numeric array');
            end
        end

        function res = sizeData(~,data)
            res = size(data);
        end

        function res = numDimensions(~,data)
            res = numel(size(data));
        end

    end
    methods (Access = protected)


        function flag = checkdir(~,dirc,varargin)
            if isempty(varargin)
                % light inspections
                if (exist(dirc,'dir') ~= 7)
                    flag = 0;
                else
                    flag = 1;
                end
            else
                % mandatory existence
                assert(exist(dirc,'dir')==7,strcat('Directory:', dirc,'does not exist'));
                flag = 1;
            end
        end

        function checkAndcreateDir(obj,dirc)
             %warning('check backslash for operative system');
             lastbks = obj.findLastChar(dirc,obj.bk);
             preDir = dirc(1 : lastbks);

            if obj.checkdir(preDir,1)
                if ~obj.checkdir(dirc)
                    mkdir(dirc);
                end
            end 
        end

        function setDataPath(obj,dataPath)
            % dataPath must exist
            obj.checkdir(dataPath,1);
            obj.dataPath = dataPath;
        end
        function setResultsPath(obj, resPath)
            % if it does not exist, create it :) 
            % this can be very useful to create tmp folders
            obj.checkAndcreateDir(resPath);
            obj.resultsPath = resPath;
        end

        function setDataAndResultsPaths(obj,paths)
            assert(length(paths)==2,'setDataAndResultsPaths function accepts only two directories');
            obj.setDataPath(paths{1});
            obj.setResultsPath(paths{2});
        end
            
        function getProperties(obj,OrgStruct)
            p = properties(obj);
            
            for i = 1 : length(p)
               if isfield(OrgStruct,p{i})
                   eval(strcat('obj.',p{i},' = OrgStruct.',p{i},';'));
               else
                   %disp(['no definition for property ',p{i}]);
               end
            end
            if isfield(OrgStruct,'printFlag')
                if OrgStruct.printFlag == 1
                    obj.print()
                end
            end 
            
        end

        function getStructProperties(obj,Target,OrgStruct)
            p = properties(obj);
            disp(Target);
            
            for i = 1 : length(p)
               if isfield(OrgStruct,p{i})
                   eval(strcat('obj.',p{i},' = OrgStruct.',p{i},';'));
               else
                   %disp(['no definition for property ',p{i}]);
               end
            end
            if isfield(OrgStruct,'printFlag')
                if OrgStruct.printFlag == 1
                    obj.print()
                end
            end 
            
        end
        function print(obj)
            % print current information
            classType = class(obj);
            disp([classType,' Properties']);
            p = properties(obj);

            for i = 1 : length(p)
                eval(strcat('if ~isnumeric(obj.',p{i},') ',strcat(' disp(strcat("',p{i},': ", obj.',p{i},'));'),...
                     strcat(' else  disp(strcat("',p{i},': ", num2str(obj.',p{i},')));'), 'end' ));
                        % ' elseif iscell(obj.',p{i},') printCell(',p{i},');'
            end
        end
        
        %%% TODO
        function printCell(obj,cellString)
            disp('TODO print cell variables');
        end
        function outputArg = help(obj,Command)
        end
    end
end

