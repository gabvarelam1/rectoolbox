classdef OrganizerObj < handle & SBEObj & SystemEvalObj
    %ORGANIZER is a wrapper to control and perform user-defined
    %requirements to perform reconstructions. The user needs to understand
    %what is going on, and the used is in charge of providing the right information for the
    %ORGANIZER object to run the recon.

    % ORGANIZER prints information for visualization
    % ORGANIZER prints objects necessary for the reconstruction process
    % ORGANIZER creates the enviroment for a success reconstruction
    
    properties (Access = public)
        dataSource          = []; % Directory/Filename
        acquisitionStyle    = []; %{'Cartesian','Wave','NonCartesian'}
        waveFrom            = []; %{hybrid-space,k-space,wave-space};
        startFromDatFile    = []; % [0,1]
        reconStyle          = []; % {'CompressedSensing','LowRank','LORAKS','LowPlusSparse'}
        dataReconStyle      = []; %{'2D','3D','m2D','p3D'}
        solverStyle         = []; %{'CGNE','mFISTA','LQSR'} objects
        reconSolver         = []; 
        encodingModel       = []; %{SF_jkPF_iRMx}
        regularizerOpt      = []; %{'norm1','norm2','nuclear'};
        Psi                 = []; %{'TV','TGV','W'}
        preEncoding         = []; %{IdentityObj, {ConcatenateObj,parameters}}
        PsiProperties       = []; % #Wavelet Levels and Wavelet Name
        lambdaMethod        = []; %{'double k-means','k-means','rayleigh'};
        fourierDirections   = []; % 1 : end of dataSize
        spatialDims         = []; % spatial dimensions
        completeDims        = []; % size of ksp
        lowRankProps        = [];
        maxIterations       = 50;
        tolerance           = 1e-4;
        xIn                 = [];
        yIn                 = [];
        indices             = [];
        concatenateRestDims = [];
        kspaceFilter        = [];
    end
    properties (Access = public, Constant)
        varSpecialList = {'S','P','C'}; % Operators that change
                                        % depending on the recon
                                        % problem.
    end
    
    methods
        function obj = OrganizerObj(OrgStruct)
            obj@SBEObj();         % stupid but effective methods
                                  % parfor, GPU and OS evaluation
                                  
            obj.getProperties(OrgStruct);
        end
        function [stInput] = prep(obj,varargin)
            
            disp('preparing data and encoding model for reconstruction');
            % TODO: Start from mat file or start from dat file
            if isempty(varargin)
                warning('TODO');
                data = load(obj.dataSource);
            else
                % clean memory
                obj.reconSolver     = [];
                obj.indices         = [];
                

                % varargin -> y , {A_1}, {A_2}, {A_n}
                disp(['Encoding Model:' , obj.encodingModel]);
                A = EncodingModelObj(obj.encodingModel,obj.acquisitionStyle);

                % order -> according to the encoding model (ksp, coils,
                % mask)
                data = struct;
                data.ksp = obj.toSingle(varargin{1});
                for cc = 2 : length(varargin)
                    % TODO: attach resources to the structure data
                    A.evalComponent(cc-1,isempty(varargin{cc}))
                    eval(strcat('data.',A.encodingComponents{cc-1},'=obj.toSingle(varargin{cc});'));
                end
                %data.coils = obj.toSingle(varargin{2});
                %data.mask = obj.toSingle(varargin{3});
            end
            
            disp('data');
            disp(data);

            listFromData = obj.getFields(data);
            listFromData = obj.removeFromList(listFromData,'ksp');

            %'ideal behavior'
            % data.ksp              % [x,y,z,C,e,t] ... etc
            % data.coils            % [x,y,z,C]   
            % data.mask             % [x,y,z]

            % analysis is done based on coils
            % in this manner it is easy to identify:
            % spatial volume
            % spatial volume assumes readout, phase, partition encoding
            %                directions unmodified.
            % # coils
            % the rest
            matrixSize = size(data.ksp);
            obj.completeDims = matrixSize;
            sizefromC = size(data.C);
            obj.spatialDims = sizefromC(1:end-1); obj.spatialDims = obj.spatialDims(obj.spatialDims ~= 1);
            
            posCoilsDims = numel(size(data.C));
            restDims = numel(size(data.ksp)) - posCoilsDims;
            if restDims == 0 
                restDims = 1;
            elseif restDims == 1
                restDims = matrixSize(posCoilsDims+1);
            elseif restDims == 2
                restDims = prod(matrixSize(posCoilsDims+1:end));
            else
                extraDims = matrixSize(posCoilsDims+1:end);
                % scales by additional dimensions
                kk = fliplr(extraDims .* [1,extraDims(1:end-2),1]);
                kk(end) = [];
                while length(kk) ~= 1
                    kk(end-1) = kk(end)*kk(end-1);
                    kk(end) = [];
                end

                restDims = kk(1);
            end
            data.ksp = reshape(data.ksp, [ matrixSize(1 : length(sizefromC)) , restDims]);

            % Need to think this
            % change slicer to obj.acquisitionStyle
            stry = obj.createStringDimensions(1:posCoilsDims);
            generalKspString = strcat('kspSlicer = @(rd_,pd_) data.ksp(',stry, ',rd_);');
            generalVarString = strcat('varSlicer = @(var_,rd_,pd_) var_(',stry, ',rd_);');
%             switch obj.reconStyle
%                 case 'LeastSquares'
%                     eval(strcat('slicer = @(i,j) sq(data.ksp(',stry, ', i),j);'));
%                 case 'CompressedSensing'
%                     eval(strcat('slicer = @(i,j) sq(data.ksp(',stry, ', i),j);'));
%                 case 'LowRank'
%                     eval(strcat('slicer = @(i,j) sq(data.ksp(',stry, ', :),j);'));
%                 otherwise
%             end 

            % Detect Acceleration Rate for the 3 spatial dimensions
            % Rx, Ry , Rz (normally Rx == 1)

            % TODO: develope creative ideas to decipher variable sizes
            % without been picky.

            % THIS IS NOT ENTIRELY CORRECT AS I should change according
            % reconstruction parameters. Maybe completeY is more correct.
            % Alternatively, present a function to re-combine y-split.              
            
            % Solver GENERATOR
            % identifies the problem and generates as many reconSolvers as
            % necessary.
            %%% NEW : ANALYZES ACQUISITIONSTYLE AND DATARECONSTYLE
            % reason: createSolver considers RECONSTYLE

            
            stInput = struct;
                                   
            switch obj.acquisitionStyle
                case 'Cartesian'
                    switch obj.dataReconStyle
                        case '1D'
                        case '2D'
                            parallelDims = 1;
                            if obj.concatenateRestDims
                                parallelDims = 1;
                                tempr = restDims;
                                restDims = 1;
                                specKspString = obj.replaceInString(generalKspString,'rd_)',':)');
                                specVarString = obj.replaceInString(generalVarString,'rd_)',':)');
                                spatialDimsTemp = [obj.spatialDims(1:2),1,tempr];
                            else
                                tempr = restDims;
                                specKspString = obj.replaceInString(generalKspString,'rd_)','rd_)');
                                specKspString = obj.replaceInString(specKspString,'data','squeeze(data');
                                specKspString = obj.replaceInString(specKspString,':,rd_)',':,rd_))');
                                specVarString = obj.replaceInString(generalVarString,'rd_)',':)');
                                spatialDimsTemp = [obj.spatialDims(1:2)];
                            end
                            eval(specKspString); % pd does nothing
                            eval(specVarString);
                            vecOfSpatialDims = 1 : 2;
                            obj.indices = cell(restDims,parallelDims);
                            for rd = 1 : restDims
                                for pd = 1 : parallelDims
                                    %obj.indices{i,j} = []
                                    tempst = struct;
                                    for opcounter = 1 : length(listFromData)
                                        eval(obj.genTempStr(listFromData{opcounter},vecOfSpatialDims));
                                    end
                                    tempst.ksp = kspSlicer(rd,pd); 
                                    % disp(pd);
                                    obj.reconSolver{rd,pd} = ...
                                    obj.createSolver(spatialDimsTemp, ...
                                                     tempst);
                                end
                            end

                        case 'm2D'
                        case '3D'
                            parallelDims = 1;
                            obj.reconSolver = cell(restDims,parallelDims);

                            eval(generalKspString); % pd does nothing
                            specVarString = obj.replaceInString(generalVarString,',rd_)',')');
                            eval(specVarString);
                            vecOfSpatialDims = 1 : 3;
                            obj.indices = cell(restDims,parallelDims);
                            for rd = 1 : restDims
                                for pd = 1 : parallelDims
                                    %obj.indices{i,j} = []
                                    tempst = struct;
                                    for opcounter = 1 : length(listFromData)
                                        eval(obj.genTempStr(listFromData{opcounter},vecOfSpatialDims));
                                    end
                                    tempst.ksp = kspSlicer(rd,pd); 
                                    % disp(pd);
                                    obj.reconSolver{rd,pd} = ...
                                    obj.createSolver(obj.spatialDims(1:end), ...
                                                     tempst);
                                end
                            end

                        case 'p3D'
                            % parallel 3D along the readout direction 1 ('x')
                            parallelDims = matrixSize(1);
                            Fi = FourierObj([1]);
                            data.ksp = Fi'*data.ksp;

                            obj.reconSolver = cell(restDims,parallelDims);

                            % intelligent squeeze ;) 
                            specKspString = obj.replaceInString(generalKspString,'data','squeeze(data');
                            specKspString = obj.replaceInString(specKspString,'(:,','(pd_,');
                            specKspString = obj.replaceInString(specKspString,'rd_)','rd_))');

                            specVarString = obj.replaceInString(generalVarString,'var_(','squeeze(var_(');
                            specVarString = obj.replaceInString(specVarString,'(:,','(pd_,');
                            specVarString = obj.replaceInString(specVarString,'rd_)',':))');

                            eval(specKspString);
                            eval(specVarString);
                            vecOfSpatialDims = 1 : 2;
                            obj.indices = cell(restDims,parallelDims);
                            for rd = 1 : restDims
                                for pd = 1 : parallelDims
                                    %obj.indices{i,j} = []
                                    tempst = struct;
                                    for opcounter = 1 : length(listFromData)
                                        eval(obj.genTempStr(listFromData{opcounter},vecOfSpatialDims));
                                    end
                                    tempst.ksp = kspSlicer(rd,pd); 
                                    obj.reconSolver{rd,pd} = ...
                                    obj.createSolver(obj.spatialDims(2:end), ...
                                                     tempst);
                                end
                            end
                        otherwise
                    end
                case 'Wave'
                    % here we assume that ksp means Hybrid k-space & image
                    % space. Unless the command is 3D, no p3D available.

                    wavey = strcat('j_:swy:obj.spatialDims(2)'); % 1D
                    wavez = strcat('k_:swz:obj.spatialDims(3)'); % 1D and 2D
                    if sum(strcmp(obj.dataReconStyle,{'1D','2D'}))
                        if ~isfield(data,'Rpi')
                            error('Expected RepmatOperator for Wave Encoding');
                        end
                        swy = obj.spatialDims(2) / data.Rpi(2);
                        swz = obj.spatialDims(3) / data.Rpi(3);
                    else
                        % 3D
                        wavey = strcat('j_:obj.spatialDims(2)'); % 1D
                        wavez = strcat('k_:obj.spatialDims(3)'); % 1D and 2D
                        flag3D = []; %'Wave-CAIPI', 'CS-Wave', 'CS-CAIPI'?
                        if strcmp(obj.reconStyle,'LeastSquares')
                            if ~isfield(data,'Rpi')
                                error('Expected RepmatOperator for Wave Encoding');
                            end
                            swy = obj.spatialDims(2) / data.Rpi(2);
                            swz = obj.spatialDims(3) / data.Rpi(3);
                            flag3D = 'Wave-CAIPI';
                        elseif strcmp(obj.reconStyle,'CompressedSensing')
                            flagS = isfield(data,'S');
                            flagRpi = isfield(data,'Rpi');
                            if or( and(flagS,flagRpi) , or(flagS,flagRpi)==0 )
                                error('is either S or Rpi Operators in the encoding model...sorry :( [snif]');
                            else
                                if flagS
                                    warning('Assuming Wave 3D reconstruction from 3D k-space');
                                    flag3D = 'CS-Wave';
                                    swy = 1;
                                    swz = 1;
                                end
                                if flagRpi
                                    warning('Assuming Wave 3D reconstruction from 3D Wave Image');
                                    flag3D = 'CS-CAIPI';
                                    swy = obj.spatialDims(2) / data.Rpi(2);
                                    swz = obj.spatialDims(3) / data.Rpi(3);
                                end
                            end
                        else
                            disp('What kind of Dark magic is this?... :o');
                            error('3D Wave not prepared for this weird setting...');
                        end

                    end

                    eval(strcat('indexgeneratorY = @(j_)',wavey,"';"));
                    eval(strcat('indexgeneratorZ = @(k_)',wavez,"';"));

                    if isempty(obj.waveFrom)
                        error(strcat('Please use obj.waveFrom to specify is eiher',...
                            'hybrid-space or k-space'));
                    end
                    switch obj.dataReconStyle
                        case '1D'
                            switch obj.waveFrom
                                case 'hybrid-space'
                                    data.ksp = data.ksp(:,1:swy,1:swz,:);
                                case 'k-space'
                                    warning('Working in Hybrid Space');
                                    Fjk = FourierObj([2,3]);
                                    data.ksp = Fjk'*(data.ksp);
                                    data.ksp = data.ksp(:,1:swy,1:swz,:);
                                case 'wave-space'
                                otherwise
                                    error('error in defintion of obj.waveFrom');
                            end


                            wave1DFlag = 1;

                            % swy*swz per recon problem
                            parallelDims = swy*swz;

                            % intelligent squeeze ;)
                            %generalKspString = strcat('kspSlicer = @(rd_,pd_) data.ksp(',stry, ',rd_);');
                            %generalVarString = strcat('varSlicer = @(var_,rd_,pd_) var_(',stry, ',rd_);');
                            specKspString = obj.replaceInString(generalKspString,'@(rd_,pd_)','@(rd_,j_,k_)');
                            specKspString = obj.replaceInString(specKspString,'ksp(:,:,:,:',strcat('ksp(:,j_,k_,:'));
                            specVarString = obj.replaceInString(generalVarString,',pd_',',j_,k_');
                            specVarString = obj.replaceInString(specVarString,'var_(:,:,:,:',strcat('var_(:,',wavey,',',wavez,',:'));

                            eval(specKspString);
                            eval(specVarString);
                            % coding 6-> 1:3 y 4:6 z
                            obj.indices = cell(restDims,parallelDims);
                            
                            vecOfSpatialDims = 1 : 4; % for Fourier Operator
                            obj.reconSolver = cell(restDims,parallelDims);
                            for rd = 1 : restDims
                                pd = 1;
                                for j = 1 : swy
                                    for k = 1 : swz
                                        
                                        [YY,ZZ] = ndgrid(indexgeneratorY(j),indexgeneratorZ(k)); 
                                        obj.indices{rd,pd} = [YY(:),ZZ(:)];

                                        tempst = struct;
                                        for opcounter = 1 : length(listFromData)
                                            eval(obj.genTempStr(listFromData{opcounter},vecOfSpatialDims,wave1DFlag));
                                        end
                                        tempst.ksp = kspSlicer(rd,j,k); 
                                        obj.reconSolver{rd,pd} = ...
                                        obj.createSolver([obj.spatialDims(1),data.Rpi(2:3)],tempst);
                                        pd = pd + 1;
                                    end
                                end
                            end
                            stInput.waveIndices = obj.indices;
                            

                        case '2D'
                            switch obj.waveFrom
                                case 'hybrid-space'
                                    data.ksp = data.ksp(:,1:swy,1:swz,:);
                                case 'k-space'
                                    warning('Working in Hybrid Space');
                                    Fjk = FourierObj([2,3]);
                                    data.ksp = Fjk'*(data.ksp);
                                    data.ksp = data.ksp(:,1:swy,1:swz,:);
                                case 'wave-space'
                                otherwise
                                    error('error in defintion of obj.waveFrom');
                            end
                            % swz slices per recon problem
                            parallelDims = swz;

                            % intelligent squeeze ;)
                            %generalKspString = strcat('kspSlicer = @(rd_,pd_) data.ksp(',stry, ',rd_);');
                            %generalVarString = strcat('varSlicer = @(var_,rd_,pd_) var_(',stry, ',rd_);');
                            specKspString = obj.replaceInString(generalKspString,'rd_,pd_','rd_,k_');
                            specKspString = obj.replaceInString(specKspString,'ksp(:,:,:,:',strcat('ksp(:,:,k_,:'));
                            specVarString = obj.replaceInString(generalVarString,'rd_,pd_','rd_,k_');
                            specVarString = obj.replaceInString(specVarString,'var_(:,:,:,:',strcat('var_(:,:,',wavez,',:'));

                            eval(specKspString);
                            eval(specVarString);

                            obj.indices = cell(restDims,parallelDims);
                            
                            vecOfSpatialDims = 1 : 3;
                            obj.reconSolver = cell(restDims,parallelDims);
                            for rd = 1 : restDims
                                pd = 1;
                                %for j = 1 : swy
                                    for k = 1 : swz
    
                                        obj.indices{rd,pd} = indexgeneratorZ(k);

                                        tempst = struct;
                                        for opcounter = 1 : length(listFromData)
                                            eval(obj.genTempStr(listFromData{opcounter},vecOfSpatialDims));
                                        end
                                        tempst.ksp = kspSlicer(rd,k); 
                                        obj.reconSolver{rd,pd} = ...
                                        obj.createSolver([obj.spatialDims(1:2),data.Rpi(3)],tempst);

                                        pd = pd + 1;
                                    end
                                %end
                            end
                            stInput.waveIndices = obj.indices;
                            
                        case 'm2D'
                            error('No Wave option with m2D');
                        case '3D'
                            switch flag3D
                                % in here we need to define parallel
                                % dimensions, encoding model and k-space
                                % separation.
                                case 'Wave-CAIPI'
                                    switch obj.waveFrom
                                        case 'hybrid-space'
                                            data.ksp = data.ksp(:,1:swy,1:swz,:);
                                        case 'k-space'
                                            warning('Working in Hybrid Space');
                                            Fjk = FourierObj([2,3]);
                                            data.ksp = Fjk'*(data.ksp);
                                            data.ksp = data.ksp(:,1:swy,1:swz,:);
                                        case 'wave-space'
                                        otherwise
                                            error('error in defintion of obj.waveFrom');
                                    end
                                case 'CS-CAIPI'
                                    switch obj.waveFrom
                                        case 'hybrid-space'
                                            data.ksp = data.ksp(:,1:swy,1:swz,:);
                                        case 'k-space'
                                            warning('Working in Hybrid Space');
                                            Fjk = FourierObj([2,3]);
                                            data.ksp = Fjk'*(data.ksp);
                                            data.ksp = data.ksp(:,1:swy,1:swz,:);
                                        case 'wave-space'
                                        otherwise
                                            error('error in defintion of obj.waveFrom');
                                    end
                                case 'CS-Wave'
                                    % do nothing to ksp
                                    warning('working in k-space');
                                    switch obj.waveFrom
                                        case 'hybrid-space'
                                    
                                        case 'k-space'
                                        
                                        case 'wave-space'
                                        otherwise
                                            error('error in defintion of obj.waveFrom');
                                    end
                            end
                                % return hybrid space data into k-space
                                parallelDims = 1;

                                % intelligent squeeze ;)
                                %generalKspString = strcat('kspSlicer = @(rd_,pd_) data.ksp(',stry, ',rd_);');
                                %generalVarString = strcat('varSlicer = @(var_,rd_,pd_) var_(',stry, ',rd_);');
                                %specKspString = obj.replaceInString(generalKspString,'rd_,pd_','rd_,k_');
                                %specKspString = obj.replaceInString(specKspString,'ksp(:,:,:,:',strcat('ksp(:,:,k_,:'));
                                %specVarString = obj.replaceInString(generalVarString,'rd_,pd_','rd_,k_');
                                %specVarString = obj.replaceInString(specVarString,'var_(:,:,:,:',strcat('var_(:,:,',wavez,',:'));
    
                                eval(generalKspString);
                                eval(generalVarString);
    
                                obj.indices = cell(restDims,parallelDims);
                                
                                vecOfSpatialDims = 1 : 3;
                                obj.reconSolver = cell(restDims,parallelDims);
                                for rd = 1 : restDims
                                    pd = 1;
                                    %for j = 1 : swy
                                        %for k = 1 : swz
        
                                            obj.indices{rd,pd} = indexgeneratorZ(pd);
    
                                            tempst = struct;
                                            for opcounter = 1 : length(listFromData)
                                                eval(obj.genTempStr(listFromData{opcounter},vecOfSpatialDims));
                                            end
                                            tempst.ksp = kspSlicer(rd,pd); % k is a dummy variable
                                            obj.reconSolver{rd,pd} = ...
                                            obj.createSolver([obj.spatialDims(1:3)],tempst);
    
                                            pd = pd + 1;
                                        %end
                                    %end
                                end
                                stInput.waveIndices = obj.indices;

                            
                        case 'p3D'
                            error('No Wave option with p3D');
                        otherwise
                    end
                case 'NonCartesian'
                    % For now I suggest to try the matMRI toolbox. 
                    switch obj.dataReconStyle
                        case '1D'
                        case '2D'
                        case 'm2D'
                        case '3D'
                        case 'p3D'
                        otherwise
                    end
                otherwise
            end



%             % OLD : ANALYZES RECONSTYLE - DATARECONSTYLE - ACQUISITIONSTYLE
%             % {'CompressedSensing','LowRank','LORAKS','LowPlusSparse'}
%             switch obj.reconStyle
%                 case 'LeastSquares'
%                     % if compressed sensing reconstruction
%                     % then, the reconstruction problem only tackles spatial
%                     % dimensions and reconstructs additional dimensions
%                     % independently.
% 
%                     % {'2D','3D','m2D','p3D'}
%                     % TODO: 1D - special CS-Wave case
%                     switch obj.dataReconStyle
%                         case '1D'
%                             switch obj.acquisitionStyle
%                                 case 'Cartesian'
%                                     error('for Wave-CAIPI only');
%                                case 'Wave'
                                    
% 
%                                 case 'NonCartesian'
%                                     error('for Wave-CAIPI only');
%                             end
% 
%                         case '2D'
%                             % Easiest case
%                             % TODO: function to confirm feasibility
%                             switch obj.acquisitionStyle
%                                 case 'Cartesian'
%                                     parallelDims = obj.spatialDims(3);
%                                     sq = @(var_,sx_) squeeze(var_(: , : ,sx_, :));
%                                     obj.reconSolver = cell(restDims,parallelDims);
%                                     for i = 1 : restDims
%                                         for j = 1 : parallelDims
% 
%                                             tempst = struct;
%                                             for opcounter = 1 : length(ls2)
%                                                 eval(strcat('tempst.',ls2{opcounter},'=sq(data.',ls2{opcounter},',j);'));
%                                             end
% 
%                                             obj.reconSolver{i} = ...
%                                                 obj.createSolver(obj.spatialDims(1:2), tempst);
%                                         end
%                                     end                                        
%                                 case 'Wave'
%                                     if ~isfield(data,'Rp')
%                                         error('Expected RepmatOperator for Wave-CAIPI');
%                                     end
%                                     parallelDims = prod(obj.spatialDims(3) ./ [data.Rp(3)]);
%                                     if parallelDims == 1 
%                                         sq = @(var_,sx_) squeeze(var_(: , : , :, :));
%                                     else
%                                         sq = @(var_,sx) squeeze(var_(: , : , sx : parallelDims : obj.spatialDims(3), :));
%                                     end
%                                     obj.reconSolver = cell(restDims,parallelDims);
%                                     for i = 1 : restDims
%                                         for j = 1 : parallelDims
% 
%                                             tempst = struct;
%                                             for opcounter = 1 : length(ls2)
%                                                 eval(strcat('tempst.',ls2{opcounter},'=sq(data.',ls2{opcounter},',j)'));
%                                             end
% 
%                                             obj.reconSolver{i} = ...
%                                                 obj.createSolver([obj.spatialDims(1:2),data.Rp(3)], tempst, []);
%                                         end
%                                     end 
%                                 case 'NonCartesian'
%                                     warning('TODO');
%                                 otherwise
%                                     error('GG surrender 5!');
%                             end
%                             
%                         case 'm2D'
%                             % TODO: how do you recognize multi-slice?
%                         case '3D'
%                             % Easy but memory expensive
%                             % TODO: function to confirm feasibility
%                             obj.reconSolver = cell(restDims,1);
%                             for i = 1 : restDims
%                             obj.reconSolver{i} = ...
%                                 obj.createSolver(obj.spatialDims, data);
%                             end
%                             
%                         case 'p3D'
%                             % parallel 3D along the readout direction 1 ('x')
%                             parallelDims = matrixSize(1);
% 
%                             % TODO: invert dimensions and leave
%                             % reconSolver{i,j} where i is too additional
%                             % dimensions and j is when splitting each of
%                             % those.
%                             obj.reconSolver = cell(restDims,parallelDims);
%                             % TODO: an intellingent squeeze
%                             sq = @(var_,sx_) squeeze(var_(sx_, : ,:, :));
%                             
%                             listFromData = obj.getFields(data);
%                             ls2 = obj.removeFromList(listFromData,'ksp');
%                             
% 
%                             for i = 1 : restDims
%                                 for sx = 1 : matrixSize(1)
%                                     % create a temporal data struct
%                                     tempst = struct;
%                                     for opcounter = 1 : length(ls2)
%                                         eval(strcat('tempst.',ls2{opcounter},'=sq(data.',ls2{opcounter},',sx)'));
%                                     end
% 
%                                     obj.reconSolver{i,sx} = ...
%                                     obj.createSolver(obj.spatialDims(2:end), ...
%                                                      tempst);
%                                 end
%                             end
%                     end
% %                 % I know that WaveCAIPI and CSWave must be member of
% %                 % least-squares and compressed sensing clusters, but I
% %                 % would try first to simply run them. Because 2D and 1D are
% %                 % optimized in hybrid space, the encoding model and the
% %                 % recon framework changes. However, that is not a good
% %                 % excuse.
% %                 case 'WaveCAIPI'
% %                     % least-squares parallelized by lines - slice groups -
% %                     % 3D
% %                     % Hybrid Wave Space is the encoding Model
% %                     % and Wave-CAIPI and CS-Wave
% %                     switch obj.dataReconStyle
% %                         case '1D'
% %                             SX = prod(matrixSize(2:3));
% %                             obj.reconSolver = cell(restDims,SX);
% % 
% %                             for i = 1 : restDims
% %                                 for sx = 1 : SX
% %                                     % TODO
% %                                     obj.reconSolver{i,sx} = ...
% %                                     obj.createSolver(obj.spatialDims(2:end), ...
% %                                                      sq(data.mask,sx), ...
% %                                                      sq(data.coils,sx));
% %                                 end
% %                             end
% % 
% %                         case '2D'
% %                             SX = prod(matrixSize(3));
% %                             obj.reconSolver = cell(restDims,SX);
% % 
% %                             for i = 1 : restDims
% %                                 for sx = 1 : SX
% %                                     % TODO
% %                                     obj.reconSolver{i,sx} = ...
% %                                     obj.createSolver(obj.spatialDims(2:end), ...
% %                                                      sq(data.mask,sx), ...
% %                                                      sq(data.coils,sx));
% %                                 end
% %                             end
% %                         case '3D'
% %                             SX = 1;
% %                             obj.reconSolver = cell(restDims,SX);
% % 
% %                             for i = 1 : restDims
% %                                 for sx = 1 : SX
% %                                     % TODO
% %                                     obj.reconSolver{i,sx} = ...
% %                                     obj.createSolver(obj.spatialDims(2:end), ...
% %                                                      sq(data.mask,sx), ...
% %                                                      sq(data.coils,sx));
% %                                 end
% %                             end
% %                     end
% %                 case 'CSWave'
% %                     % Same as WaveCAIPI but includes regularizer
% %                     switch obj.dataReconStyle
% %                         case '1D'
% %                             SX = prod(matrixSize(2:3));
% %                             obj.reconSolver = cell(restDims,SX);
% % 
% %                             for i = 1 : restDims
% %                                 for sx = 1 : SX
% %                                     % TODO
% %                                     obj.reconSolver{i,sx} = ...
% %                                     obj.createSolver(obj.spatialDims(2:end), ...
% %                                                      sq(data.mask,sx), ...
% %                                                      sq(data.coils,sx));
% %                                 end
% %                             end
% % 
% %                         case '2D'
% %                             SX = prod(matrixSize(3));
% %                             obj.reconSolver = cell(restDims,SX);
% % 
% %                             for i = 1 : restDims
% %                                 for sx = 1 : SX
% %                                     % TODO
% %                                     obj.reconSolver{i,sx} = ...
% %                                     obj.createSolver(obj.spatialDims(2:end), ...
% %                                                      sq(data.mask,sx), ...
% %                                                      sq(data.coils,sx));
% %                                 end
% %                             end
% %                         case '3D'
% %                             SX = 1;
% %                             obj.reconSolver = cell(restDims,SX);
% % 
% %                             for i = 1 : restDims
% %                                 for sx = 1 : SX
% %                                     % TODO
% %                                     obj.reconSolver{i,sx} = ...
% %                                     obj.createSolver(obj.spatialDims(2:end), ...
% %                                                      sq(data.mask,sx), ...
% %                                                      sq(data.coils,sx));
% %                                 end
% %                             end
% %                     end
%                 case 'CompressedSensing'
%                     % if compressed sensing reconstruction
%                     % then, the reconstruction problem only tackles spatial
%                     % dimensions and reconstructs additional dimensions
%                     % independently.
% 
%                     % {'2D','3D','m2D','p3D'}
%                     % TODO: 1D - special CS-Wave case
%                     switch obj.dataReconStyle
%                         case '2D'
%                             % Easiest case
%                             % TODO: function to confirm feasibility
%                             obj.reconSolver = cell(restDims,1);
%                             for i = 1 : restDims
%                             obj.reconSolver{i} = ...
%                                 obj.createSolver(obj.spatialDims, data);
%                             end
%                         case 'm2D'
%                             % TODO: how do you recognize multi-slice?
%                         case '3D'
%                             % Easy but memory expensive
%                             % TODO: function to confirm feasibility
%                             obj.reconSolver = cell(restDims,1);
%                             for i = 1 : restDims
%                             obj.reconSolver{i} = ...
%                                 obj.createSolver(obj.spatialDims, data);
%                             end
%                             
%                         case 'p3D'
%                             % parallel 3D along the readout direction 1 ('x')
%                             parallelDims = matrixSize(1);
% 
%                             % TODO: invert dimensions and leave
%                             % reconSolver{i,j} where i is too additional
%                             % dimensions and j is when splitting each of
%                             % those.
%                             obj.reconSolver = cell(restDims,parallelDims);
%                             % TODO: an intellingent squeeze
%                             sq = @(var_,sx_) squeeze(var_(sx_, : ,:, :));
%                             
%                             listFromData = obj.getFields(data);
%                             ls2 = obj.removeFromList(listFromData,'ksp');
%                             opParams = cell(length(ls2),1);
%                             for i = 1 : restDims
%                                 for sx = 1 : matrixSize(1)
%                                     tempst = struct;
%                                     for opcounter = 1 : length(ls2)
%                                         if ~isempty(eval(strcat('data.',ls2{opcounter})))
%                                             eval(strcat('tempst.',ls2{opcounter},'=sq(data.',ls2{opcounter},',sx);'));
%                                         else
%                                             eval(strcat('tempst.',ls2{opcounter},'= [];'));
%                                             switch ls2{opcounter}
%                                                 case 'F'
%                                                     opParams{opcounter} = 1:length(obj.spatialDims(2:end));
%                                             end
% 
%                                         end
%                                     end
%                                     obj.reconSolver{i,sx} = ...
%                                     obj.createSolver(obj.spatialDims(2:end), ...
%                                                      tempst,opParams);
%                                 end
%                             end
%                     % end CompressedSensing
%                     end
%                 case 'LowRank'
%                     switch obj.dataReconStyle
%                         case '2D'
%                             % [x,y,1,n]
%                             obj.reconSolver{1} = ...
%                             obj.createSolver(obj.spatialDims, ...
%                                                  data, restDims);
%                         case 'm2D'
%                             % dummy dimension
%                         case '3D'
%                             % [x,y,z,n]
%                             %matrixSize = 208,208,208,32,5
%                             %posCoilsDims = 4
%                             %restDims = 5
%                             
%                         case 'p3D'
%                             % [1,y,z,n]
%                             %matrixSize = 208,208,208,32,5
%                             %posCoilsDims = 4
%                             %restDims = 5
%                             % parallel 3D along the readout direction 1 ('x')
%                             % obj.spatialDims
%                             % data
%                             parallelDims = matrixSize(1);
% 
%                             % restDims becomes 1 
%                             % TODO: think when it is not the case
%                             tempM = restDims;
%                             restDims = 1;
%                             obj.reconSolver = cell(restDims,parallelDims);
%                             % TODO: an intellingent squeeze
%                             sq = @(var_,sx_) squeeze(var_(sx_, : , :, :, : , :));
%                             listFromData = obj.getFields(data);
%                             ls2 = obj.removeFromList(listFromData,'ksp');
%                             for i = 1 : restDims
%                                 for sx = 1 : matrixSize(1)
%                                     tempst = struct;
%                                     for opcounter = 1 : length(ls2)
%                                         eval(strcat('tempst.',ls2{opcounter},'=sq(data.',ls2{opcounter},',sx)'));
%                                     end
%                                     obj.reconSolver{i,sx} = ...
%                                     obj.createSolver([obj.spatialDims(2:end)], ...
%                                                      tempst,tempM);
%                                 end
%                             end
% 
% 
% 
%                         otherwise
%                             error('gg wp surrender 5');
%                     end
%                 otherwise
%                     %case 'LORAKS'
%                     %case 'LowPlusSparse'
%                     error('no implementation yet');
%             end

            % converge variables to present the input data...
            nProblemsExtraDims      = size(obj.reconSolver,1);
            nProblemsp3D            = size(obj.reconSolver,2);
            
            stInput.yTotal = cell(nProblemsExtraDims,nProblemsp3D);
            stInput.xTotal = cell(nProblemsExtraDims,nProblemsp3D);
            

            % This literally remains but we need to prepare ksp in advance
            % I also need a function to return y and x. 
            for i = 1 : nProblemsExtraDims
                for j = 1 : nProblemsp3D
                    % eval(strcat('y = sq(data.ksp(',stry, ', i),j);')); %
                    % alternative. must evaluate speed.
                    %y = slicer(i,j);
                    %x = obj.reconSolver{i,j}.At(y);
                    
                    stInput.yTotal{i,j} = obj.reconSolver{i,j}.gety();
                    stInput.xTotal{i,j} = obj.reconSolver{i,j}.getx();
    
                    obj.yIn{i,j} = obj.reconSolver{i,j}.gety();
                    obj.xIn{i,j} = obj.reconSolver{i,j}.getx();
                end
            end

        end

        function update(obj,varargin)
            % For now it only allows to modify the following
            % I expect the following: {'field',variable2change2}
            for i = 1 : 2 : length(varargin)
                switch varargin{i}
                    case 'sampling'
                    case 'reconStyle'
                    case 'dataReconStyle'
                    case 'kspace'
                    case 'coils'
                end
                    
            end
            % Sampling Mask
            % dataReconStyle from p3D to 3D
            

            % TODO: allow the following:
            % CoilMaps assumes that coil maps are already coil-compressed
            % reconStyle from CompressedSensing to LORAKS (and to the other modes?)


            obj.prep();
        end

        function stOutput = run(obj,varargin)
    
            nProblemsExtraDims  = size(obj.reconSolver,1);
            nProblemsp3D        = size(obj.reconSolver,2);
    
            
            if isempty(varargin)
                nProblems           = nProblemsExtraDims*nProblemsp3D;
            else
                % try to use this in a couple of slices to define settings 
                % and then run it for the entire recon problem
                updateFlag = [];
                newlambdaMethod = [];
                newtolerance = [];
                newMaxIterations = [];
                % user settings defined on the fly
                valid_argnames = {'index','lambda','tolerance','maxIterations'};
                [args_specified,args_location] = ismember(valid_argnames,varargin(1:2:end));
                if args_specified(1) == true
                    indexLoc = (args_location(1)*2-1)+1;
                    nProblems = length(varargin{indexLoc});
                end
                if args_specified(2) == true
                    newlambdaMethod = varargin{(args_location(2)*2-1)+1};
                    updateFlag = [updateFlag,1];
                end
                if args_specified(3) == true
                    newtolerance = varargin{(args_location(3)*2-1)+1};
                    updateFlag = [updateFlag,2];
                end
                if args_specified(4) == true
                    newMaxIterations = varargin{(args_location(4)*2-1)+1};
                    updateFlag = [updateFlag,3];
                end
                % Adjust
                if ~isempty(updateFlag)
                    disp('Updating...');
                    for i = 1 : length(varargin{indexLoc})
                        for j = 1 : length(updateFlag)
                            switch j
                                case 1
                                    % lambdaMethod
                                    obj.reconSolver{varargin{indexLoc}(i)}.regularizer.updateRegularizationMethod(newlambdaMethod);
                                case 2
                                    % tolerance
                                    obj.reconSolver{varargin{indexLoc}(i)}.setTolerance(newtolerance);    
                                case 3
                                    % maxIterations
                                    obj.reconSolver{varargin{indexLoc}(i)}.setMaxIterations(newMaxIterations);
                                otherwise
                                    %
                            end
                        end
                    end
                end
                
            end
     
%             if nProblems == 1
%                 disp(strcat('Runnng... ', num2str(nProblems), '  reconstruction(s)...'));
%                 [stOutput.reconstruction,stOutput.opt] = ...
%                     obj.reconSolver{varargin{1}}.run(obj.yIn{varargin{1}},obj.xIn{varargin{1}});
%                 
%             else
            if ~isempty(varargin)
                disp(strcat('Runnng... ', num2str(nProblems), '  reconstruction(s)...'));
                % TODO: to be more precise in the future...
                stOutput.reconstruction = cell(nProblems,1);
                stOutput.opt = cell(nProblems,1);
                for i = 1 : nProblems
                % This is intended for a couple of slices only.
                    [reconstruction,opt] = ...
                    obj.reconSolver{varargin{indexLoc}(i)}.run(obj.yIn{varargin{indexLoc}(i)},obj.xIn{varargin{indexLoc}(i)});
                    stOutput.reconstruction{i} = reconstruction;
                    stOutput.opt{i} = opt;
                    stOutput.indices{i} = obj.indices(varargin{indexLoc}(i));
                end
                %%% clean results
                if nProblems == 1
                    stOutput.reconstruction = stOutput.reconstruction{1};
                    stOutput.opt = stOutput.opt{1};
                else
                    opt = struct;
                    li = length(varargin{indexLoc});
                    opt.tolerance = stOutput.opt{1}.tolerance;
                    opt.lambdaDet = stOutput.opt{1}.lambdaDet;
                    
                    maxIters = length(stOutput.opt{1}.convergence);
                    opt.convergence = zeros(maxIters,li);
                    opt.fcost = zeros(maxIters,li);
                    opt.gcost = zeros(maxIters,li);
                    opt.alpha = zeros(length(stOutput.opt{1}.alpha),li);
                    opt.regw = zeros(length(stOutput.opt{1}.regw),li);
                    opt.iterations = zeros(3,li);

                    for i = 1 : li
                        opt.convergence(:,i) = stOutput.opt{i}.convergence(:);
                        opt.fcost(:,i) = stOutput.opt{i}.fcost(:);
                        opt.gcost(:,i) = stOutput.opt{i}.gcost(:);
                        opt.alpha(:,i) = stOutput.opt{i}.alpha(:);
                        opt.regw(:,i) = stOutput.opt{i}.regw(:);
                        opt.iterations(:,i) = stOutput.opt{i}.iterations(:);
                    end
                    stOutput.opt = opt;
                end
                clear reconstruction; clear opt;

            elseif nProblems == 1
                 disp(strcat('Runnng... ', num2str(nProblems), '  reconstruction(s)...'));
                 [stOutput.reconstruction,stOutput.opt] = ...
                     obj.reconSolver{1}.run(obj.yIn{1},obj.xIn{1});
                 
            else
                disp(strcat('Runnng... ', num2str(nProblems), '  reconstruction(s)...'));

                % be sure to close any previous interactive session
                if obj.parallelFlag
                    delete(gcp('nocreate'))
                    % create pool of workers with the current cluster settings
                    matlab_ncores = feature('numcores');
                    disp(strcat('Number of Workers:',num2str(matlab_ncores)));
                    parallel.defaultClusterProfile('local');
                    c = parcluster();
                    c.NumWorkers = matlab_ncores;
                    parpool(c,matlab_ncores);
                end

                if nProblemsp3D > nProblemsExtraDims
                    % parallelization is performed across slices/other
                    % dimensions
                    reconstruction = single(zeros([obj.spatialDims,nProblemsExtraDims]));
                    optSettings = cell(nProblemsp3D,1);
                        
                    for i = 1 : nProblemsExtraDims
                        % TODO: think an intelligent way for this...
    
                        containers      = cell(nProblemsp3D,1);
                        yin             = cell(nProblemsp3D,1);
                        xin             = cell(nProblemsp3D,1);
                        
                        rec             = cell(nProblemsp3D,1);
    
                        for j = 1 : nProblemsp3D
                             containers{j} =  obj.reconSolver{i,j};
                             yin{j} = obj.yIn{i,j};
                             xin{j} = obj.xIn{i,j};
                        end
    
                        switch obj.acquisitionStyle
                            case 'Cartesian'
                                parfor (j = 1:nProblemsp3D, obj.parforArg)
                                [reconstruction(j,:,:,i),optSettings{j}] = ... 
                                        containers{j}.run(yin{j},xin{j});
                                end
                            case 'Wave'
                                %for  j = 1 : nProblemsp3D % testing
                                
                                parfor (j = 1:nProblemsp3D, obj.parforArg)
                                    [rec{j},optSettings{j}] = containers{j}.run(yin{j},xin{j});
                                end
                                switch obj.dataReconStyle
                                    case '1D'
                                        for j = 1 : nProblemsp3D
                                            nL = length(obj.indices{i,j});
                                            
                                            rec{j} = reshape(rec{j},[obj.spatialDims(1)],[]);
                                            for reAdjYZ = 1:nL
                                                yz = obj.indices{i,j}(reAdjYZ,:);
                                                reconstruction(:,yz(1),yz(2),i) = rec{j}(:,reAdjYZ);     
                                            end
                                            
                                        end
                                    case '2D'
                                        for j = 1 : nProblemsp3D
                                            nL = length(obj.indices{i,j});
                                            %rec{j} = reshape(rec{j},[obj.spatialDims(1:2)],[]);
                                            for reAdjYZ = 1:nL
                                                yz = obj.indices{i,j}(reAdjYZ);
                                                reconstruction(:,:,yz,i) = rec{j}(:,:,reAdjYZ);     
                                            end
                                            
                                        end
                                    case '3D'
                                        % I guess that...
                                        reconstruction(:,:,:,i) = rec;
                                end
                            case 'NonCartesian'
                        end
                    end

                    % cleaning process
                    for j = 1 : nProblemsp3D
                        containers{j} = [];
                        xin{j} = [];
                        yin{j} = [];
                    end
                    clear containers; clear xin; clear yin;

                else
                    %%%% The same thing but the other way around
                    % parallelization is performed across additional
                    % dimensions
                    % nProblemsp3D < nProblemsExtraDims
                    reconstruction = single(zeros([obj.spatialDims,nProblemsExtraDims]));
                    optSettings = cell(nProblemsExtraDims,1);

                    for i = 1 : nProblemsp3D
                        % TODO: think an intelligent way for this...
    
                        containers      = cell(nProblemsExtraDims,1);
                        yin             = cell(nProblemsExtraDims,1);
                        xin             = cell(nProblemsExtraDims,1);
                        
                        rec             = cell(nProblemsExtraDims);
    
                        for j = 1 : nProblemsExtraDims
                             containers{j} =  obj.reconSolver{j,i};
                             yin{j} = obj.yIn{j,i};
                             xin{j} = obj.xIn{j,i};
                        end
    
                        switch obj.acquisitionStyle
                            case 'Cartesian'
                                switch obj.dataReconStyle
                                    case '2D'
                                        if nProblemsp3D == 1
                                            parfor (j = 1 : nProblemsExtraDims, obj.parforArg)
                                                [reconstruction(:,:,j),optSettings{j}] = ... 
                                                    containers{j}.run(yin{j},xin{j});
                                            end
                                        else
                                            parfor (j = 1 : nProblemsExtraDims, obj.parforArg)
                                                [reconstruction(i,:,:,j),optSettings{j}] = ... 
                                                    squeeze(containers{j}.run(yin{j},xin{j}));
                                            end
                                        end
                                    case '3D'
                                        if nProblemsp3D == 1
                                            parfor (j = 1 : nProblemsExtraDims, obj.parforArg)
                                            %for j = 1 : nProblemsExtraDims
                                            %for testing purpose keep the
                                            %commented line.
                                            [reconstruction(:,:,:,j),optSettings{j}] = ... 
                                                containers{j}.run(yin{j},xin{j});
                                            end
                                        else
                                            parfor (j = 1 : nProblemsExtraDims, obj.parforArg)
                                            [reconstruction(i,:,:,j),optSettings{j}] = ... 
                                                squeeze(containers{j}.run(yin{j},xin{j}));
                                            end                                           
                                        end
                                end
                            case 'Wave'
                                %for  j = 1 : nProblemsp3D % testing
                                parfor (j = 1 : nProblemsExtraDims, obj.parforArg)
                                    [rec{j},optSettings{j}] = containers{j}.run(yin{j},xin{j});
                                end
                                switch obj.dataReconStyle
                                    case '1D'
                                        for j = 1 : nProblemsExtraDims
                                            nL = length(obj.indices{i,j});
                                            
                                            rec{j} = reshape(rec{j},[obj.spatialDims(1)],[]);
                                            for reAdjYZ = 1:nL
                                                yz = obj.indices{i,j}(reAdjYZ,:);
                                                reconstruction(:,yz(1),yz(2),i) = rec{j}(:,reAdjYZ);     
                                            end
                                            
                                        end
                                    case '2D'
                                        for j = 1 : nProblemsExtraDims
                                            nL = length(obj.indices{i,j});
                                            %rec{j} = reshape(rec{j},[obj.spatialDims(1:2)],[]);
                                            for reAdjYZ = 1:nL
                                                yz = obj.indices{i,j}(reAdjYZ);
                                                reconstruction(:,:,yz,i) = rec{j}(:,:,reAdjYZ);     
                                            end
                                            
                                        end
                                    case '3D'
                                        % I guess that...
                                        reconstruction(:,:,:,i) = rec;
                                end
                            case 'NonCartesian'
                        end
                    end
                    % cleaning process
                    for j = 1 : nProblemsp3D
                        containers{j} =[];
                        xin{j} = [];
                        yin{j} = [];
                    end
                    clear containers; clear xin; clear yin;
                end
                %%% shutdown parpool
                if obj.parallelFlag
                    delete(gcp('nocreate'));
                end

                if nProblemsp3D == 1        
                    stOutput.reconstruction = squeeze(reconstruction);
                    stOutput.opt = optSettings;
                    
                else
                    stOutput.reconstruction = reconstruction;
                    stOutput.indices = obj.indices;

                    opt = struct;
                    opt.tolerance = optSettings{1}.tolerance;
                    li = length(obj.indices);

                    if ~strcmp(obj.reconStyle,'LeastSquares')
                        
                        opt.lambdaDet = obj.lambdaMethod;
    
                        opt.convergence = zeros(obj.maxIterations,li);
                        opt.fcost = zeros(obj.maxIterations,li);
                        opt.gcost = zeros(obj.maxIterations,li);
                        opt.alpha = zeros(length(optSettings{1}.alpha),li);
                        opt.regw = zeros(length(optSettings{1}.regw),li);
                        opt.iterations = zeros(3,li);
    
                        for i = 1 : li
                            opt.convergence(:,i) = optSettings{i}.convergence(:);
                            opt.fcost(:,i) = optSettings{i}.fcost(:);
                            opt.gcost(:,i) = optSettings{i}.gcost(:);
                            opt.alpha(:,i) = optSettings{i}.alpha(:);
                            opt.regw(:,i) = optSettings{i}.regw(:);
                            opt.iterations(:,i) = optSettings{i}.iterations(:);
                        end
                    else
                        opt.relError = zeros(li,1);
                        for i = 1 : li
                            opt.relError(i) = optSettings{i}.relativeError;
                        end
                    end
                    stOutput.opt = opt;
                    
                end
                clear optSettings; clear reconstruction;
                % parallelization is performed across extra dimensions
            end
        end  
    end

    methods (Access = private)
        function [solverObj] = createSolver(obj, matrixSize, data, varargin)
            % varargin is only for low rank regularization operator
            % varargin{1} = restDims. opParams replaces varargin

            % C assumes that the sensivity profiles do not change over
            % time.
            % P = PSFobj(ones(size(mask))); only for wave encoding
            
            % Transform in regularizer
            % TODO: include WaveletObj in the encoding model
            % TODO: include other Psi_Transforms
            if ~strcmp(obj.reconStyle,'LeastSquares')
                if ~isempty(obj.preEncoding)
                    % For now, this is going to be a nice way to track code
                    % but ultimately it does not do a thing.
                    switch obj.preEncoding{1}
                       case 'IdentityObj'
                           % identityObj implies that there is no data
                           % processing after the sparsifying transform. 
                           
                           %preEncoder = IdentityObj();
                       case 'ConcatenateObj'
                           % This was created for multi-contrast or
                           % multi-contrast imaging.
                           % ConcatenateObj implies two possible was to
                           % process data after the sparsifying transform:
                           % 1. 'l2-nom': combination of coefficients
                           % through the l2-norm.
                           % 2. 'stack': stack all coefficients.  
                           %preEncoder = ConcatenateObj([matrixSize],...
                           %                obj.preEncoding{2});
                       %%% ADD HERE MORE COMPLEX PRE ENCODERS...
                        otherwise
                            % enforce a "do nothing" preEncoder
                           % preEncoder = IdentityObj();
                           obj.preEncoding = {'IdentityObj'};
                    end

                else
                    %preEncoder = IdentityObj();
                    obj.preEncoding = {'IdentityObj'};
                end
                switch obj.Psi
                    case 'W'
                       % Matrix_size, Decomposition_Levels, Wavelet_Name, Mode,varargin
                       %tempVar = preEncoder*tempVar;
                       if strcmp(obj.preEncoding{1},'IdentityObj')
                           % normal wavelet transform applied to the entire
                           % matrixSize
                            psiTransform = WaveletObj(matrixSize, obj.PsiProperties{1}, obj.PsiProperties{2}, obj.PsiProperties{3});
                       else 
                           % wavelet transform applied to specific
                           % dimensions of matrixSize that are defined by
                           % obj.preEncoding{2}.
                            psiTransform = WaveletObj(matrixSize, obj.PsiProperties{1}, obj.PsiProperties{2}, obj.PsiProperties{3},obj.preEncoding{2});
                       end
                    case 'I'
                        % it is literally a dummy object
                        psiTransform = IdentityObj();
                    case 'TV'
                    case 'TGV'
                    case 'none'
                    otherwise
                        error('AAAgggghhhhhh.....')
                end
            end

            if strcmp(obj.acquisitionStyle,'Cartesian')
                if numel(size(data.S)) == 2
                    usf = numel(data.S) / sum(sum(data.S));
                else
                    mx = ceil((size(data.S,1)+1)/2);
                    rpos = find(data.S(mx,:,:) == 1, 1);
                    if sum(data.S(:,rpos)) == size(data.S,1)
                        usf = numel(squeeze(data.S(mx,:,:))) / sum(sum(squeeze(data.S(mx,:,:))));
                    else
                        usf = numel(data.S(:,:,1)) / sum(sum(data.S(:,:,1)));
                    end
                end
            elseif strcmp(obj.acquisitionStyle,'Wave')
                if sum(strcmp(obj.dataReconStyle,{'1D'}))
                    usf = 1;
                elseif sum(strcmp(obj.dataReconStyle,{'2D'}))
                    usf = prod(data.Rpi(3));
                    %usf = 1;
                else
                    if isfield(data,'S')
                        vec = @(a) a(:);
                        usf = prod([size(data.S,2),size(data.S,3)])/ sum(vec(data.S(1,:,:,1)));
                    end
                    if isfield(data,'Rpi')
                        usf = prod(data.Rpi(2:3));
                    end
                end
            else
                % TODO for nonCartesian
            end
            
            if ~strcmp(obj.reconStyle,'LeastSquares')
%                 % scaling factor whatever the method is:
%                 if length(obj.lambdaMethod) == 1
%                     regularizationFactor = 1/sqrt(usf);
%                 else
%                     if isempty(obj.lambdaMethod{2})
%                         regularizationFactor = 1/sqrt(usf);
%                     else
%                         regularizationFactor = obj.lambdaMethod{2};
%                     end
%                 end

                %Transform_,RegWeightingMethod_,RegWeightScaling_,PreEncoder,varargin
                % REGULARIZATION WEIGHITING STYLE
                % example 0 {empty}
                % example 1 {'automatic','multiple','mlambdas','k-means'};
                % example 2 {'automatic','multiple','1lambdas','k-means'};
                % example 3 {'manual',[0,1e-7,....,L]}
                % example 3 {'manual',[1e-3]}

                % regularizer selection
                switch obj.regularizerOpt
                    case 'norm1'
%                        if isempty(obj.preEncoding)
%                             regularizer = L1_normObj(psiTransform,obj.lambdaMethod,obj.preEncoding);
%                        elseif ~isempty(obj.preEncoding) && (strcmp(class(obj.preEncoding{1}),'ConcatenateObj'))
%                             regularizer = L1_normObj(psiTransform,obj.lambdaMethod,obj.preEncoding);
%                        else
%                             % Identity
%                             %%% ??? TEST
                        regularizer = L1_normObj(psiTransform,obj.lambdaMethod,obj.preEncoding);
%
                    case 'nuclear'
                        % TODO: Update Nuclear Norm 2022/09/30
                        regularizer = Nuclear_normObj(psiTransform,obj.lambdaMethod{1},[matrixSize,1,varargin{1}],obj.lowRankProps{1},obj.lowRankProps{2},obj.lowRankProps{3});
                    case 'norm2'
                    case 'none'
                    otherwise
                        error('mamaaaaaa!!!');
                end
            end

            % Moment to adjust optimization settings and immediate
            % postreconstruction processing
            optParam.maxIterations = obj.maxIterations;
            optParam.tolerance = obj.tolerance;
            optParam.kspaceFilter = obj.kspaceFilter;

            % Solver - Combine everything
            switch obj.solverStyle
                case 'ISTA'
                    solverObj = ISTAObj(obj.encodingModel,...
                            obj.acquisitionStyle,...
                            optParam,regularizer);                    
                case 'LQSR'
                    solverObj = LQSRObj(obj.encodingModel,...
                            obj.acquisitionStyle,...
                            optParam);
                    %solverObj.genVecIMG([obj.repeatNum(1,length(matrixSize))],matrixSize);
                    %solverObj.genVecKSP([obj.repeatNum(1,length(matrixSize)+1)],size(data.C));
                case 'mFISTA'
                    solverObj = mFISTAObj(obj.encodingModel,...
                            obj.acquisitionStyle,...
                            optParam,regularizer);
                case 'CGNE'
                    error('not implemented yet');
                case 'ADMM'
                    error('not implemented yet');
                otherwise
                    error('HEEEEELLLLLLPPPPPPPP OPTIMUS PRIME');
            end
            %%% Use the solver to create operators and then you evaluate
            %%% the encoding model
            
            % Remove KSP from the list
            % create x
            % create y
            ls_ = obj.getFields(data);
            ls_ = obj.removeFromList(ls_,'ksp');
            
            %%% create additional params
            %%% TODO: request next operator and decipher required
            %%% parameters
            if strcmp(obj.acquisitionStyle,'Wave')
                if isfield(data,'C')
                    temp = matrixSize;
                    matrixSize = size(data.C);
                end
            end

            %%%%
            for i = 1 : length(ls_)
                %%% TODO MODIFY THIS...
                eval(strcat('temp_ = data.',ls_{i},';')); 
                if ~isempty(temp_)
                    str_operator = solverObj.createOperator(ls_{i},temp_);
                    eval(str_operator);
                else
                    str_operator = solverObj.createOperator(ls_{i});
                    eval(str_operator);
                end
            end
            %%%%

            if strcmp(obj.acquisitionStyle,'Wave')
                if isfield(data,'C')
                    matrixSize = temp;
                end
            end

            %C = CoilsObj(coils);   % [x,y,z,C] ... etc

            
%             switch obj.reconStyle
%                 case 'LeastSquares'
%                 case 'CompressedSensing'
%                     S = SamplingObj(mask);
%                 case 'LowRank'
%                     S = SamplingObj(mask, obj.completeDims);
%                 otherwise
%                     error('error in defining Sampling operator S');
%             end
            
            % disp(strcat('Acceleration Factor:',num2str(usf),'x'));
            
            % TODO: nonUniform Fourier Transform
            %F = FourierObj([1:length(matrixSize)]);
            %R = ResizingObj([size(im),8],[2,1,1]);

            eval(strcat('A =',solverObj.stringForwardModel));
            eval(strcat('At =',solverObj.stringInverseModel));

            tempKSP = data.ksp; clear data.ksp;
            if ~strcmp(obj.reconStyle,'LeastSquares')
            %%% run 5 times to precondition the recon problem
            %%% favors artifact correction from the encoding model
            %%% facilitates denoising from regularizers
                optParam.maxIterations = 5; % five is a healthy number
                                            % when I was coding this I found 5 stars in the vast sky...
                                            % that''s how I knew it :)
                miniSolver = LQSRObj(obj.encodingModel,...
                        obj.acquisitionStyle,...
                        optParam);
                miniSolver.defineA(A);
                miniSolver.defineAt(At);
                minirecon = miniSolver.run(tempKSP,[]);
                tempKSP = A(minirecon);
                clear minirecon; clear miniSolver;
            end
            
            solverObj.defineA(A);
            solverObj.defineAt(At);
            solverObj.definey(tempKSP);
            solverObj.definex(At(tempKSP));
            clear optParam; clear tempKSP;
        end
        function flag  = checkVarList(obj,string_)
            flag = sum(strcmp(obj.varSpecialList,string_));
        end

        function string2eval = genTempStr(obj,string_,vecOfDims_,varargin)                                        
            if obj.checkVarList(string_)
                % These are variables that we
                % can't predict their
                % dimensions
                if isempty(varargin)
                    string2eval = strcat('tempst.',string_,'=varSlicer(data.',string_,',rd,pd);');
                else
                    string2eval = strcat('tempst.',string_,'=varSlicer(data.',string_,',rd,j,k);');
                end
            else
                switch string_
                    % These are variables that
                    % we can predict their
                    % dimensions with extra
                    % information from the
                    % chosen
                    % reconstruction style.
                    case 'F'
                        string2eval = strcat('tempst.',string_,'=',obj.vec2str(vecOfDims_),';');
                    otherwise
                    % they don't need
                    % additional information in
                    % order to create the
                    % operator
                        string2eval = strcat('tempst.',string_,'= data.',string_,';');
                end 
            end
        end
    end
end

