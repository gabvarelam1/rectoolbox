function [Selected_Level, Sparsity_per_level, Coefficients_per_level] = levelSelector(obj,Signal, Wavelet_Name , varargin)
%%% this function computes the ideal level of decomposition which maximizes
%%% the asymptotic sparsity from wavelets for n-th dimensional data.

%%% Example [level, Sparsity_per_level] = nD_wavelet_level_selector(I, wname);
%%% Input:
%%% (1) I: Data {1,2,3}D please
%%% (2) wname: mother wavelet name. Example 'db4'.
%%% (3) [Optinal] Mask: apply mask if any part of the signal can be
%%%                     ignored. Otherwise, the algorithm assumes the
%%%                     complete Data as relevant for the algorithm.
%%% Output:
%%% (1) level: ideal level for wavelet decomposition
%%% (2) Sparsity_per_level: sparsity per level according to the Gini Index
%%% (3) [Optional] Coefficients_per_1D: a cell which size is according to
%%%                                     number dimensions from data and contains cells which contain
%%%                                     the coefficient per level of the different dimensions.  

%%%% by gab, 2019
%%%% by gab, 2023

    data_dim = define_number_dimensions_from_signal(Signal);
    
    %%% 2. Preset for Algorithm
    if ~isempty(varargin)
        data = varargin{1} .* Signal;
    else
        data = Signal;
    end
    
    wavelet_name = Wavelet_Name;
    
    switch data_dim
        case 1
            [Selected_Level, Sparsity_per_level, Coefficients_per_level] = ...
                run_algorithm_1D(data,wavelet_name, obj.good_sparsity_level(1));
        case 2
            [Selected_Level, Sparsity_per_level, Coefficients_per_level] = ...
                run_algorithm_2D(data,wavelet_name, obj.good_sparsity_level(2));
        case 3
            [Selected_Level, Sparsity_per_level, Coefficients_per_level] = ...
                run_algorithm_3D(data,wavelet_name, obj.good_sparsity_level(3));
    end
end

%%% Additional functions

function [number_dimensions] = define_number_dimensions_from_signal(Signal)
    %%% Determine number of dimensions and check
    [a,b,c,d] = size(Signal);

    %%% 1. Determine the number of dimensions in the data
    %%% not the best procedure, but it helps if people uses a reasonably
    %%% structure for the input data...
    if ( a == 1 ) || ( b==1 )
        %%% one dimensional data
        number_dimensions = 1;
    elseif (c == 1) && (d == 1)
        %%% two dimensional data
        number_dimensions = 2;
    elseif (d == 1) 
        %%% three dimensional data
        number_dimensions = 3;
    elseif d > 1
        disp('Error ... impossible to characterize the data in levelSelector function...');
        number_dimensions = [];
        return;
    end
end

function GI = Gini_Index(SetCoefficients)
    % The Gini Index is 1 for maximum sparsity
    % The Gini Index is 0 for a uniform population.
    s = sort(abs(SetCoefficients(:)),'ascend');
    N = length(s);
    s = reshape(s,[1,N]);
    v = 1:N;
    l1norm = norm(s,1);
   
    
    GI = 1 - 2*sum( (s / l1norm ) .* ( (N-v+0.5)/N )  );
    % if SetCoefficients is a zero vector its going to be NaN.
    if isnan(GI)
        GI = 0;
    end
end

function [coefficients] = nD_extract_coefficients_from_wavelets(Coefficients_Set, Book, Number_Dimensions_Data , Extraction_Number , Coefficient_Types, Detail_Type)

    %%% function to extract coefficients from a matlab nD wavelet operator.
    % it changes according to the number of dimensions because the operator
    % is messy... :/
    
    %%% gab, 2019
    
    
%%%
% set_ranges_approx = @(book_ , iter_) (1 + sum(prod(book_(1:(iter_-1),:),2)) ) : sum(prod(book_(1:iter_,:),2));
% set_ranges_details_all = @(book_ , iter_) (1 + 3*sum(prod(book_(1:(iter_-1),:),2)) ) : 3*sum(prod(book_(1:iter_,:),2));

    
    switch Number_Dimensions_Data
        case 1
            coefficients = Coefficients_Set( (1 + sum(Book(1:(Extraction_Number-1))) : sum(Book(1:Extraction_Number))));
        case 2
            if strcmp(Coefficient_Types,'both')
                init_index = 2;
                switch Extraction_Number
                    case 1
                        % just approx coefficients
                        fact_1 = 1;
                        fact_2 = 1;
                        init_index = 1;
                        approx_factor = 0;
                    case 2
                        % boundary between approx and detail coefficients
                        fact_1 = 0;
                        fact_2 = 3;
                        approx_factor = 1;
                    otherwise
                        % detail coefficients
                        fact_1 = 3;
                        fact_2 = 3;
                        approx_factor = 1;
                end
            elseif strcmp(Coefficient_Types,'approx')
                fact_1 = 1;
                fact_2 = 1;
                approx_factor = 0;
                init_index = 1;
            elseif strcmp(Coefficient_Types,'detail')
                fact_1 = 3;
                fact_2 = 3;
                approx_factor = 0;
                init_index = 1;
            else
                fact_1 = [];
                fact_2 = [];
                approx_factor = [];
                init_index = [];
                coefficients = [];
                disp('Error in function nD_extract_coefficients_from_wavelets');
                return
            end
            
            %%% approx or all-details
            if isempty(Detail_Type) || strcmp(Detail_Type,'all')
                coefficients = ...
                    Coefficients_Set( (1 + approx_factor*sum(prod(Book(1,:),2)) + fact_1*sum(prod(Book(init_index:(Extraction_Number-1),:),2))  ) : ...
                                    ( approx_factor*sum(prod(Book(1,:),2)) + fact_2*sum(prod(Book(init_index:Extraction_Number,:),2)) ) );
            elseif strcmp(Detail_Type,'H')
                % set_ranges_details_H = @(book_ , iter_) (1 + 3*sum(prod(book_(1:(iter_-1),:),2)) ) : sum(prod(book_(1:iter_,:),2));
                fact_3 = 0;
                fact_4 = 1;
                coefficients = ...
                    Coefficients_Set( (1 + approx_factor*sum(prod(Book(1,:),2)) + fact_1*sum(prod(Book(init_index:(Extraction_Number-1),:),2)) + ...
                                       fact_3*sum(prod(Book(init_index:Extraction_Number,:),2))) : ...
                                       ( approx_factor*sum(prod(Book(1,:),2)) + fact_2*sum(prod(Book(init_index:Extraction_Number-1,:),2)) + ...
                                       fact_4*sum(prod(Book(Extraction_Number,:),2))) );
            elseif strcmp(Detail_Type,'V')
                % set_ranges_details_V = @(book_ , iter_) (1 + 3*sum(prod(book_(1:(iter_-1),:),2)) +   sum(prod(book_(1:(iter_-1),:),2) ) : ...
%                                             2*sum(prod(book_(1:iter_,:),2)))
                fact_3 = 1;
                fact_4 = 2;
                coefficients = ...
                    Coefficients_Set( (1 + approx_factor*sum(prod(Book(1,:),2)) + fact_1*sum(prod(Book(init_index:(Extraction_Number-1),:),2)) + ...
                                       fact_3*sum(prod(Book(Extraction_Number,:),2))) : ...
                                       ( approx_factor*sum(prod(Book(1,:),2)) + fact_2*sum(prod(Book(init_index:Extraction_Number-1,:),2)) + ...
                                       fact_4*sum(prod(Book(Extraction_Number,:),2))) );
            elseif strcmp(Detail_Type,'D')
                % set_ranges_details_D = @(book_ , iter_) (1 + 3*sum(prod(book_(1:(iter_-1),:),2)) + 2*sum(prod(book_(1:(iter_-1),:),2)) : ...
%                                             3*sum(prod(book_(1:iter_,:),2)));
                fact_3 = 2;
                fact_4 = 3;
                coefficients = ...
                    Coefficients_Set( (1 + approx_factor*sum(prod(Book(1,:),2)) + fact_1*sum(prod(Book(init_index:(Extraction_Number-1),:),2)) + ...
                                       fact_3*sum(prod(Book(Extraction_Number,:),2))) : ...
                                       ( approx_factor*sum(prod(Book(1,:),2)) + fact_2*sum(prod(Book(init_index:Extraction_Number-1,:),2)) + ...
                                       fact_4*sum(prod(Book(Extraction_Number,:),2))) );

            else
                coefficients = [];
            end
        case 3
    end
end



function [appropriate_level, sparsity_per_level, coefficients_per_level] = ...
    run_algorithm_1D(data,wavelet_name,appropriate_threshold)
    %%% Algorithm
    current_level = 1;
    n_dims = 1;
    flag = 1;
    good_sparsity_level = appropriate_threshold;
    maxLevel = 10; % After 10, it clearly means that the algorithm fail.
                   % If it fails, stick with L=3;
    %
    while flag
        next_level = current_level + 1;
        [data] = randshift(data);
        [coeffs,book] = wavedec(data,next_level,wavelet_name);
         
        % book contains: {AN,DN, DN-1, DN-2, ... , DN-N , Length of Signal}

        gi = zeros(next_level,1);
        coeffs_per_level = cell(size(gi));

        gi_low = Gini_Index(nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , 1, wavelet_name, [] ));
        %coeffs_per_level{1} = coeffs( (1) : sum(book(1)) ); 
        for iter = (next_level+1):-1:2
            level_coeffs = nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , iter, wavelet_name, [] );
            gi(iter-1) = Gini_Index(level_coeffs);
            coeffs_per_level{iter-1} = level_coeffs;
        end

        gi = flipud(gi);

        % Main condition to stop
        % Sj <= Tr && Sj+1 > Tr
        if  (gi(next_level) - gi(current_level)) < 0 && gi(current_level) > good_sparsity_level
            flag = 0;
            appropriate_level = current_level;
        else
            % increase j
            current_level = current_level + 1;
        end

        if current_level == maxLevel
            % good sparsity cannot be achieve. Most likely, the data is
            % just noise. Stop and use either L=1 or L=3. 
            flag = 0;
            appropriate_level = 3;
            warning('automatic decomposition level selection failed! Setting it to default');
        end

    end
    %Summary of Results
    sparsity_per_level = [gi_low ; flipud(gi(1:end-1))];
    coefficients_per_level = cell(next_level+1,1);  % to visualize the gi_low + rejected level too
    coefficients_per_level{1} = nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , 1, wavelet_name, [] );
    for iter = 2:(next_level+1)
        coefficients_per_level{iter} = coeffs_per_level{iter-1};
    end
end

function [appropriate_level, sparsity_per_level, coefficients_per_level] = ...
    run_algorithm_2D(data,wavelet_name,appropriate_threshold)

    current_level = 1;
    flag = 1;
    dtype = {'H','V','D'};
    n_dims = 2;
    good_sparsity_level = appropriate_threshold;
    maxLevel = 10;

    while flag
    
        next_level = current_level + 1;
        
        [data] = randshift(data);
        [coeffs,book] = wavedec2(data,next_level,wavelet_name);
         
        % book contains: {AN,DN, DN-1, DN-2, ... , DN-N , Length of Signal}
        %Sj = zeros(appropriate_level+1,1);
        gi_mean = zeros(next_level,1);
        coeffs_per_level = cell(size(gi_mean));
        
        gi_x = zeros(next_level,1);
        gi_y = zeros(next_level,1);
        gi_xy = zeros(next_level,1);
    %   Sparsity Values

    %    Sj(1) = [sj_funct( nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , 1, 'both',[]) )];
        coeffs_low = nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , 1, 'both',[]);
        gi_low = Gini_Index(coeffs_low); 
        for iter = (next_level+1):-1:2
             for iter2 = 1:3
                switch iter2
                    case 1
                        level_coeffs_x = nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , iter , 'both', dtype{iter2});
                        gi_x(iter-1) = Gini_Index(level_coeffs_x);
                    case 2
                        level_coeffs_y = nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , iter , 'both', dtype{iter2});
                        gi_y(iter-1) = Gini_Index(level_coeffs_y);
                   otherwise
                        level_coeffs_d = nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , iter , 'both', dtype{iter2});
                        gi_xy(iter-1) = Gini_Index(level_coeffs_d);
                 end
             end
             %gi_mean(iter-1) = mean([gi_x(iter-1),gi_y(iter-1),gi_xy(iter-1)]);
             coeffs_per_level{iter-1} = [level_coeffs_x,level_coeffs_y,level_coeffs_d];
             gi_mean(iter-1) = Gini_Index(coeffs_per_level{iter-1});
         end

        gi_mean = flipud(gi_mean);

        % Main condition to stop
        % Sj <= Tr && Sj+1 > Tr
        if  (gi_mean(next_level) - gi_mean(current_level)) < 0 && gi_mean(current_level) > good_sparsity_level
            flag = 0;
            appropriate_level = current_level;
        else
            % increase j
            current_level = current_level + 1;
        end

        if current_level == maxLevel
            % good sparsity cannot be achieve. Most likely, the data is
            % just noise. Stop and use either L=1 or L=3. 
            flag = 0;
            appropriate_level = 3;
            warning('automatic decomposition level selection failed! Setting it to default');
        end

    end
    [coeffs,book] = wavedec2(data,appropriate_level,wavelet_name);
    coeffs_low = nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , 1, 'both',[]);
    gi_low = Gini_Index(coeffs_low); 
    sparsity_per_level = [gi_low ; gi_mean];
    coefficients_per_level = cell(next_level+1,1); % to visualize rejected level too
    coefficients_per_level{1} = coeffs_low;
    for iter = 2:(next_level+1)
        coefficients_per_level{iter} = coeffs_per_level{iter-1};
    end
end

function [appropriate_level, sparsity_per_level, coefficients_per_level] = ...
    run_algorithm_3D(data,wavelet_name,appropriate_threshold)

    current_level = 1;
    flag = 1;
    good_sparsity_level = appropriate_threshold;
    maxLevel = 7;
    %
    vec = @(a) a(:);
    while flag
    
        next_level = current_level + 1;
        [data] = randshift(data);
        [wstruct] = wavedec3(data,next_level,wavelet_name);
        
        %Nl = appropriate_level + 2; 
        % book contains: {AN,DN, DN-1, DN-2, ... , DN-N , Length of Signal}
        %Sj = zeros(appropriate_level+1,1);
        gi_mean = zeros(next_level,1);
        directions_per_level = 7;
        gi_3D_per_level = zeros(directions_per_level,1);
    %   Sparsity Values

    %    Sj(1) = [sj_funct( nD_extract_coefficients_from_wavelets(coeffs, book, n_dims , 1, 'both',[]) )];
        gi_low = Gini_Index( vec( wstruct.dec{1} ) ); 
        for iter = 1:next_level
             for iter2 = 1:7
                gi_3D_per_level(iter2) = Gini_Index( vec( wstruct.dec{ ...
                                                         directions_per_level*(iter-1) + iter2 +1} ) );
             end
             gi_mean(iter) = mean(gi_3D_per_level);
         end

        gi_mean = flipud(gi_mean);

        % Main condition to stop
        % Sj <= Tr && Sj+1 > Tr
        if  (gi_mean(next_level) - gi_mean(current_level)) < 0 && gi_mean(current_level) > good_sparsity_level
            flag = 0;
            appropriate_level = current_level;
        else
            % increase j
            current_level = current_level + 1;
        end

        if current_level == maxLevel
            % good sparsity cannot be achieve. Most likely, the data is
            % just noise. Stop and use either L=1 or L=3. 
            flag = 0;
            appropriate_level = 3;
            warning('automatic decomposition level selection failed! Setting it to default');
        end

    end
    sparsity_per_level = [gi_low;gi_mean];
    coefficients_per_level = [];
end

function [out,r] = randshift(in)
    % Randomly shift input in all dimensions
    % Outputs shifted input and the shifts r
    
    s = size(in);
    
    r = zeros(1,length(s));
    
    
    for i = 1:length(s)
        r(i) = randi(s(i));
    end
    
    out = circshift(in,r);
end






