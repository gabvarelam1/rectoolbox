function [res] = cell2num(~, data_)
    % this function is only to vectorize the 3D wavelet transform
    vec = @(x_) x_(:);

    % lol, level 1 has 8 and the rest has 7. Be careful with that.
    if iscell(data_)
        res = vecAll(data_);
    else
        res = data_;
    end

end

function res = vecAll(data_)
    l = length(data_);
    tot = 0;
    cumtot = zeros(l+1,1); % eval if this is wrong
    cc = 2;
    for i = 1 : l
        temp = numel(data_{i});
        tot = tot + temp;
        cumtot(cc) = tot;
        cc = cc + 1;
    end
    dd = diff(cumtot);
    cc = 1;
    res = zeros(tot,1);
    for i = 1 : l
        %disp([cc,dd(i),length(data_{i}(:))]);
        res( cc : (cc-1) + dd(i)  ) = data_{i}(:);
        cc = cc + dd(i);
    end
end

