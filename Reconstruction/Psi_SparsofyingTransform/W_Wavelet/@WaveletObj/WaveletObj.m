classdef WaveletObj < handle & SystemEvalObj
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        W                       =[];
        Wt                      =[];
        decompositionLevels     =[];
        waveletName             =[];
        adjoint                 = 0;
        mode                    =[];
        matrixSize              =[];
        numDimensions           =[];
        book                    =[];
        rshift                  =[];
        codes                   =[];
        waveletLevels           =[];
        lenCoeffs               =[];
        specialMode             =[];
        specialMtxSize          =[];
        specialEncoding         =[];
    end

    properties (Access = public, Constant)
        good_sparsity_level = [.75, .6 , .6]; % 1D , 2D , 3D , else is unknown for me.
    end
    
    methods (Access = public)
        function obj = WaveletObj(Matrix_size, Decomposition_Levels, Wavelet_Name, Mode,varargin)
            
            % call superclass constructor
            obj@SystemEvalObj();

            if strcmp(Decomposition_Levels,'automatic')
                %%% IF YOU CHOOSE THIS MODE, IT MEANS THAT Matrix_size is
                %%% in fact the image/volume of interest.

                % How to determine the number of wavelet levels using the
                % Gini Index. 
                % For reference: "Automatic determination of the regularization weighting 
                %          for wavelet-based compressed sensing
                %          reconstructions", MRM, 2021.

                % The big idea: Each level of the wavelet transform should be sparse. 
                % If it is not sparse, or sparser than the previous level, 
                % it means that it complicates the ability to differentiate between structure and noise.
                % Therefore, stop adding levels.

                % IMPORTANT: Codes are old but still useful.

                if sum(imag(Matrix_size(:))) ~= 0
                    [LrealChannel] =  obj.levelSelector(real(Matrix_size), Wavelet_Name);
                    [LimagChannel] =  obj.levelSelector(imag(Matrix_size), Wavelet_Name);
                
                    Decomposition_Levels = min([LrealChannel, LimagChannel]);
                else
                    [Decomposition_Levels] =  obj.levelSelector(real(Matrix_size), Wavelet_Name);
                end
                
                warning(strcat('Result from automatic selection, L=',num2str(Decomposition_Levels)));
                % returning Matrix_size to its true purpose
                Matrix_size = size(Matrix_size);
            end

            if ~isempty(varargin)
                warning('special mode enabled: applying wavelet transform in specific dimensions');
                obj.specialMode = 1;
                % varargin{1} is a logical vector to indicate specific
                % dimensions.
                obj.specialMtxSize = Matrix_size; % true size
                obj.specialEncoding = logical(varargin{1});
                temp = Matrix_size(logical(varargin{1}));
                if length(temp) == 1
                    Matrix_size = [temp,1];
                else
                    Matrix_size = temp;
                end
            else
                obj.specialMode = 0;    
            end

            temp_size = Matrix_size;
            temp_size(temp_size==1) = [];
            num_Dimensions = numel(temp_size);
            if num_Dimensions > 3
                error('No implementation of a wavelet for N > 3');
            else
                %disp(strcat('Creating Wavelet Operator for ', num2str(num_Dimensions),'D ...' ));
                %disp(strcat(' Wavelet Operator only works for matrix size [', num2str(Matrix_size),'] ...' ))
                %disp(strcat('Additional Dimensions Over 3 will results in independent 3D transforms'));
            end
            if isempty(Mode)
                Mode = 'sym';
            end
            % check GPU constaints
            obj.mode = Mode;
            dwtmode(Mode, 0);
        
            obj.W = obj.defineWavelet('forward',num_Dimensions,Decomposition_Levels, Wavelet_Name, Mode, []);
            
            dummy_signal = zeros(Matrix_size);
            switch num_Dimensions
                case 1
                    [c,book] = wavedec(dummy_signal,Decomposition_Levels,Wavelet_Name);
                    obj.lenCoeffs = cumsum(prod(book,2));
                    obj.lenCoeffs(end) = length(c);
                case 2
                    [c,book] = wavedec2(dummy_signal,Decomposition_Levels,Wavelet_Name);
                    obj.lenCoeffs = cumsum(prod(book,2) .* [1;3*ones(Decomposition_Levels,1);1]);
                    obj.lenCoeffs(end) = length(c);
                case 3
                    st = wavedec3(dummy_signal,Decomposition_Levels,Wavelet_Name);
                    book = st.sizes;
                otherwise
                    book = [];
            end
        
            obj.Wt = obj.defineWavelet('inverse',num_Dimensions,Decomposition_Levels, Wavelet_Name, Mode, book);
            obj.decompositionLevels = Decomposition_Levels;
            obj.waveletName = Wavelet_Name;


            obj.matrixSize = Matrix_size;
            obj.numDimensions = num_Dimensions;
            obj.book = book;
            obj.rshift = [];
            obj.genCodesAndLevels(num_Dimensions,Decomposition_Levels);
        end

        function res = RSOS(obj,b)
            % this should work for 1D and 2D...
            lenOp = length(b);
            sb = length(b{1});
            res = zeros([sb,length(b)]);
            % stry = obj.genRepString(':,',length(sb) ); % for the future
            for i = 1 : lenOp
                res(:,i) = b{i}(:);
            end
            %res = mean(abs(res),2); % FAVORITE
            res = sqrt(sum(abs(res).^2,2)); % TESTING
            % DISCARDED OPTIONS:
            %res = abs(res(:,1));
            %res = min(abs(res)')';
            %res = max(abs(res)')';
            
            %res = median(res,2);  % root square of means according to some people
                                  % let's try mean first
                                  % mean is badly affected by outliers
                                  % median kind of the same
        end

        function res = stack(obj,b)
            lenOp = length(b); %cell
            sb = length(b{1});
            res = zeros(sb*lenOp,1);
            for i = 1 : lenOp
                res( (1 + (i-1)*sb) : i*sb ) = b{i};
            end
        end


        function [Count, Bins] = histogram(~,Data, N)
            Maxd = max(Data);
            Mind = min(Data);
            Bins = linspace(Mind-.1*Mind, Maxd+.1*Maxd, N);
            
            Count = zeros(N,1);
            for iter = 2 : N
               Count(iter-1) = sum((Data >= Bins(iter-1)) .* (Data < Bins(iter)) ); 
            end
        end

        function lambda = selectLambda(obj,style,data,N,varargin)
            [c,b] = obj.histogram(data,N);
            c(1) = [];
            b(1) = [];
            switch style
                case 'Rayleigh'
                    [~,pos] = max(conv(c,[1/3,1/3,1/3])); % To smooth the histogram
                    lambda = (b(pos) / sqrt(2));
                case 'Manual'
                    lambda = varargin{1};
            end

        end

        function denres = softthreshold(~,data,Lambda)

                denres =  ( abs(data) >= Lambda) .* ...
                        (exp( -1i*angle(data) ) .* ( abs(data) - Lambda))  ;

        end
        function denres = hardthreshold(~,data,Lambda)
                
                denres =  conj(( abs(data) >= Lambda) .* data);

        end

        function L1 = kmeans(obj, coefficients, scaling)
            h = obj.applyHistogram(abs(coefficients(:)));
            vals = h.Values(2:end);
           
            if ~isempty(vals)
                [~,pos] = max(vals);
                kgroups = 2;
                map_of_groups = kmeans(vals(:),kgroups); % noise-, mixed- and signal-based wavelet coefficients as clusters.
                bin_pos = find(map_of_groups == map_of_groups(pos),1,'last');
                if (bin_pos == length(vals)) || (bin_pos == 1)
                    bin_pos = round(length(vals)/2) + 1;
                else
                    bin_pos = bin_pos+1;
                end
                threshold = h.BinEdges(bin_pos); % +1 to compensate for the removed value
            else
                threshold = mean(abs(coefficients(:)));
            end
            %L1 = obj.regWeightScalingFactor *(threshold);
            if isempty(scaling)
                L1 = (threshold/sqrt(2));
            else
                L1 = scaling * (threshold/sqrt(2));
            end

        end

        function [denres,lambdas] = softDenoise(obj,style,data,N,varargin)
            if obj.specialMode == 1
                % we expect different levels of wavelet coefficients
                nl = length(data);
                lambdas = zeros(nl,1);
                denres = data;
                for i = 1 : nl
                    if isempty(varargin)
                        lambdas(i) = obj.selectLambda(style,abs(data{i}),N);
                    else
                        lambdas(i) = obj.selectLambda(style,abs(data{i}),N,varargin{1});
                    end
                    denres{i} = obj.softthreshold(data{i},lambdas(i));
                    %denres{i} = obj.softthreshold(data{i},lambdas(i) * 2.2171e-05*sqrt(2*(log(13))));

                end
            else
                % we expect a cell that contains different levels of
                % wavelet coeffiicients
                swavmat = size(data);
                
                ss3 = obj.getCoefficients(data{1,1},'d',[1:obj.decompositionLevels]);

                nl = size(ss3,1); clear ss3;

                lambdas = zeros([swavmat,nl]);
                denres = data;
                for i = 1 : swavmat(1)
                    for j = 1 : swavmat(2)
                        
                        appcoef = obj.getCoefficients(data{i,j},'app',obj.decompositionLevels);
                        temp = obj.getCoefficients(data{i,j},'d',[1:obj.decompositionLevels]);
                        for z = 1 : nl
                            
                            lambdas(i,j,z) = obj.selectLambda(style,abs(temp{z}),N);
                            temp{z} = obj.softthreshold(temp{z},lambdas(i,j,z));
                            
                        end
                        denres{i,j} = [appcoef ; obj.cell2num(temp)];

                    end
                end

            end
        end

        function [denres,lambdas] = hardDenoise(obj,style,data,N,varargin)
            if obj.specialMode == 1
                % we expect different levels of wavelet coefficients
                nl = length(data);
                lambdas = zeros(nl,1);
                denres = data;
                for i = 1 : nl
                    if isempty(varargin)
                        lambdas(i) = (2*sqrt(2))*obj.selectLambda(style,abs(data{i}),N);
                    else
                        lambdas(i) = (2*sqrt(2))*obj.selectLambda(style,abs(data{i}),N,varargin{1});
                    end
                    denres{i} = obj.hardthreshold(data{i},lambdas(i));
                    %denres{i} = obj.hardthreshold(data{i},varargin{1});

                end
            else
                % we expect a cell that contains different levels of
                % wavelet coeffiicients
                swavmat = size(data);
                
                ss3 = obj.getCoefficients(data{1,1},'d',[1:obj.decompositionLevels]);

                nl = size(ss3,1); clear ss3;

                lambdas = zeros([swavmat,nl]);
                denres = data;
                for i = 1 : swavmat(1)
                    for j = 1 : swavmat(2)
                        
                        appcoef = obj.getCoefficients(data{i,j},'app',obj.decompositionLevels);
                        temp = obj.getCoefficients(data{i,j},'d',[1:obj.decompositionLevels]);
                        for z = 1 : nl
                            
                            lambdas(i,j,z) = obj.selectLambda(style,abs(temp{z}),N);
                            temp{z} = obj.hardthreshold(temp{z},lambdas(i,j,z));
                            
                        end
                        denres{i,j} = [appcoef ; obj.cell2num(temp)];

                    end
                end

            end
        end

        % NOT NECESSARY
        %function switchGPU(obj)
        %    switchGPU@SystemEvalObj(obj);
        %    % Need to re-define forward and inverse transforms
        %    nd = obj.numDimensions;
        %    lv = obj.decompositionLevels;
        %    wn = obj.waveletName;
        %    bk = obj.book;
        %    % TODO: if GPU on -> use enabled mode on gpuArray
        %    md = obj.mode;

        %    obj.Wt = obj.defineWavelet('inverse',nd,lv, wn, md, bk);
        %    obj.W = obj.defineWavelet('forward',nd,lv, wn, md, []);
        %end

        function switchParallel(obj)

            switchParallel@SystemEvalObj(obj); %<- No real reason to add it, right? 
            warning('WaveletObj does not change when switching parallel computing mode');

        end
    end

    methods (Access = private)
        
        function Transform = defineWavelet(obj,ForOrInv,num_dimensions,level,wname,mode,book)

          %W = dbwavf(wname);
          %[Lo_D,Hi_D,Lo_R,Hi_R] = orthfilt(W);
          [Lo_D,Hi_D,Lo_R,Hi_R] = wfilters(wname);
          % error: gpuArray only accepts explicit numeric or logical arrays
          %if obj.gpuFlag()
          %    % if gpu enabled we transfer data to gpuArray
          %    % and we need to change the mode of the wavelet transform.
          %    ar = @(dat_) gpuArray(dat_);
          %else
          %    % dummy function
          %    ar = @(dat_) dat_;
          %end

          if strcmp(ForOrInv,'forward')
        
            switch num_dimensions
                case 1
                    %Transform = @(im)
                    Transform = @(im)  wavedec((real(im)),level,Lo_D,Hi_D) + ...
                                       1i*wavedec((imag(im)),level,Lo_D,Hi_D);
                case 2
                    %Transform = @(im)
                    Transform = @(im)  wavedec2((real(im)),level,Lo_D,Hi_D) + ...
                                       1i*wavedec2((imag(im)),level,Lo_D,Hi_D);
                case 3
                    %Transform = @(im) wavedec3(im,level,wname,'mode',mode);
                    Transform = @(im)  {wavedec3((real(im)),level,{Lo_D,Hi_D,Lo_R,Hi_R}),...
                                       wavedec3((imag(im)),level,{Lo_D,Hi_D,Lo_R,Hi_R})};
            end
            
          else  % inverse
                % strcmp(ForOrInv,'inverse')
            switch num_dimensions
                case 1
                    Transform = @(wavstruct) ...
                        waverec((real(wavstruct)),book,wname) +...
                        1i*waverec((imag(wavstruct)),book,wname);
                case 2
                    Transform = @(wavstruct) ...
                        waverec2((real(wavstruct)),book,wname) +...
                        1i*waverec2((imag(wavstruct)),book,wname);
                case 3
                    Transform = @(wavstruct) ...
                        waverec3((wavstruct{1})) + ...
                        1i*waverec3((wavstruct{2}));
            end
            
          end
          
        end
        function [out] = randshift(obj,in)
            % Randomly shift input in all dimensions
            % Outputs shifted input and the shifts r
            
            % to make it consistent in additional dimensions, rshift,r, is
            % defined once.
            if isempty(obj.rshift)
                s = size(in);
                r = zeros(1,length(s));
                for i = 1:length(s)
                    r(i) = randi(s(i));
                end
                obj.rshift = r;
            end
            
            out = circshift(in,obj.rshift);
            
            
        end
        function out = randunshift(obj,in)
            % Unshifts
            out = circshift(in,-obj.rshift);
            %obj.rshift = [];
        end

        function  cleanrshift(obj)
            obj.rshift = [];
        end

        function genCodesAndLevels(obj,nd,L)
            obj.codes = cell(L+1,1);
            obj.waveletLevels = zeros(L+1,1);

            obj.codes{1} = 'app'; % app + high-frequency wavelet coefficients
            obj.waveletLevels(1) = L;

            if nd < 3
                for i = 1 : L
                   obj.codes{i+1} = 'all'; % all for HVD high-frequency wavelet coefficients
                   obj.waveletLevels(i+1) = L-i+1; % from L to 1
                end
            else
              for i = 1 : L
                   obj.codes{i+1} = 'det'; % det for the 7 components of the level
                   obj.waveletLevels(i+1) = L-i+1; % from L to 1
               end
            end
        end
        function string_ = genRepString(~,characters,ntimes)
            b = characters;
            string_ = b;
            for i = 2 : ntimes
                string_ = strcat(string_ , b);
            end
            string_(end) = [];
        end
        function checkSize(obj,sizeMatrix)
            sw = obj.matrixSize(obj.specialEncoding);
            if sum(sizeMatrix(1:length(sw)) == sw) ~= length(sw)
                error(strcat('expecting matrix size:[',num2str(obj.specialMtxSize),']'));
            end
        end
        function [st] = applyHistogram(obj,data)
            if length(data) < 511
                N = nextpow2(length(data))-5;
                [count,bins] = obj.genHistogram(abs(data(:)),2^(N));
                st.Values = count;
                st.BinEdges = bins;
            else
                %figure,
                h = histogram(data);
                st.Values = h.Values;
                st.BinEdges = h.BinEdges(1:end);
                close
            end
        end

        function [Count, Bins] = genHistogram(~,Data, N)
            Maxd = max(Data);
            Mind = min(Data);
            Bins = linspace(Mind-.1*Mind, Maxd+.1*Maxd, N);
            
            Count = zeros(N,1);
            for iter = 2 : N
               Count(iter-1) = sum((Data >= Bins(iter-1)) .* (Data < Bins(iter)) ); 
            end
        end
    end
end

