function res = getCoefficients(obj,waveCoeffs,wType,wLevels,varargin)

    % waveCoefficients that are out of the class
    % wType = 
    %   1D: There is no type but here helps to differentiate between App
    %   and Det functions.
    %   2D: 'all'-'h'-'v'-'d' NOTE: 'all' is hvd.
    %   3D: ???

    % wLevels: from what wavelet levels. It can be a number or vector of
    % levels.
    %   1D:
    %   2D:
    %   3D: ???
    
    if ~isempty(wLevels)
        wL = length(wLevels);
        if wL > obj.decompositionLevels
            error(strcat('Wavelet Obj has only',num2str(obj.decompositionLevels),'<',num2str(wL)));
        end
        
    else
        % if empty work with the maximum level
        wLevels = obj.waveletLevels;
    end
    wL = length(wLevels);

    switch obj.numDimensions

        case 1
            
            % approximation
            if strcmp('app',wType)
                if wL == 1
                    res = appCoeff1D(waveCoeffs,obj.book,obj.waveletName,wLevels);
                else
                    res = cell(wL,1);
                    for i = 1 : wL
                        res{i} = appCoeff1D(waveCoeffs,obj.book,obj.waveletName,wLevels(i));
                    end
                end
            elseif strcmp('det',wType)
                if wL == 1
                    res = detCoeff1D(waveCoeffs,obj.book,obj.waveletName,wLevels);
                else
                    res = cell(wL,1);
                    for i = 1 : wL
                        res{i} = detCoeff1D(waveCoeffs,obj.book,obj.waveletName,wLevels(i));
                    end
                end
            else
                % all
                res = waveCoeffs;
            end

        case 2
            %wL = length(wLevels);
            % approximation
            if strcmp('app',wType)
                if wL == 1
                    res = appCoeff2D(waveCoeffs,obj.book,obj.waveletName,wLevels);
                else
                    res = cell(wL,1);
                    for i = wL : -1 : 1
                        res{wL - i +1} = appCoeff2D(waveCoeffs,obj.book,obj.waveletName,wLevels(i));
                    end
                end
            else
                if strcmp('all',wType)
                    if wL == 1
                        res = detCoeff2D('compact',waveCoeffs,obj.book,wLevels);
                    else
                        res = cell(wL,1);
                        for i = wL : -1 : 1
                            res{wL - i +1} = detCoeff2D('compact',waveCoeffs,obj.book,wLevels(i));
                        end
                    end
                else
                    if wL == 1
                        res = detCoeff2D(wType,waveCoeffs,obj.book,wLevels);
                    else
                        res = cell(wL,1);
                        for i = wL : -1 : 1
                            res{wL - i +1} = detCoeff2D(wType,waveCoeffs,obj.book,wLevels(i));
                        end
                    end
                end
            end

        case 3
            if isempty(varargin)
                switch wType
                    case 'all'
                        res = allCoeff3D(waveCoeffs);
                    case 'app'
                        res = appCoeff3D(waveCoeffs);
                    case 'det'
                        res = detCoeff3D(waveCoeffs,wLevels);
                    otherwise
                        error('3 options only: "all", "app" and "det"');
                end
            else
                % varargin for now only accepts the very special case when
                % you want to extract an specific layer (out of 7) from a
                % level on a 3D wavelet transform
                switch wType
                    %case 'all'
                    %    res = allCoeff3Dlayer(waveCoeffs,varargin);
                    case 'app'
                        res = appCoeff3D(waveCoeffs);
                    case 'det'
                        res = detCoeff3Dlayer(waveCoeffs,wLevels,varargin{1});
                    otherwise
                        error('2 option(s) only: "app","det"');
                end

            end
    end
    
end

function res = removeNonImag(res)
    if sum(imag(res))==0
        res = real(res);
    end
end

function res = appCoeff1D(c,b,wn,l)
    res = appcoef(real(c),b,wn,l) + 1i*appcoef(imag(c),b,wn,l);
    res = removeNonImag(res);
end
function res =detCoeff1D(c,b,wn,l)
    res = detcoef(real(c),b,l) + 1i*detcoef(imag(c),b,l);
    res = removeNonImag(res);
end
function res = appCoeff2D(c,b,wn,l)
    res = appcoef2(real(c),b,wn,l) + 1i*appcoef2(imag(c),b,wn,l);
    res = reshape(removeNonImag(res(:)),[1,numel(res)]);
end
function res = detCoeff2D(t,c,b,l)
    res = detcoef2(t,real(c),b,l) + 1i*detcoef2(t,imag(c),b,l);
    res = removeNonImag(res);
end
function res = appCoeff3D(c)
    % wavestruct.dec{1} 
    res = c{1}.dec{1} + 1i*c{2}.dec{1};
    res = removeNonImag(res);
end

function res = allCoeff3D(c)
    
    len = length(c{1}.dec);
    res = cell(len,1);
    
    for i = 1 : len
        res{i} = removeNonImag(c{1}.dec{i} +1i*c{2}.dec{i}); 
    end

end

function res = detCoeff3D(c,wlevels)
    % wavestruct.dec{1 'app' + 7*wlevels 'det' coefficients}
    maps_per_level = 7;
    len = length(wlevels)*maps_per_level;
    totalLen = size(c{1}.sizes,1)-1;
    

    res = cell(len,1);
    cc = 1;
    for i = (totalLen - wlevels)+1
        for j = maps_per_level*(i-1)+2 : maps_per_level*i+1
            res{cc} = removeNonImag(c{1}.dec{j} +1i*c{2}.dec{j});
            cc = cc + 1;
        end
    end
end

function res = detCoeff3Dlayer(c,wlevels,layer)
    
    maps_per_level = 7;
    len = length(wlevels);
    totalLen = size(c{1}.sizes,1)-1;
    
    if len == 1
        for i = (totalLen - wlevels)+1
            j = maps_per_level*(i-1)+layer+1;
            res = removeNonImag(c{1}.dec{j} +1i*c{2}.dec{j});
        end
    else
        res = cell(len,1);
        cc = 1;
        for i = (totalLen - wlevels)+1
            j = maps_per_level*(i-1)+layer+1; % if layer is 8 it shoulf fail
            res{cc} = removeNonImag(c{1}.dec{j} +1i*c{2}.dec{j});
            cc = cc + 1;
        end
    end
end