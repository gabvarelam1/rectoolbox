function [res] = mtimes(a,b)
    if a.specialMode == 0
        % Usual wavelet transform
        if a.adjoint
            res = a.Wt(b);
            res = a.randunshift(res);
            %a.cleanrshift();

            if sum(imag(res(:))) == 0
                res = real(res);
            end
            a.adjoint = xor(a.adjoint,1);
        else
            [b] = a.randshift(b);
            res = a.W(b);
            
        end
    else
        % special mode enabled
        % wavelet transform is applied in a specific direction from a
        % bigger matrix
        if a.adjoint
            sw = a.matrixSize; % to apply wavelet transform
            so = a.specialMtxSize(~a.specialEncoding);
            lenOp = prod(so);

            res = zeros([sw,lenOp]);

            for i = 1 : lenOp
                    res(:,:,i) = a.Wt(b{i});
                    res(:,:,i) = a.randunshift(res(:,:,i));
            end
            %a.cleanrshift(); % refreshing the shift distorts convergence
            %in the optimization process.
            res = reshape(res,a.specialMtxSize);
            % res = a.Wt(b);
            %res = obj.randunshift(res);

            if sum(imag(res(:))) == 0
                res = real(res);
            end
            a.adjoint = xor(a.adjoint,1);
        else
            %[bshifted] = obj.randshift(b);
            sw = a.matrixSize(a.specialEncoding); % to apply wavelet transform
            sb = size(b);
            a.checkSize(sb(a.specialEncoding));
            
            so = a.specialMtxSize(~a.specialEncoding);
            lenOp = prod(so);
            res = cell(lenOp,1);
            % repeat : depending on the number of dimensions
            stry = a.genRepString(':,',length(sw));
            for i = 1 : lenOp
                eval(strcat('[b(',stry,',i)] = a.randshift(b(',stry,',i));'));
                eval(strcat('res{i} = a.W(b(',stry,',i));'));
            end
        end
    end
end