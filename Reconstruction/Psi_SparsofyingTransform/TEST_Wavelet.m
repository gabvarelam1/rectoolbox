clear all; close all; clc;
%% 1D 
% Test 1: Behavior when signal is equal to mother wavelet
signal = zeros(128,1);
signal(51) = 1;
signal(52) = -1;

W1d = WaveletObj(size(signal),2,'haar','sym');

ws = W1d*signal;
figure, 
subplot 121, plot(real(ws))
signalrec = W1d'*ws;
figure, 
subplot 121,plot(signalrec);
subplot 122,plot(signal);

% Test 2: Behavior when signal is noise
signal2 = rand(size(signal));

ws = W1d*signal2;
figure, 
subplot 121, plot(real(ws))
signalrec2 = W1d'*ws;
figure
subplot 121, plot(signalrec2); 
subplot 122, plot(signal2);

% Test 3: A real 1d signal
load handel
signal3 = y; clear y;
time3 = (1:size(signal3))/Fs; clear Fs;

% Test 4: 1 big haar mother wavelet

signal4 = zeros(128,1);
signal4(50:64) = 1;
signal4(65:79) = -1;
figure, plot(signal4)

%% Testing Automatic Level 1D

W1d = WaveletObj(signal,'automatic','haar','sym');  % 1 mother wavelet
W1e = WaveletObj(signal2,'automatic','haar','sym'); % pure noise
W1f = WaveletObj(signal3,'automatic','haar','sym'); % a real signal
W1f = WaveletObj(signal4,'automatic','haar','sym'); % a real signal
%% 2D

im = double(imread('cameraman.tif'));
%% Testing Automatic Level 2D

W2t = WaveletObj(im,'automatic','db4','ppd');  % cameraman
L = W2t.decompositionLevels;
%%
W2d = WaveletObj(size(im),L,'db4',[]);

ws = W2d*im;
imrec = W2d'*ws;

figure, imshow([im,imrec],[]); 
disp(strcat('error between images:',num2str(norm(im(:)-imrec(:))/norm(im(:))),'%'));
%% Testing extraction of coefficients
% checked
W2d.getBook()
ss2 = W2d.getCoefficients(ws,'app',L);
ss1 = W2d.cell2num(W2d.getCoefficients(ws,'all',[1:L]));
ss3 = W2d.getCoefficients(ws,'d',[1:2]);

lenapp = W2d.lenCoeffs(1);
vec = @(a)a(:);
norm( vec(abs(ss2)) - vec(abs(ws(1:lenapp))) ) / norm(abs(ws(1:lenapp)))

norm( abs([vec(ss2);vec(ss1)]) - vec(abs(ws) )) / norm(abs(ws))
figure, plot(abs(ss2(:)))
%%



%% Applying 1D wavelet transform in 2D image

im = double(imread('cameraman.tif'));

W1d = WaveletObj(size(im),1,'db4',[],[1,0]);

ws1 = W1d*im;
imrec = W1d'*ws1;

figure, imshow([im,imrec],[]); 
disp(strcat('error between images:',num2str(norm(im(:)-imrec(:))/norm(im(:))),'%'));
% 
% W2d.getBook()
% ss2 = W2d.getCoefficients(ws,'app',2);
% ss1 = W2d.getCoefficients(ws,'all',1);
% ss3 = W2d.getCoefficients(ws,'d',[1:3]);
%% 3D

im3D = repmat(im,[1,1,256]);
W3d = WaveletObj(size(im3D),3,'db4','ppd');

ws = W3d*im3D;

rec3 = W3d'*ws;
figure, imshow(squeeze(rec3(128,:,:)),[]);
figure, imshow(squeeze(rec3(:,:,128)),[]);
W3d.getBook()
%% Testing coefficient extraction
% checked
ssapp = W3d.getCoefficients(ws,'app',[]);
aaaa = ws{1}.dec{1} + ws{2}.dec{1};

norm(vec(ssapp) - vec(aaaa))
%% Extracting all levels at once

coeffs = (W3d.cell2num(W3d.getCoefficients(ws,'all',[])));
%%
%% checked
ss1 = W3d.getCoefficients(ws,'det',1);
ss2 = W3d.getCoefficients(ws,'det',2);
ss3 = W3d.getCoefficients(ws,'det',3);
 
ss37 = W3d.getCoefficients(ws,'det',3,7); %j=8
ss11 = W3d.getCoefficients(ws,'det',1,1); %j=16
ss17 = W3d.getCoefficients(ws,'det',1,7); %j=22
ss21 = W3d.getCoefficients(ws,'det',2,1); %j=9

norm(ss3{7}(:) - ss37(:))
norm(ss1{1}(:) - ss11(:))
%% checked
ssall = vec( abs( W3d.cell2num(W3d.getCoefficients(ws,'all',[])) ) );

aaaa = [];
for i = 1 : 22
    aaaa = [aaaa;  abs( vec( ws{1}.dec{i}(:) ) + 1i*vec(ws{2}.dec{i}(:)) ) ];
end
norm(ssall - aaaa)

%% Applying 2D wavelet transforms in a 3D case

W2d = WaveletObj(size(im3D),3,'db4',[],[1,1,0]);
%
ws2 = W2d*im3D;
%%
imrec = W2d'*ws2;

%figure, imshow([im,imrec],[]); 
disp(strcat('error between images:',num2str(norm(im3D(:)-imrec(:))/norm(im(:))),'%'));
%% Testing automatic level 3D

W3t = WaveletObj(im3D,'automatic','db4','ppd');  % cameraman
%% switching GPU and Parallel modes
% working and independent between objects. However, subclasses need to
% unify GPU and CPU performance 

W3d.switchParallel()
W3d.switchGPU()
W3d.printSystem()
W2d.printSystem()
%clc
W3d.switchParallel()
W2d.printSystem()