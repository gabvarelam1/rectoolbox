classdef IdentityObj < handle
    %IDENTITYOBJ dummy object to do nothing when included in the model
    
    properties
        adjoint = 0;
        decompositionLevels = 1;
    end
    
    methods
        function obj = IdentityObj(~)
            %IDENTITYOBJ Construct an instance of this class
        end
        
        function outputArg = mtimes(~,a)
            outputArg = a;
        end
    end
end

