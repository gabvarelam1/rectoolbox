classdef MaskingObj < handle

%%% MaskingObj is a very similar class as SamplingObj.
%%% However, the big difference is that MaskingObj is thought
%%% To eliminate high-frequency perturbations that occur during the reconstruction process, and which are easy to identify and to remove.
%%% For example: Extrapolation of spirals in k-space, and Extrapolation of Compressed Sensing Reconstruction in both domains.

%%% You can interpret MaskingObj as a soft filter for results. Alternatively, you could include it in the encoding model to Suppress obvious errors.

%%% If you choose 'k-space' the class transforms the data into frequency space and applies the mask of your choice to return it to k-space. 
%%% If you choose 'image' the class applies the filter assuming that we are already in image space.

%%% If you use the 'k-space' option, the Fourier Transform assumes the size of the mask as the dimensions for it,
%%% and additional dimensions are just multiplied with bsxfun.

    properties (Access=public)
        filter_mask       = [];
        mask_size         = [];
        fourierT          = [];
    end

    properties (Access=private,Hidden=true)
    end
    
    methods (Access = public)
        function obj = MaskingObj(Mask,Domain,varargin)

            flag3d = 0;
            if (numel(Mask) > 1) && (numel(Mask) < 4) 
                % it assumes that Mask is really a vector and you want to
                % create an Elliptical mask in k-space. if data is in 3D it applies,
                % it applies the Elliptical mask in y,z. 
                ss = Mask;

                if length(Mask) == 2
                    % 2d
                    sx = ss(1);
                    sy = ss(2);
                    sstemp = ss;
                else
                    % 3d
                    % assumes that elliptical scan is in y-z
                    flag3d = 1;
                    sx = ss(2);
                    sy = ss(3);
                    sstemp = ss(2:3);
                end
                % radius
%                 R = ceil((min([sx,sy])+1)/2);
%                 % center
%                 %C = ceil(([sx,sy]+1)/2);
%                 [X,Y] = meshgrid(1:sy, 1:sx);
%                 X = X - (sy-1)/2;
%                 Y = Y - (sx-1)/2;

                radx = sx/2;
                rady = sy/2;
                    
                % centre
                if mod(sx,2) == 0
                    midX = ceil((sx-1)/2);
                else
                    midX = ceil((sx+1)/2);
                end
                
                if mod(sx,2) == 0
                    midY = ceil((sy-1)/2);
                else
                    midY = ceil((sy+1)/2);
                end
                
                % find positions
                temp = ones(sstemp,'single');
                [px,py] = find(temp==1);
                
                % remove corners
                % Rpos = sqrt((px-midX).^2 + (py-midY).^2);
                Rpos = ( (px-midX).^2 /radx^2) + ( (py-midY).^2/rady^2 );
                for i = 1 : length(Rpos)
                    %if Rpos(i) > R 
                    if Rpos(i) > 1
                        temp(px(i),py(i)) = 0;
                    end
                end
                
%                 temp = zeros([sx,sy],'single');
%                 temp(round(sqrt((X(:)).^2 + (Y(:)).^2)) <=  R) = 1;

                if ~isempty(varargin)
                    % need to program this like in my old codes
                    switch varargin{1}
                        case 'erode'
                            if length(varargin) == 1
                                se = strel("disk",3);
                            else
                                se = strel("disk",varargin{2});
                            end
                            temp = imerode(temp,se);
                        case 'dilate'
                            if length(varargin) == 1
                                se = strel("disk",1);
                            else
                                se = strel("disk",varargin{2});
                            end
                            temp = imdilate(temp,se);
                    end
                end
                
                if flag3d
                    obj.filter_mask = repmat(permute(temp,[3,1,2]),[ss(1),1,1]);
                else
                    obj.filter_mask = temp;
                end
                clear temp; clear flag3d;

                obj.mask_size = size(obj.filter_mask);
                fourierDims = 1 : numel(obj.mask_size);
                obj.fourierT = FourierObj([fourierDims]);

            else
                % if varargin is empty, the mask is literally the variable 
                % Mask. if not, it adjusts the "Mask" variable according to your
                % preferences.
                if ~isempty(varargin)
                    % need to program this like in my old codes
                    switch varargin{1}
                        case 'erode'
                            if length(varargin) == 1
                                se = strel("disk",3);
                            else
                                se = strel("disk",varargin{2});
                            end
                            obj.filter_mask = imerode(Mask,se);
                        case 'dilate'
                            if length(varargin) == 1
                                se = strel("disk",1);
                            else
                                se = strel("disk",varargin{2});
                            end
                            obj.filter_mask = imdilate(Mask,se);
                    end
                else
                    obj.filter_mask = Mask;
                end
                
                obj.mask_size = size(obj.filter_mask);
    
                switch Domain
                    case 'k-space'
                        fourierDims = 1 : numel(obj.mask_size);
                        obj.fourierT = FourierObj([fourierDims]);
                    case 'image'
                        obj.fourierT = [];
                    otherwise
                        error('');
                end
            end
        end

        function res = mtimes(obj,b)
            if isempty(obj.fourierT)
                res = bsxfun(@times,obj.filter_mask,b);
            else
                res = obj.fourierT'*(bsxfun(@times,obj.filter_mask,(obj.fourierT*b)));
            end
        end
        function printS(obj,varargin)
            n = obj.mask_size;
            n(n == 1) = [];
            switch numel(n)
                case 1
                    figure, plot(mask);
                case 2
                    figure, imshow(obj.filter_mask);
                case 3
                    ss = size(obj.filter_mask);
                    mid = ceil((ss+1)/2);
                    if isempty(varargin)
                        figure, imshow(squeeze(obj.filter_mask(:,:,mid(3))),[]);
                    else
                        switch varargin{1}
                            case 1
                                figure, imshow(squeeze(obj.filter_mask(mid(1),:,:)),[]);
                            case 2
                                figure, imshow(squeeze(obj.filter_mask(:,mid(2),:)),[]);
                            case 3
                                figure, imshow(squeeze(obj.filter_mask(:,:,mid(3))),[]);
                            otherwise
                                warning('printing 3rd dimension');
                                figure, imshow(squeeze(obj.filter_mask(:,:,mid(3))),[]);
                        end
                    end
                otherwise
                    warning('NOT IMPLEMENTED');
            end
        end
    % end Access public
    end

end