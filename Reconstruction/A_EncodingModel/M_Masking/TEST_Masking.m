%clear all; close all; clc;

%% test Masking Obj
%% Tests in 2D
im = single(imread('cameraman.tif'));
figure, imshow(im,[]);

ss = size(im);
mid = ceil((ss+1)/2);
ext = 32;
mask = zeros(ss,'single');
mask(mid(1)-ext : mid(1)+ext , mid(2)-ext : mid(2)+ext) = 1;

M = MaskingObj(mask,'image');

% test 0: print mask
M.printS();
%% tests on image domain
% test 1: image domain
filt1 = M*im;
figure, imshow(filt1,[])
% test 2: image domain erode by 10
M = MaskingObj(mask,'image','erode',10);
filt2 = M*im;
% test 3: image domain dilate by 20
M = MaskingObj(mask,'image','dilate',20);
filt3 = M*im;

figure, imshow([filt2,filt1,filt3],[])
%% test on k-space
% test 4: k-space domain any mask
M = MaskingObj(mask,'k-space');
M.printS();

filt4 = M*im;

M = MaskingObj(mask,'k-space','dilate', 50);
M.printS();
filt5 = M*im;

figure, imshow(abs([filt4,im,filt5]),[])

M = MaskingObj(size(im),'k-space','erode',20);
M.printS();

M = MaskingObj(size(im),'k-space');
M.printS();
%% Tests in 3D

im3d = repmat(permute(im,[3,1,2]),[256,1,1]);
mask3d = repmat(permute(mask,[3,1,2]),[256,1,1]);

M = MaskingObj(mask3d,'image');

% test 0: print mask
M.printS();
M.printS(1);
M.printS(2);
M.printS(3);
M.printS(4);
%% tests on image domain
% test 1: image domain
filt1 = M*im3d;

vism = @(im) squeeze(im(mid(1),:,:));

figure, imshow(vism(filt1),[])
% test 2: image domain erode by 10
M = MaskingObj(mask3d,'image','erode',10);
filt2 = M*im3d;
% test 3: image domain dilate by 20
M = MaskingObj(mask3d,'image','dilate',20);
filt3 = M*im3d;

figure, imshow([vism(filt2),vism(filt1),vism(filt3)],[])
%% test on k-space
% test 4: k-space domain any mask
M = MaskingObj(mask3d,'k-space');
filt4 = M*im3d;

M = MaskingObj(mask3d,'k-space','dilate', 50);
M.printS();
filt5 = M*im3d;

figure, imshow(abs([vism(filt4),vism(im3d),vism(filt5)]),[])


%% Elliptical Filter

M = MaskingObj(size(im),'k-space');
M.printS();

M = MaskingObj(size(im3d),'k-space','dilate',30);
M.printS();
M.printS(1);
M.printS(2);
M.printS(3);
M.printS(4);
M = MaskingObj(size(im3d),'k-space','erode',20);
M.printS();
M.printS(1);
