clear all; close all; clc
addpath(genpath('./'));

st =load("vol_test_vectorize.mat");
size(st.vol)
%%
im = st.vol;
figure, imshow(abs(squeeze(im(:,105,:,3))),[]);
%%
V = VectorizeObj(size(im),[1,1,1,0]);

res2 = V*im;
size(res2)
imrec = V'*res2;
%%
figure, imshow([abs(squeeze(im(:,105,:,3))),...
                abs(squeeze(imrec(:,105,:,3)))],[]);

V2 = VectorizeObj(size(im),[1,1,0,0]);

res2 = V2*im;
size(res2);
imrec = V2'*res2;
figure, imshow([abs(squeeze(im(:,105,:,3))),...
                abs(squeeze(imrec(:,105,:,3)))],[]);
%%
V3 = VectorizeObj(size(im),[1,1,0,0],'reverse');
%%
size(res2)
im3 = V3*res2;
%%
size(im3)
im4 = V3'*im3;
size(im4)