classdef VectorizeObj < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties 
        matrixSize      = [];
        adjoint         = 0;
        reverseMode     = 0;
        lastIndex
        toVec
    end
    
    methods (Access = public)
        function obj = VectorizeObj(OriginalMatrixSize,Dims2Vectorize,varargin)
            % Dims2Vectorize = [1,1,1, 0 ... end];
            % matrixSize = [ 208,208,208,600]
            if isempty(varargin)
                obj.adjoint = 0;
                obj.reverseMode = 0;
            elseif strcmp(varargin{1},'reverse')
                    obj.reverseMode = 1;
            else
                error('incorrect initilization of constructor - VectorizeObj');
            end
            obj.matrixSize  = OriginalMatrixSize;

            obj.lastIndex = find(Dims2Vectorize,1,'last');
            obj.toVec = prod(obj.matrixSize(1:obj.lastIndex));

        end
        
        function  res = mtimes(obj,a)
            if ~obj.reverseMode
                if obj.adjoint
                    res = reshape(a,obj.matrixSize);
                    obj.adjoint = xor(obj.adjoint,1);
                else
                    if obj.lastIndex ~= length(obj.matrixSize)
                        res = reshape(a,[obj.toVec,obj.matrixSize(obj.lastIndex+1:end)]);
                    else
                        res = reshape(a,[obj.toVec,1]);
                    end
                end
            else
                if obj.adjoint
                    if obj.lastIndex ~= length(obj.matrixSize)
                        res = reshape(a,[obj.toVec,obj.matrixSize(obj.lastIndex+1:end)]);
                    else
                        res = reshape(a,[obj.toVec,1]);
                    end
                    obj.adjoint = xor(obj.adjoint,1);
                else
                    res = reshape(a,obj.matrixSize);   
                end
            end
        end

        function res = ctranspose(obj)
            obj.adjoint = xor(obj.adjoint,1);
            res = obj;
        end
    end
end

