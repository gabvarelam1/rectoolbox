clear all; close all; clc

im = double(imread('cameraman.tif'));

F = FourierObj([1,2]);

ksp = F*im;

figure, imshow(log(abs(ksp)+eps),[])

imrec = F'*ksp;

figure, imshow([abs(im),abs(imrec)],[])