classdef FourierObj < handle
    %FOURIEROBJ Summary of this class goes here
    % FourierObj applies the forward and the inverse Fourier transform in
    % the dimensions specified by the user during construction of the
    % object. 
    
    %   Detailed explanation goes here
    
    properties (Access = public)
        dimension2FFT   = [];
        adjoint         = 0; % initialization
    end
    
    methods
        function obj = FourierObj(dims2fft)
            %FOURIEROBJ Construct an instance of this class
            
            % dims2fft is a vector specifying in which dimensions you want
            % to apply the Fourier transform. 
            % adjoint defines if we want to do the forward or the inverse
            % Fourier transform. 
            
            obj.dimension2FFT = dims2fft;
        end
    end
end

