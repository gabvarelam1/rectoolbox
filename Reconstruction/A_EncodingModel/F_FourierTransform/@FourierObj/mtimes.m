function [res] = mtimes(a,b)


%res.fft1c = @(im,dim) (1/sqrt(size(im,dim)))*fftshift(fft(ifftshift(im,dim),[],dim),dim);
%res.ifft1c = @(ksp,dim) sqrt(size(im,dim))*fftshift(ifft(ifftshift(im,dim),[],dim),dim);
%res.dimensions = Dimensions2FFT;

fft1c = @(im,dim) (1/sqrt(size(im,dim)))*fftshift(fft(ifftshift(im,dim),[],dim),dim);
ifft1c = @(ksp,dim) sqrt(size(ksp,dim))*fftshift(ifft(ifftshift(ksp,dim),[],dim),dim);
    
    if a.adjoint
        res = b;
        for i = fliplr(a.dimension2FFT)
            res = ifft1c(res,i);
        end
        a.adjoint = 0;
        %a.adjoint = xor(a.adjoint,1); % go back to zero
    else
        res = b;
        for i = a.dimension2FFT
            res = fft1c(res,i);
        end
    end
end