function res = times(a,b)
    % My interpretation from this link
    % https://www.mathworks.com/matlabcentral/answers/77832-does-1-2-call-times-or-mtimes
    % is that times is the function in charge of a .* b
    % to avoid issues (and because I'm not thinking on amplifying coefficents)
    % I'll redirect it to mtimes.
    
    % gab 2021
    
    res = mtimes(a,b);
end