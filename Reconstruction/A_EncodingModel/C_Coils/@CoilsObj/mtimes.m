function res = mtimes (a, x)

if a.adjoint
    %res = sum(x .* conj(a.coil_maps),length(a.size_data)+1);
    res = sum(x .* conj(a.coil_maps),length(a.size_data)+1) ./ ...
        (eps + sum(abs(a.coil_maps).^2,length(a.size_data)+1));
    
    a.adjoint = xor(a.adjoint,1);
else
%     res = zeros(size(a.coil_maps));
%     switch a.number_dimensions
%         case 2
%             for i = 1 : a.number_channels
%                 res(:,:,i) = a.coil_maps(:,:,i) .* x;
%             end
%         case 3
%             for i = 1 : a.number_channels
%                 res(:,:,:,i) = a.coil_maps(:,:,:,i) .* x;
%             end
%     end
      res = bsxfun(@times,x,a.coil_maps);
end