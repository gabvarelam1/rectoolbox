classdef CoilsObj < handle
    properties (Access = private)
        adjoint             = 0;
        coil_maps           = [];
        size_data           = [];
        number_dimensions   = [];
        number_channels     = [];
    end
    
    methods (Access = public)
        function obj = CoilsObj(coils,varargin)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            s = size(coils);
            obj.coil_maps = coils;
            obj.size_data = s(1:end-1);
            obj.number_dimensions = length(s(1:end-1));
            obj.number_channels = s(end);
        end

        function res = getAllCoils(obj)
            res = obj.coil_maps;
        end

        function res = getCoiln(obj,n)
            res = obj.coil_maps([obj.size_data,n]);
        end
        
    end
end

