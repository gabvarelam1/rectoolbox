classdef SamplingObj < handle
    
    properties (Access=public)
        sampling_mask       = [];
        adjoint             = 0;
        matrixSize          = []; 
        multiPatternFlag    = 0;
    end

    properties (Access=private,Hidden=true)
    end
    
    methods (Access = public)
        function obj = SamplingObj(Mask,varargin)
            %SAMPLINGOBJ Construct an instance of this class
            %   Detailed explanation goes here
            
            % varargin{1} accepts the original size problem
            if ~isempty(varargin)
                obj.multiPatternFlag = 1;
                obj.matrixSize = varargin{1};
                if length(varargin{1}) == length(size(Mask))+2
                    % assuming difference from the readout direction x.
                    Mask = permute(Mask,[length(size(Mask))+1,1:length(size(Mask))]);
                end
            end

            obj.sampling_mask = Mask;
        end

        function printS(obj)
            figure,
            midx = ceil((size(obj.sampling_mask,1)+1)/2);
            % TODO: adapt to 2D/3D sampling masks
            if ~obj.multiPatternFlag
                
                imshow(squeeze(obj.sampling_mask(:,:,1)),[],'init',400);
            else 
                numPatterns = size(obj.sampling_mask,length(size(obj.sampling_mask)));
                for i = 1 : numPatterns
                    subplot(1,numPatterns,i)
                    imshow(squeeze(obj.sampling_mask(:,:,i)),[]);
                end


            end
        end


    end

    methods (Access = private, Hidden = true)
        function strDims = createStringDimensions(~,L)
            strDims = '';
            for i = 1 : L
                strDims = strcat(strDims,' : ,');
            end
            strDims(end) = [];
        end
    end
end

