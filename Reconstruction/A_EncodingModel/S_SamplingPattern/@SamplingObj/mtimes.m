function [res] = mtimes(a,b)
  
    if a.multiPatternFlag == 0
        res = bsxfun(@times,a.sampling_mask,b);
    else
        smtx = size(a.sampling_mask);
        omtx = a.matrixSize;
        L = omtx(end);
        stry = a.createStringDimensions(length(omtx)-1);
        strs = a.createStringDimensions(length(smtx)-1);   
        res = zeros(size(b));
        for i = 1 : L
            eval(strcat(...
            'res(',stry,',i) = bsxfun(@times,a.sampling_mask(',strs,',i),b(',stry,',i));'));
        end

    end
end