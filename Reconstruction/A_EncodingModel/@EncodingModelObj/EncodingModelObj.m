classdef EncodingModelObj < handle & SBEObj

    properties (Access = public)
        stringForwardModel
        stringInverseModel
        forwardModel
        inverseModel
        cartesianFlag = true;
        encodingComponents = [];
    end
    properties (Access = public, Constant)
        list_encoding_model_operators = ["S","F","C","Rz","P","V","M","Fi","Fjk","Vx","Vy","Rp",'Rpi',""];
        operator_demands_input        =  [1,  0,  1,  1,  1,  0,  1,   0,   0,    1,   1,   1, 1];
        operator_names  = {'mask','fourier','coils','resize','psf','vectorize','masking','fourier1','fourier23','vecImg','vecKsp','repmat','repinverse','empty'};
    end
    methods (Access = public)
        function obj = EncodingModelObj(ForwardModelasSTRING,StringSamplingStyle)

            obj@SBEObj();

            %disp(strcat('Creating Encoding Model Operator'));
            
            obj.checkEncodingModel(ForwardModelasSTRING);

            obj.stringForwardModel = obj.defineTransform('forward');
            obj.stringInverseModel = obj.defineTransform('inverse');

            if strcmp(StringSamplingStyle,'Cartesian')
                obj.cartesianFlag = true;
            elseif strcmp(StringSamplingStyle,'Wave')
                obj.cartesianFlag = true;
            else
                obj.cartesianFlag = false;
            end
        end

        function defineA(obj,function_handle_forward_model)
            obj.forwardModel = function_handle_forward_model;
        end

        function defineAt(obj,function_handle_inverse_model)
            obj.inverseModel = function_handle_inverse_model;
        end

        function res = A(obj,data)
            res = obj.forwardModel(data);
        end

        function res = At(obj,data)
            res = obj.inverseModel(data);
        end

        function evalComponent(obj,num,emptyFlag)
            flags = strcmp(obj.list_encoding_model_operators,obj.encodingComponents{num}); 
            pos = find(flags);
            if obj.operator_demands_input(pos) ~= (~emptyFlag)
                error(strcat('Operator ',obj.encodingComponents{num}, 'expects input data'));
            end
            
        end
        function string2createOperator = createOperator(obj,code,varargin)
            %list_encoding_model_operators = {'S','F','C','R','P','V','M','Fi','Fjk'};
            % operator_demands_input        =  [1,  0,  1,  1,  1,  0,  1,   0,   0];
            % operator_names  = {'sampling','fourier','coils','resize','psf','vectorize','mask','fourier1','fourier23'};
            str_ = [];
            switch code
                case 'S'
                    % SamplingObj(Mask,MultiPatternFlag)
                    switch length(varargin)
                        case 1
                            str_ = strcat('S = SamplingObj(data.S);');
                        case 2
                            str_ = strcat('S = SamplingObj(data.S,matrixSize);');
                    end
                case 'F'
                    % FourierOb([ dimensions2applyTransform] );
                    str_ = strcat('F = FourierObj(',obj.vec2str(varargin{1}),');');
                case 'C'
                    str_ = strcat('C = CoilsObj(data.C);');
                case 'Rz'
                    str_ = strcat('Rz = ResizingObj(matrixSize, data.Rz);');
                case 'P'
                    str_ = strcat('P = PSFObj(data.P);');
                case 'V'
                    % VectorizeObj(OriginalMatrixSize,Dims2Vectorize)
                    str_ = strcat('V = VectorizeObj(matrixSize,',obj.vec2str(varargin{1}),');');
                case 'M'
                    str_ = strcat('M = MaskingObj(data.M);');
                case 'Fi'
                    str_ = strcat('Fi = FourierObj([1]);');
                case 'Fjk'
                    str_ = strcat('Fjk = FourierObj([2,3]);');
                case 'Vx'
                    str_ = strcat('Vx = VectorizeObj(matrixSize,',obj.vec2str(varargin{1}),');');
                case 'Vy'
                    str_ = strcat('Vy = VectorizeObj(matrixSize,',obj.vec2str(varargin{1}),",'reverse');");
                case 'Rp'
                    % RepmatObj(originalMatrixSize,vector4repmat)
                    str_ = strcat('Rp = RepmatObj(matrixSize,',obj.vec2str(varargin{1}),');');
                case 'Rpi'
                    str_ = strcat('Rpi = RepmatObj(matrixSize,',obj.vec2str(varargin{1}),',"inverse");');
                otherwise
                    error('non existent code to create string for operator. Please add it :)');
            end
            string2createOperator = str_;
        end
    end

    methods (Access = private)

        function checkEncodingModel(obj,string_)
            % this function de-constructs the encoding model to make user
            % that it is valid for the current version of the toolbox.

            if ~strcmp(string_,'')
                i = 1;
                n = length(string_);
                
                cc = 1;
    
                upcase = obj.isuppercase(string_);
                lwcase = obj.islowercase(string_);
                anytransp = obj.istransp(string_);
    
                tempComponents = cell(sum(upcase),1);
                
                if sum(double(or(or(upcase,lwcase),anytransp))) ~= n
                    error('encoding model only accepts lower - uppers and transpose characters');
                end
    
                if ~obj.isuppercase(string_(1))
                    error('encoding model must start with uppercase character');
                end
    
                while i <= n
                    start = i;
                    stop = find(upcase(start+1 : end),1,"first")-1;
                    if isempty(stop)
                        if upcase(end) == 0
                            stop = n - start;
                        else
                            stop = 0;
                        end
                    end
                    temp = string_(start : (start + stop));
    
                    if sum(strcmp(temp,obj.list_encoding_model_operators)) == 0
                        error('Inexistant operator for new encoding model');
                    else
                        tempComponents{cc} = temp; 
                        cc = cc + 1;
                    end
                    i = i + stop + 1;
                end
                cc = cc-1;
                %disp('Valid encoding model... :)');
                obj.encodingComponents = cell(cc,1);
                for j = 1 : cc
                    obj.encodingComponents{j} = tempComponents{j};
                end
                clear tempComponents;
            else
                obj.encodingComponents = cell(1,1);
                obj.encodingComponents{1} = ''; 
            end
%             while i <= n
%                 if strcmp(string_(i),'F')
%                     if (i+2 <= n)
%                         if ((strcmp(string_(i : i + 2),'Fjk'))  )
%                             temp = 'Fjk';
%                             i = i + 2;
%                         elseif ((strcmp(string_(i : i + 1),'Fi')) )
%                             temp = 'Fi';
%                             i = i + 1;
%                         end
%                     elseif (i+1 <= n)
%                         if ((strcmp(string_(i : i + 1),'Fi')) )
%                             temp = 'Fi';
%                             i = i + 1;
%                         else
%                             temp = string_(i);
%                         end
%                     else
%                         temp = string_(i);
%                     end
%                 else
%                     temp = string_(i);
%                 end
% 
%                 if sum(strcmp(temp,obj.list_encoding_model_operators)) == 0
%                     error('Inexistant operator for new encoding model');
%                 else
%                     tempComponents{cc} = temp; 
%                     cc = cc + 1;
%                 end
%                 i = i + 1;
%             end
%             cc = cc-1;
%             disp('Valid encoding model... :)');
%             obj.encodingComponents = cell(cc,1);
%             for j = 1 : cc
%                 obj.encodingComponents{j} = tempComponents{j};
%             end
%             clear tempComponents;
        end

        function Transform = defineTransform(obj, FoI)%,ForwardModelasSTRING)
    
            m = '*';
            s1 = '(';
            s2 = ')';
            a = "'";
            if ~isempty(obj.encodingComponents{1})
                if strcmp(FoI,'forward')
                    beg = '@(im)';
                    string_ = 'im';
                    for i = length(obj.encodingComponents) : -1 : 1
                    % Transform = @(im) S*F*C
                        string_ = strcat(s1,obj.encodingComponents{i},m,string_,s2);
                    end
        
                elseif strcmp(FoI,'inverse')
                    beg = '@(ksp)';
                    string_ = 'ksp';
                    for i = 1 : length(obj.encodingComponents)
                        % Transform = @(ksp) C'*F'*S'
                      string_ = strcat(s1,obj.encodingComponents{i},a,m,string_,s2);
                   
                    end    
        
                end
                Transform = strcat(beg,string_,';');
            else
                if strcmp(FoI,'forward')
                    beg = '@(im)';
                    string_ = 'im';
                else
                    beg = '@(ksp)';
                    string_ = 'ksp';
                end
                Transform = strcat(beg,string_,';');
            end
        end

    end

end

