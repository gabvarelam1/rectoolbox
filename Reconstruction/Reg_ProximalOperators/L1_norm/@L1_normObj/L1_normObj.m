classdef L1_normObj < handle & RegCoreObj
    % classdef nameclass < handle allows you to update properties inside an
    % object. More info at
    % https://www.mathworks.com/help/matlab/matlab_oop/comparing-handle-and-value-classes.html

    % L1_NORMOBJ is a subclass extension of the superclass RegCoreObj
    % to tackle specific specs regarding the L1-norm regularization:

    %                R{x} = \| \Psi x \|_1
    %               prox = softhresholding function
    
    properties
        % Properties defined on superclass
        hackWavelets                = false;
        L1                          = @(x_) sum(norm(x_(:),1));
    end
    
    methods (Access = public)
        function obj = L1_normObj(Transform_,RegWeightingMethod_,PreEncoder,varargin)
            %L1_NORMOBJ Construct an instance of this class
            %   Detailed explanation goes here

            % call superclass constructor
            % RegCoreObj(Transform,RegWeighteingMethod,RegWeightScaling,varargin)
            obj@RegCoreObj(Transform_, RegWeightingMethod_,PreEncoder,varargin);
            
            
            if isa(Transform_,'WaveletObj')
                if strcmp(PreEncoder{1},'ConcatenateObj')
                    obj.hackWavelets = true;
                end
            end

        end

        function res = proximalOperation(obj,data_,alphaFactor_)
            % this method defines
            % T^-1 * S_\lambda{ T*x }
            
            if ~obj.hackWavelets
                res = obj.preEncoding'*(obj.psi'*( obj.softThresholding(...
                    obj.psi * (obj.preEncoding * data_), alphaFactor_)));
            else
                temp = obj.psi * (data_);
                %res = cell(size(temp));
                for i = 1 : length(temp)
                    temp{i} = obj.softThresholding(temp{i},alphaFactor_);
                end
                res = (obj.psi'*(temp));
            end
            
        end

        function cost = costFunction(obj,data_)

            if obj.isPsi('WaveletObj')
                if ~obj.hackWavelets
                    cost = obj.runWaveletCost( obj.preEncoding * data_);
                else
                    cost = obj.runWaveletCost(data_);
                end
            else
                %%%
            end

        end

        function res = softThresholding(obj,ComplexData, Alpha)
            %method sofThresholding computes the softThresholding function
            % for obj.Psi * x coefficients
            
            % TODO: reformulate
            %if length(obj.RegularizationWeighting) == 1
            %    Lambda = obj.RegularizationWeighting * Alpha;
            %    res =  ( abs(ComplexData) > Lambda) .* ...
            %        (exp( 1i*angle(ComplexData) ) .* ( abs(ComplexData) - Lambda))  ;
            %else

            if obj.isPsi('WaveletObj')

                if ~iscell(ComplexData)
                    book = obj.psi.getBook();
                    wL = length(obj.regularizationWeighting);
%                     lenCoeffs = cumsum(prod(book,2) .* [1;3*ones(wL,1)]);
%                     lenCoeffs(end) = length(ComplexData);

                    res = ComplexData;

                    % low-frequency wavelet coefficients
                    lambda = obj.regularizationWeighting(1) * Alpha;
                    range_ = 1 : (obj.psi.lenCoeffs(1)-1);
                    res(range_) =  ( abs(res(range_)) > lambda) .* ...
                            (exp( 1i*angle(res(range_)) ) .* ( abs(res(range_)) - lambda))  ;

                    % high-frequency wavelet coefficients
                    for i = 2 : wL % app are attended depending on the regularization weighting
                        lambda = obj.regularizationWeighting(i) * Alpha;
                        %%% CHECK HERE
                        range_ = obj.psi.lenCoeffs(i-1)+1 : obj.psi.lenCoeffs(i);
                        
                        res(range_) =  ( abs(res(range_)) > lambda) .* ...
                            (exp( 1i*angle(res(range_)) ) .* ( abs(res(range_)) - lambda))  ;
                    end
                else

                    % 3D based on cell data type
                    res = ComplexData;
                    maps_per_level = 7;
                    L = obj.psi.decompositionLevels;

                    % low-frequency wavelet coefficients (app coeffs)
                    lambda = obj.regularizationWeighting(1) * Alpha;
                    iter_dat =  obj.psi.getCoefficients(...
                                    ComplexData, 'app', L);
                    filt_dat = ( abs(iter_dat) > lambda) .* ( exp( 1i*angle(iter_dat) ) .* ( abs(iter_dat) - lambda))  ;

                    res{1}.dec{1} = real(filt_dat);
                    res{2}.dec{1} = imag(filt_dat);


                    % high-frequency wavelet coefficients
                    for i = 1 : L

                        lambda = obj.regularizationWeighting(1+i) * Alpha;

                        for j = 1 : maps_per_level

                            iter_dat =  obj.psi.getCoefficients(...
                                            ComplexData, 'det' , i , j );
                            filt_dat = ( abs(iter_dat) > lambda) .* ( exp( 1i*angle(iter_dat) ) .* ( abs(iter_dat) - lambda))  ;

                            pos = maps_per_level * (L - i) + j + 1; % + app
                            res{1}.dec{pos} = real(filt_dat);
                            res{2}.dec{pos} = imag(filt_dat);

                        end
                    end
                end
            end
            %end
        end

        function determineRegWeighting(obj,data_)
            
            if strcmp(obj.regWeightingStyle,'automatic')
                switch obj.classPsi()
                    case 'WaveletObj'
                        obj.runWaveletForm(data_);
                    case 'IdentityObj'
                        %obj.runIdentityForm(data_);
                    otherwise
                        error('hehe');
                end
            else
                % regularization weighting selection is manual
                %obj.regularizationWeighting = zeros(L+1,1);
                % skip because in manual lambda was already defined during
                % construction of object.
            end
            
        end


%         function updateRegularizationWeighting(obj, data_)
%             %%% in case that you iteratively want to update the value of
%             %%% lambda.
%             obj.regularizationWeighting = obj.defineRegWeighting(data_);
% 
%         end
% 
%         function updateRegularizationMethod(obj, strMethod)
%             obj.regWeightingMethod = strMethod;
%         end

        function updatePsi(obj, newTransform)
            obj.psi = obj.psyType(newTransform);
        end

        function res = getLambdaDet(obj)
            res = obj.lambdaDet;
        end
        
    end

    methods (Access = public)
        function runWaveletForm(obj, data_)

           %waveletCoefficients = obj.psi * data_;
           %%% METHODS USE TEMPORAL PSI CREATED IN HERE. THIS IS WITH THE
           %%% PURPOSE TO AVOID ISSUES BETWEEN CONCATENATED AND SINGLE
           %%% CONTRAST RECONSTRUCTIONS. CONCATENATED CONTRASTS ESTIMATE
           %%% THE REGULARIZATION FROM THE CONCATENATED SPACE ONCE, AND
           %%% DURING THE ITERATIVE RECONSTRUCTION PROCESS, IT APPLIES A
           %%% WAVELET TO THE SPECIFIC SIZE OF THE CONTRAST.
           L = obj.psi.decompositionLevels;
           if obj.hackWavelets
%                switch class(obj.preEncoding)
%                    case 'IdentityObj'
               psi = obj.psi;
               waveletCoefficients = data_;
%                    case 'ConcatenateObj'
%                        psi = WaveletObj(size(data_),L,obj.psi.waveletName,obj.psi.mode);
%                        waveletCoefficients = psi * data_;
%                end
           else
               psi = WaveletObj(size(data_),L,obj.psi.waveletName,obj.psi.mode);
               waveletCoefficients = psi * data_;
           end

           if psi.numDimensions < 3
                condAll = 'all';
           else
                condAll = 'det'; % 3D
           end

           obj.regularizationWeighting = zeros(L+1,1);
           
           if strcmp(obj.regWeightingNumber,'multiple')
               switch obj.regWeightingHF
                   case 'mlambdas'
                      % 1 lambda for each high-frequency level
                       for i = 2 : L+1 % 2 to avoid app coefficients
                               switch obj.regWeightingMethod
                                   case 'double k-means'
                                       obj.regularizationWeighting(i) = ...
                                           obj.doubleKmeans((psi.cell2num(psi.getCoefficients(waveletCoefficients,psi.codes{i},psi.waveletLevels(i)))));
                                   case 'k-means'
                %                        obj.regularizationWeighting(i) = ...
                %                            obj.kmeans((obj.psi.cell2num(obj.psi.getCoefficients(waveletCoefficients,obj.psi.codes{i},obj.psi.waveletLevels(i)))));
                                       obj.regularizationWeighting(i) = ...
                                           obj.kmeans((psi.cell2num(psi.getCoefficients(waveletCoefficients,psi.codes{i},psi.waveletLevels(i)))));
                                   case 'rayleigh'
                                       obj.regularizationWeighting(i) = ...
                                           obj.rayleigh((psi.cell2num(psi.getCoefficients(waveletCoefficients,psi.codes{i},psi.waveletLevels(i)))));
                                   otherwise
                                       % identify the new function handle
                                       % or simply type it as an additional case
                               end
                       end
                   case '1lambdas'
                      % 1 lambda for all high-frequency levels
                      switch obj.regWeightingMethod
                           case 'double k-means'
                               obj.regularizationWeighting(2:L+1) = ...
                                   obj.doubleKmeans((psi.cell2num(psi.getCoefficients(waveletCoefficients,condAll,psi.waveletLevels(2:L+1)))));
                           case 'k-means'
                               obj.regularizationWeighting(2:L+1) = ...
                                   obj.kmeans((psi.cell2num(psi.getCoefficients(waveletCoefficients,condAll,psi.waveletLevels(2:L+1)))));
                           case 'rayleigh'
                               obj.regularizationWeighting(2:L+1) = ...
                               obj.rayleigh((psi.cell2num(psi.getCoefficients(waveletCoefficients,condAll,psi.waveletLevels(2:L+1)))));
                           otherwise
                               % identify the new function handle
                               % or simply type it as an additional case
                      end
                   case 'firstLevel'
                       % apply the lambda from the lowest level to all
                       % levels
                       switch obj.regWeightingMethod
                           case 'double k-means'
                               obj.regularizationWeighting(2:L+1) = ...
                                   obj.doubleKmeans((psi.cell2num(psi.getCoefficients(waveletCoefficients,condAll,psi.waveletLevels(L+1)))));
                           case 'k-means'
                               obj.regularizationWeighting(2:L+1) = ...
                                   obj.kmeans((psi.cell2num(psi.getCoefficients(waveletCoefficients,condAll,psi.waveletLevels(L+1)))));
                           case 'rayleigh'
                               obj.regularizationWeighting(2:L+1) = ...
                               obj.rayleigh((psi.cell2num(psi.getCoefficients(waveletCoefficients,condAll,psi.waveletLevels(L+1)))));
                           otherwise
                               % identify the new function handle
                               % or simply type it as an additional case
                      end

               end
           else
               % 1 single lambda for both low- and high-frequency wavelet
               % coefficients
                switch obj.regWeightingMethod
                   case 'double k-means'
                       obj.regularizationWeighting(1:L+1) = obj.doubleKmeans(...
                           (psi.cell2num(psi.getCoefficients(waveletCoefficients,'all',[]))));
                   case 'k-means'
                       obj.regularizationWeighting(1:L+1) = obj.kmeans(...
                           (psi.cell2num(psi.getCoefficients(waveletCoefficients,'all',[]))));
                   case 'rayleigh'
                       obj.regularizationWeighting(1:L+1) = obj.rayleigh( ...
                           (psi.cell2num(psi.getCoefficients(waveletCoefficients,'all',[]))));
                   otherwise
                       % identify the new function handle
                       % or simply type it as an additional case
                end


           end
        end

        function costTotal = runWaveletCost(obj, data_)

           waveletCoefficients = obj.psi * data_;
           if obj.hackWavelets
               waveletCoefficients = obj.psi.RSOS(waveletCoefficients);
           end
           L = obj.psi.decompositionLevels;

           costTotal = 0;
           for i = 1 : L+1

              costTotal = costTotal + obj.L1( ...
                  obj.regularizationWeighting(i) * obj.psi.cell2num(obj.psi.getCoefficients(waveletCoefficients,obj.psi.codes{i},obj.psi.waveletLevels(i))));
           end

        end
    % end private methods
    end
end

