classdef RegCoreObj < handle & SystemEvalObj
    %REGCOREOBJ is the super class that cares about the core of
    %regularization. 
    % Main functions:
    % i. Selection of lambda method
    % ii. Proximal Operation
    
    properties (Access = public)
        psi                                     = [];
        preEncoding                             = [];
        preEncodingTraits                       = [];
        lambdaDet                               = [];
        %%%%
        regWeightingStyle                       = 'automatic';
        regWeightingNumber                      = 'multiple';
        regWeightingHF                          = 'mlambdas';
        regWeightingMethod                      = 'k-means';
        regWeightScalingFactor                  = 1;
        %%%
        regularizationWeighting                 = 0;
    end
    properties (Access = protected, Constant , Hidden = true)
        % to obviate some stuff
        psiClasses = {'WaveletObj','IdentityObj'};
        pEncodingClass = {'IdentityObj','ConcatenateObj'};
        %lambdaCategories = {'WaveletDistribution','Manual','NRMSE'};
        lambdaStyle = {'automatic','manual'};
        lambdaNumber = {'single','multiple'};
        lambdaHighFreq = {'1lambdas','mlambdas','firstLevel'};
        % 1lambda -> 1 value for all high-frequency wavelet coefficients
        % across levels;
        % mlambda -> 1 lambda for all high-frequency wavelet coefficients
        % per level;
        coeffDistribution = {'k-means','double k-means','rayleigh'};
    end
    
    methods (Access = public)
        function obj = RegCoreObj(Transform,RegWeighteingMethod,PreEncoder,varargin)

            % super class
            obj@SystemEvalObj();
            % check Transform ... invalid class generates an error...
            % obj.psiType(Transform);
            
            % PSI
            obj.checkType('psiClasses',class(Transform)); obj.psi = Transform;
            
            % PRE-ENCODER
            % only verifies that preencoder is registered...
            obj.checkType('pEncodingClass',PreEncoder{1})
            %obj.preEncodingTraits = PreEncoder{2}; % ???? dont remember this
            obj.preEncoding = IdentityObj;
            
%             else
%                 % check if it is valid and add it
%                 obj.checkType('pEncodingClass',class(PreEncoder));
%                 obj.preEncoding = PreEncoder;
%                 %obj.pEType(PreEncoder);
%             end

            % REGULARIZATION WEIGHITING STYLE
            % example 1 {'automatic','multiple','mlambdas','k-means'};
            % example 2 {'manual','multiple',[0,1e-7,....,L]}
            % To get lambdas based on a metric it is better to just run the
            % recon multiple times and manually defined it.
            obj.updateRegularizationMethod(RegWeighteingMethod);

%             if ~isempty(RegWeightScaling)
%                 obj.regWeightScalingFactor = RegWeightScaling;
%             end

            if ~isempty(varargin)
                %obj.regWeightingMethod = RegWeighteingMethod;
                
            else
                % explore variables to change
            end
        end

        function updateRegularizationMethod(obj,RegWeighteingMethod)
            switch class(obj.psi)
                %%%% \PSI = Wavelets
                case 'WaveletObj'
                    if ~isempty(RegWeighteingMethod)
                        assert(length(RegWeighteingMethod) < 6, 'Regularization Weighting Style recognizes up to 5 variables');
                        
                        obj.checkType('lambdaStyle',RegWeighteingMethod{1}); 
                        obj.regWeightingStyle = RegWeighteingMethod{1};
            
                        switch obj.regWeightingStyle
                            case 'automatic'
                                %assert(2 < length(RegWeighteingMethod), 'Regularization Weighting Style needs at least 3 variables');
                                
                                switch length(RegWeighteingMethod)
                                    case 2
                                        obj.checkType('lambdaNumber',RegWeighteingMethod{2}); 
                                        obj.regWeightingNumber = RegWeighteingMethod{2};
                                        
                                    case 3
                                        obj.checkType('lambdaNumber',RegWeighteingMethod{2}); obj.regWeightingNumber = RegWeighteingMethod{2};
                                        obj.checkType('lambdaHighFreq',RegWeighteingMethod{3});
                                        obj.regWeightingHF = RegWeighteingMethod{3};
                                        
                                    case 4
                                        obj.checkType('lambdaNumber',RegWeighteingMethod{2}); obj.regWeightingNumber = RegWeighteingMethod{2};
                                        obj.checkType('lambdaHighFreq',RegWeighteingMethod{3}); obj.regWeightingHF = RegWeighteingMethod{3};
                                        obj.checkType('coeffDistribution',RegWeighteingMethod{4});
                                        obj.regWeightingMethod = RegWeighteingMethod{4};
                                        
                                    case 5
                                        obj.checkType('lambdaNumber',RegWeighteingMethod{2}); obj.regWeightingNumber = RegWeighteingMethod{2};
                                        obj.checkType('lambdaHighFreq',RegWeighteingMethod{3});obj.regWeightingHF = RegWeighteingMethod{3};
                                        obj.checkType('coeffDistribution',RegWeighteingMethod{4});obj.regWeightingMethod = RegWeighteingMethod{4};
                                        assert(isnumeric(RegWeighteingMethod{5}),'fifth position for automatic determination of lambda must be numeric for re-scaling result');
                                        if ~isempty(RegWeighteingMethod{5})
                                            obj.regWeightScalingFactor = RegWeighteingMethod{5};
                                        end
                                end
        
                            case 'manual'
                                assert(2 == length(RegWeighteingMethod), 'Regularization Weighting Style needs 2 variables');
                                assert(isnumeric(RegWeighteingMethod{2}),'When manual is selected second position of regWeighting style must be numeric');
                                L = obj.psi.decompositionLevels;
                                if length(RegWeighteingMethod{2}) ~= (L+1)
                                    if length(RegWeighteingMethod{2}) == 1
                                        obj.regularizationWeighting = repmat(RegWeighteingMethod{2},[1,L+1]);
                                    else
                                        error('manual regularization weighting must be 1 number or a vector equal to the number of decomposition levels from psi.')
                                    end
                                else
                                    obj.regularizationWeighting = RegWeighteingMethod{2};
                                end
                                obj.regWeightingNumber     = [];
                                boj.regWeightingHF         = [];
                                obj.regWeightingMethod     = [];
                                obj.regWeightScalingFactor = 1;
                        end
                    end
                    obj.lambdaDet = {   obj.regWeightingStyle,...                       
                                        obj.regWeightingNumber,...                      
                                        obj.regWeightingHF, ...                          
                                        obj.regWeightingMethod, ...                      
                                        obj.regWeightScalingFactor      };
                case 'IdentityObj'
                    %%%% \PSI = Identity
                    switch obj.regWeightingStyle
                        case 'automatic'
                            obj.regularizationWeighting = RegWeighteingMethod{2};
                            obj.regWeightingNumber     = [];
                            boj.regWeightingHF         = [];
                            obj.regWeightingMethod     = RegWeighteingMethod{2};
                            obj.regWeightScalingFactor = 1;
                        case 'manual'
                            assert(2 == length(RegWeighteingMethod), 'Regularization Weighting Style needs 2 variables');
                            assert(isnumeric(RegWeighteingMethod{2}),'When manual is selected second position of regWeighting style must be numeric');
                            % internal
                            obj.regularizationWeighting = RegWeighteingMethod{2};
                            obj.regWeightingNumber     = [];
                            boj.regWeightingHF         = [];
                            obj.regWeightingMethod     = [];
                            obj.regWeightScalingFactor = 1;
                    end
                    % for report
                    obj.lambdaDet = {   obj.regWeightingStyle,...                       
                    obj.regWeightingNumber,...                      
                    obj.regWeightingHF, ...                          
                    obj.regWeightingMethod, ...                      
                    obj.regWeightScalingFactor      };

            end
        end
    end

    methods (Abstract = true)
        % these functions need to be defined in the corresponding
        % subclasses
         costFunction();
         proximalOperation();
         determineRegWeighting();
         getLambdaDet();
         updatePsi();
       
    end

    methods (Access = protected)

        % these functions can be called by subclasses
        function flag = isPsi(obj,type_)
            if strcmp(class(obj.psi),type_)
                flag = 1;
            else
                flag = 0;
            end
        end

        function className_ = classPsi(obj)
            className_ = class(obj.psi);
        end

        function checkType(obj,type,var)
            flag = 0;
            eval(strcat('checker = obj.',type,';'));
            for i = 1 : length(checker)
                if strcmp(checker{i},var)
                    flag = 1;
                end
            end
            assert(flag,strcat('Does not exit option:',var,'in obj.',type));
        end

        function psiType(obj,transform)
            strtype = class(transform);
            flag = 0;
            for i = 1 : length(obj.psiClasses)
                if strcmp(strtype,obj.psiClasses{i})
                    flag = 1;
                    obj.psi = transform;
                end
            end
            if ~flag
                error('Psi Transform not yet included!...');
            end
        end

        function pEType(obj,transform)
            strtype = class(transform);
            flag = 0;
            for i = 1 : length(obj.pEncodingClass)
                if strcmp(strtype,obj.pEncodingClass{i})
                    flag = 1;
                    obj.preEncoding = transform;
                end
            end
            if ~flag
                error('preEncoding Transform not yet included!...');
            end
        end
        
        function L1 = doubleKmeans(obj, coefficients)
            figure,
            vals = abs(coefficients(:));
            h = histogram(vals,256);
            first_threshold = h.BinEdges(2);
            vals(vals > first_threshold) = [];
            
            h = histogram(vals,64);
            vals = h.Values(2:end);
            [~,pos] = max(vals);
            kgroups = 2;
            map_of_groups = kmeans(vals',kgroups,"Replicates",5,'Start','uniform'); % noise-, mixed- and signal-based wavelet coefficients as clusters.
            bin_pos = find(map_of_groups == map_of_groups(pos),1,'last');
            if bin_pos == 1
                % if this is the case it means that there is a very abrupt
                % expontential describing coefficients
                % best solution is to average the initial bins
                threshold = mean(h.BinEdges(2 : 5));
            elseif (bin_pos == length(vals))
                bin_pos = round(length(vals)/2) + 1;
                threshold = h.BinEdges(bin_pos); % +1 to compensate for the removed value
            else
                bin_pos = bin_pos+1;
                threshold = h.BinEdges(bin_pos); % +1 to compensate for the removed value

            end
            L1 = obj.regWeightScalingFactor * (threshold/sqrt(2));

            close
        end

        function L1 = kmeans(obj, coefficients)
            
            h = obj.applyHistogram(abs(coefficients(:)));
            vals = h.Values(2:end);
           
            if ~isempty(vals)
                [~,pos] = max(vals);
                kgroups = 2;
                map_of_groups = kmeans(vals(:),kgroups,'Replicates',5,'Start','uniform'); % noise-, mixed- and signal-based wavelet coefficients as clusters.
                bin_pos = find(map_of_groups == map_of_groups(pos),1,'last');
                if bin_pos == 1
                    % if this is the case it means that there is a very abrupt
                    % expontential describing coefficients
                    % best solution is to average the initial bins
                    threshold = mean(h.BinEdges(2 : 5));
                elseif (bin_pos == length(vals))
                    bin_pos = round(length(vals)/2) + 1;
                    threshold = h.BinEdges(bin_pos); % +1 to compensate for the removed value
                else
                    bin_pos = bin_pos+1;
                    threshold = h.BinEdges(bin_pos); % +1 to compensate for the removed value
 
                end
           else
                threshold = mean(abs(coefficients(:)));
            end
            %L1 = obj.regWeightScalingFactor *(threshold);
            L1 = obj.regWeightScalingFactor * (threshold/sqrt(2));
        end

        function L1 = rayleigh(obj, coefficients)
            
                h = histogram(abs(coefficients),round(numel(coefficients)/100));
                vals = h.Values;
                vals(1) = 0; % The first bin can be inflated if there was masking in im0
                [~,threshInd] = max(vals);
                L1 = 0.5*(h.BinEdges(threshInd) + h.BinEdges(threshInd+1))/sqrt(2);
                %%% Tweeking a little bit lamVal to avoid
                %%% over-regularization
                L1 = obj.regWeightScalingFactor * L1/2;

        end
    end

    methods (Access = private)
        % these function can only be called inside RegCoreObj

        function [st] = applyHistogram(obj,data)
            if length(data) < 511
                N = nextpow2(length(data))-5;
                [count,bins] = obj.genHistogram(abs(data(:)),2^(N));
                st.Values = count;
                st.BinEdges = bins;
            else
                %figure,
                h = histogram(data);
                st.Values = h.Values;
                st.BinEdges = h.BinEdges(1:end);
                close
            end
        end

        function [Count, Bins] = genHistogram(~,Data, N)
            Maxd = max(Data);
            Mind = min(Data);
            Bins = linspace(Mind-.1*Mind, Maxd+.1*Maxd, N);
            
            Count = zeros(N,1);
            for iter = 2 : N
               Count(iter-1) = sum((Data >= Bins(iter-1)) .* (Data < Bins(iter)) ); 
            end
        end
    end

end

