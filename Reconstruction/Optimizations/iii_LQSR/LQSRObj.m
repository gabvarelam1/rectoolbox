classdef LQSRObj < EncodingModelObj & OptParamsObj

    properties (Access = protected)
        % EncodingModelObj takes care of function_handles A and At
        % OptParamsObj takes care of optimization parameters, results, etc
         vecKSP = [];
         vecIMG = [];
    end
    methods
        function obj = LQSRObj(forwardModelSTRING, cartesianSamplingflag, optParams_)
            %disp(strcat('Creating LQSR Object'));

            % call superclass constructor
            obj@EncodingModelObj(forwardModelSTRING, cartesianSamplingflag);
            obj@OptParamsObj(optParams_);

            obj.maxIterations = optParams_.maxIterations;
            obj.tolerance = optParams_.tolerance;
            %obj.EncodingModel = EncodingModel_;
            %obj.defineA(Forward_);
            %obj.defineAt(Inverse_);
            
            obj.OptSuccess = 0;
            obj.Iterations = [];
            obj.relativeError = [];
        end

%         function genVecKSP(obj,dims2vec,matrixSize)
%             obj.vecKSP = VectorizeObj(dims2vec,matrixSize);
%         end
% 
%         function genVecIMG(obj,dims2vec,matrixSize)
%             obj.vecIMG = VectorizeObj(dims2vec,matrixSize);
%         end

        function [resultOpt,optSettings] = run(obj, Y_, X_)
            %%% this objject solves \hat{x} = argmin_x ||Ax-y||_2^2
            %%% using the LQSR function. From X_ and Y_, we define Resizing
            %%% objects to return data to its natural matrixsize.
            param = struct;
            
            param.vy = VectorizeObj(size(Y_),ones(1,length(size(Y_))));
            param.F = obj.forwardModel;
            
            param.Fprime = obj.inverseModel;
            temp = param.Fprime(Y_);
            s = size(temp); clear temp;
            param.vx = VectorizeObj(s,ones(1,length(s)));

            [resultOpt,flag,relerror,iters] = ...
                lsqr(@LSQRdefinition,Y_(:),obj.tolerance,obj.maxIterations,...
                [],[],[], param);

            obj.OptSuccess = flag;
            obj.relativeError = relerror;
            obj.Iterations = iters;
            resultOpt = param.vx'*resultOpt;

            optSettings = obj.getOpts();
        end

    end

end

