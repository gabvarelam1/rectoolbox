function[res, tflag] = LSQRdefinition(In_ , Params ,tflag)

    if strcmp(tflag,'transp')
        res = Params.vy'*In_;
        res = Params.Fprime(res);
        res = Params.vx*res;
    else
        res = Params.vx'*In_;
        res = Params.F(res);
        res = Params.vy*res;
    end
end