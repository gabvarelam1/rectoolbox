classdef ISTAObj < handle & EncodingModelObj & OptParamsObj
    %MFISTA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        prepFlag = false;
        regularizer
    end
    
    methods
        function obj = ISTAObj(forwardModelSTRING, cartesianSamplingflag , optParamsStruct,regularizerObj)
            
            % call superclass constructor
            obj@EncodingModelObj(forwardModelSTRING, cartesianSamplingflag);
            obj@OptParamsObj(optParamsStruct);

            obj.regularizer = regularizerObj;
            if obj.cartesianFlag == true
                obj.alpha = 1;
            else
                disp('need to compute alpha during prep function');
            end

        end
        
        function prep(obj,X_)
            % This function is to prepare parameter selection and proper
            % definitions prior to running FISTA.
            
            % PREP REGULARIZER IN OPT
            % change here
            if obj.regularizer.hackWavelets
                X_ = obj.regularizer.psi * X_;
                
                switch obj.regularizer.preEncodingTraits{3}
                    case 'l2-norm'
                        X_ = obj.regularizer.psi.RSOS(X_);
                    case 'stack'
                        X_ = obj.regularizer.psi.stack(X_);
                end
                obj.regularizer.determineRegWeighting(abs(X_));
            else
                obj.regularizer.determineRegWeighting(abs(obj.regularizer.preEncoding * X_));
            end
           %obj.regularizer.determineRegWeighting(X_); % change here
            obj.prepFlag = true;
            % 
            obj.setreg(obj.regularizer.regularizationWeighting);
        end

        function [Reconstruction,optParams] = run(obj,Y_,X_)
            % Determines parameters
            if ~obj.prepFlag
                obj.prep(X_);
            end
            % Determines Lambda

            % runs monotone FISTA
            %-> def monotone: if the error increases for increasing the
            %                 alpha parameter, then return alpha to starting point.
            %                 alpha = 1 (For Cartesian).

            % FISTA
            a = obj.alpha;
            if obj.GPUFlag
                yn = gpuArray(X_);
                Y_ = gpuArray(Y_);
            else
                yn = X_;
            end
            cost_function = 100000000;
            
            vec = @(x_) x_(:);
            convergenceRate = zeros(obj.maxIterations,1);
            relativeError = zeros(obj.maxIterations,1);
            fcost = zeros(obj.maxIterations,1);
            gcost = zeros(obj.maxIterations,1);

            %%% Testing reweighting vector
%             reweight= ones(1,obj.maxIterations);
%             rwfunc = @(num) num*.95;
%             for i = 5 : obj.maxIterations
%                 reweight(i) = rwfunc(reweight(i-1));
%             end
            
            flag = 1;
            it = 1;
            pastconvrate = 1;
            itrelerror = 0;
            while flag
                
                % residue
                r = obj.inverseModel(Y_ - obj.forwardModel(yn));

                % compute cost functions
                dataConsistencyCost = norm(vec(r))^2; 
                fcost(it) = dataConsistencyCost;
                regularizerCost     = obj.regularizer.costFunction(yn);
                gcost(it) = regularizerCost;

                cost_function = dataConsistencyCost+regularizerCost;
                convergenceRate(it) = cost_function;

                yn = obj.regularizer.proximalOperation(yn + a*(r),a);

                %obj.regularizer.regularizationWeighting = obj.regularizer.regularizationWeighting*reweight(it);
                %if mod(it,10) == 0
                %    obj.prep(yn);
                %end
%                 if mod(it,10) == 0
%                     obj.regularizer.regularizationWeighting = obj.regularizer.regularizationWeighting*.95;
%                 end
%                 if it == 5
%                    obj.regularizer.regularizationWeighting = obj.regularizer.regularizationWeighting*.63; 
%                 end
                relativeError(it) = itrelerror;
                itrelerror = (abs(pastconvrate-convergenceRate(it))/convergenceRate(it));
                flag = (it < obj.maxIterations) && ( itrelerror > obj.tolerance );
                pastconvrate = convergenceRate(it);
                it = it + 1;
            end
            it = it - 1;

            if obj.kspaceFilter == 1
                % remove corners from k-space using an elliptical mask
                % generated by MaskingObj.
                if numel(size(yn)) >= 3
                    % for 3D data and maybe multi-contrast
                    kSuppress = MaskingObj(squeeze(size(yn(:,:,:,1))),'k-space');
                    for i = 1 : size(yn,4)
                        yn(:,:,:,i) = kSuppress * yn(:,:,:,i);
                    end
                elseif (size(yn,3) < 32) && (size(yn,3) > 1) 
                    % 2D multi contrast
                    kSuppress = MaskingObj(squeeze(size(yn(:,:,1))),'k-space');
                    for i = 1 : size(yn,3)
                        yn(:,:,i) = kSuppress * yn(:,:,i);
                    end
                else
                    % 2D
                    kSuppress = MaskingObj(size(yn),'k-space');
                    yn = kSuppress * yn;
                end
            end
            
            if obj.GPUFlag()
                Reconstruction = gather(yn);
            else
                Reconstruction = yn;
            end

            obj.setFromOpt('convergence',convergenceRate);
            obj.setFromOpt('relativeError',relativeError);
            obj.setFromOpt('fcost',fcost);
            obj.setFromOpt('gcost',gcost);
            obj.setFromOpt('iterationsRatio',[it,obj.maxIterations,(it/obj.maxIterations)])
            obj.setFromOpt('lambdaDet',obj.regularizer.getLambdaDet());

            obj.lastReconstruction = Reconstruction;
            obj.OptSuccess = true;
            obj.Iterations = obj.maxIterations;
            
            %obj.RelativeError = norm(vec(obj.A(Reconstruction))-Y_(:))^2;
            % to call again prep function
            obj.prepFlag = false;
            optParams = obj.getOpts();
        end
        
        function outputArg = save(obj,filename)
        end

    end
end
