classdef mFISTAObj < ISTAObj
    %MFISTA is a class that changes the optimization procedure from ISTA
    
    properties
%         prepFlag = false;
%         regularizer
    end
    
    methods
        function obj = mFISTAObj(forwardModelSTRING, cartesianSamplingflag , optParamsStruct,regularizerObj)
            
            % call superclass constructor
            obj@ISTAObj(forwardModelSTRING, cartesianSamplingflag, optParamsStruct, regularizerObj);

%             obj.regularizer = regularizerObj;
%             if obj.cartesianFlag == true
%                 obj.alpha = 1;
%             else
%                 disp('need to compute alpha during prep function');
%             end

        end
        
%         function prep(obj,X_)
%             % This function is to prepare parameter selection and proper
%             % definitions prior to running FISTA.
%             
%             % PREP REGULARIZER IN OPT
%             % change here
%             obj.regularizer.determineRegWeighting(abs(obj.regularizer.preEncoding * X_));
%             %obj.regularizer.determineRegWeighting(X_); % change here
%             obj.prepFlag = true;
%             % 
%             obj.setreg(obj.regularizer.regularizationWeighting);
%         end

        function [Reconstruction,optParams] = run(obj,Y_,X_)
            % Determines parameters
            if ~obj.prepFlag
                obj.prep(X_);
            end
            % Determines Lambda

            % runs monotone FISTA
            %-> def monotone: if the error increases for increasing the
            %                 alpha parameter, then return alpha to starting point.
            %                 alpha = 1 (For Cartesian).

            % FISTA
            % a = obj.alpha;
            a = 1;
            cost_function = 100000000;
            yn = X_;
            vec = @(x_) x_(:);
            convergenceRate = zeros(obj.maxIterations,1);
            fcost = zeros(obj.maxIterations,1);
            gcost = zeros(obj.maxIterations,1);
            alphait = zeros(obj.maxIterations,1);
            flag = 1;
            it = 1;
            pastconvrate = 1;
            itrelerror = 0;
            while flag
            %for it = 1 : obj.maxIterations
                
                % residue
                r = obj.inverseModel(Y_ - obj.forwardModel(yn));

                % compute cost functions
                dataConsistencyCost = norm(vec(r))^2; 
                fcost(it) = dataConsistencyCost;
                regularizerCost     = obj.regularizer.costFunction(yn);
                gcost(it) = regularizerCost;

                if dataConsistencyCost+regularizerCost > cost_function
                    a = 1;
                end
                alphait(it) = a;
                %a = 1;
                cost_function = dataConsistencyCost+regularizerCost;
                convergenceRate(it) = cost_function;

                xn = obj.regularizer.proximalOperation(yn + a*(r),a);
                temp_a = a;
                a = (1 + sqrt(1+4*a^2)) / 2;
                yn = xn + ((temp_a - 1) / a) * (xn - yn);
                
                relativeError(it) = itrelerror;
                itrelerror = (abs(pastconvrate-convergenceRate(it))/convergenceRate(it));
                flag = (it < obj.maxIterations) && ( itrelerror > obj.tolerance );
                pastconvrate = convergenceRate(it);
                it = it + 1;
            end
            it = it - 1;

            if obj.kspaceFilter == 1
                % remove corners from k-space 
                kSuppress = MaskingObj(size(yn),'k-space');
                yn = kSuppress * yn;
            end
            if obj.GPUFlag()
                Reconstruction = gather(yn);
            else
                Reconstruction = yn;
            end
            obj.setFromOpt('convergence',convergenceRate);
            obj.setFromOpt('relativeError',relativeError);            
            obj.setFromOpt('convergence',convergenceRate);
            obj.setFromOpt('fcost',fcost);
            obj.setFromOpt('gcost',gcost);
            obj.setFromOpt('alpha',alphait);
            obj.setFromOpt('iterationsRatio',[it,obj.maxIterations,(it/obj.maxIterations)])
            obj.setFromOpt('lambdaDet',obj.regularizer.getLambdaDet());

            obj.lastReconstruction = Reconstruction;
            obj.OptSuccess = true;
            obj.Iterations = obj.maxIterations;
            
            obj.relativeError = norm(vec(obj.A(Reconstruction))-Y_(:))^2;
            % to call again prep function
            obj.prepFlag = false;
            optParams = obj.getOpts();
        end
        
        function outputArg = save(obj,filename)
        end

    end
end
