classdef OptParamsObj < handle
    %OPTPARAMSOBJ is a superclass defined to handle all optimization
    %parameters for FISTA (and derivatives), LQSR and CGNE
    %it also helps to visualize results and save last recon
    
    properties
        % property name % restrictions                  % default
        %printFlag (1,1)                                 = 0;
        %saveFlag  (1,1)                                 = 0;
        
        y                                               = [];
        x                                               = [];

        kspaceFilter                                    = [];
         
        maxIterations (1,1) {mustBeInteger}             = 50;
        tolerance (1,1) {mustBePositive}                = 1e-6;

        % last performed optimization
        lambdaDet                                       = [];
        lastReconstruction                              = [];
        OptSuccess                                      = [];
        Iterations                                      = [];
        iterationsRatio                                 = [];
        relativeError                                   = [];
        regularizationWeightings                        = [];
        convergence                                     = [];
        fcost                                           = [];
        gcost                                           = [];
        alpha                                           = [];
        
    end
    
    methods (Access = public)
        function obj = OptParamsObj(Params)
            %OPTPARAMSOBJ Construct an instance of this class
            %   Detailed explanation goes here
            
            p = properties(obj);
            print = 0;
            if isfield(Params,'printFlag')
                if Params.prinFlag == 1
                    print = 1;
                end
            end
            for i = 1 : length(p)
               if isfield(Params,p{i})
                   eval(strcat('obj.',p{i},' = Params.',p{i},';'));
               else
                   if print
                       disp(['no definition for property ',p{i}]);
                   end
               end
            end

        end
        function definey(obj,y)
            obj.y = y;

            %%% NEED TO THINK THE BEST PLACE FOR THIS!
            %%% YOU CAN COMPUTE IT ONCE ONLY

%             % detect Elliptical Scanning to generate k-space frequency mask
%             % Assumption: CS and LS do not extrapolate information
%             % need to move this function in case that LORAKS gets
%             % implemented in here.
%             ss = size(y);
%             mid = ceil((ss+1)/2);
%             switch (numel(ss)-1) % coils
%                 case 1
%                     obj.kspacefilter = ones(ss);
%                 case 2
%                     obj.defineKspaceFilter(y);
%                 case 3
%                     obj.defineKspaceFilter(y(mid(1),:,:,1));
%                 otherwise
%                     obj.defineKspaceFilter(y(mid(1),:,:,1,1));
%             end
            
        end
        function definex(obj,x)
            obj.x = x;
        end
        function res = getx(obj)
            res = obj.x;
        end
        function res = gety(obj)
            res = obj.y;
        end
        function setTolerance(obj,val)
            obj.tolerance = val;
        end
        function setMaxIterations(obj,val)
            obj.maxIterations = val;
        end

    end

    methods (Access = protected)
        function setreg(obj,regw)
            obj.regularizationWeightings = regw;
        end
        function setFromOpt(obj,keyword,dataFromVar)
            switch keyword
                case 'convergence'
                    obj.convergence = dataFromVar;
                case 'fcost'
                    obj.fcost = dataFromVar;
                case 'gcost'
                    obj.gcost = dataFromVar;
                case 'alpha'
                    obj.alpha = dataFromVar;
                case 'relativeError'
                    obj.relativeError = dataFromVar;
                case 'iterationsRatio'
                    obj.iterationsRatio = dataFromVar;
                case 'lambdaDet'
                    obj.lambdaDet = dataFromVar;
            end
        end
        function res = getOpts(obj)
            res = struct;
            res.relativeError = obj.relativeError;
            res.convergence = obj.convergence;
            res.fcost = obj.fcost;
            res.gcost = obj.gcost;
            res.alpha = obj.alpha;
            res.regw = obj.regularizationWeightings;
            res.tolerance = obj.tolerance;
            res.iterations = obj.iterationsRatio;
            res.lambdaDet = obj.lambdaDet;
        end
    end

    methods (Access = private)
        
%         %%% TO FILL
%         function defineKspaceFilter(obj,ksp)
% 
% 
%         end

    end

end

