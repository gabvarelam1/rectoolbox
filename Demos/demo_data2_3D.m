clear all; close all; clc;

% make sure to run start to add all paths
%%% /ReconToolbox/start.m;

%% Data
code = 'data2';
ffm = '.mat';

load(strcat(code,ffm));
whos

% data1.mat consists of MEGRE data (ky,kz,coils,TEs) and sensitivity coil
% profiles (x,y,coils).
%%

ss = size(ksp);
mid = ceil((ss+1)/2);
vr = size(coils,4);


%% mask poisson vd
% data1 is fully-sampled so we can create a sampling pattern to test cs
% performance given a sampling pattern. 

[mask] = squeeze(genPoisPattern([ss(1:3)],3,2,0,1));
%%
figure, imshow(squeeze(mask(1,:,:)),[]);
usf = numel(mask) / sum(mask(:))

fullksp = ksp;
ksp = bsxfun(@times, mask, ksp);


%% Zero-filled reconstruction
% to create the zero-filled reconstruction we define our encoding model.
% x = A^T y with A = SFC (S is fused with the sub sampled k-space);

F = FourierObj([1,2,3]);
C = CoilsObj(coils); 

imzf = C'*(F'*ksp);
reference = C'*(F'*fullksp(:,:,:,:,:));
echo = 3;
figure,
subplot 131, imshow(abs(squeeze(imzf(:,:,105,1,echo))),[]);
subplot 132, imshow(abs(squeeze(imzf(:,105,:,1,echo))),[]);
subplot 133, imshow(abs(squeeze(imzf(105,:,:,1,echo))),[]);

figure,
subplot 131, imshow(abs(squeeze(reference(:,:,105,1,echo))),[]);
subplot 132, imshow(abs(squeeze(reference(:,105,:,1,echo))),[]);
subplot 133, imshow(abs(squeeze(reference(105,:,:,1,echo))),[]);

%% use Zero-Filled reconstruction to determine L for the wavelet transform
% W2 is a wavelet transform that in 'automatic' mode, it utilizes the
% inputa data to determine a number of wavelet levels that compresses the
% data.


waveletName = 'db4';
mode = 'ppd';
W2 = WaveletObj(squeeze(imzf(:,:,:,echo)),'automatic',waveletName,mode);
L = W2.decompositionLevels;

%% Compressed Sensing
% organizer is a struct that contains the most relevant information for the
% reconstruction process, and OrganizerObj is a class that organizes the
% data given the input parameters, and performs the selected
% reconstruction.

organizer = struct;
organizer.acquisitionStyle = 'Cartesian';
organizer.startFromDatFile = 0;
organizer.reconStyle = 'CompressedSensing'; % {'CompressedSensing','LowRank','LORAKS','LowPlusSparse'}
organizer.dataReconStyle = '3D'; %{'2D','3D','m2D','p3D'} p3D = parallel 3D
organizer.solverStyle = 'ISTA'; %{'CGNE','mFISTA','LQSR'}
organizer.encodingModel = 'SFC'; %{SF_jkPF_iRMx}
organizer.regularizerOpt = 'norm1'; %{'norm1','norm2','nuclear'};
organizer.Psi = 'W'; %{'W','TV','TGV'}
organizer.PsiProperties = {L,waveletName,mode}; % #Wavelet Levels and Wavelet Name and mode


% IMPORTANT: this is the parameter most relevant. In regularized
% reconstructions, the regularization term balances the the data
% consistency term and the regularization term. Greater values promotes
% increased sparsity on your reconstruction at the expense of image
% quality.  

% Here are some examples on how to use this parameter. 

% REGULARIZATION WEIGHITING STYLE
% example 1 {'automatic','multiple','mlambdas','k-means', scaling factor};
% example 2 {'automatic','multiple','1lambdas','k-means', scaling factor};
% example 3 {'manual',[0,1e-7,....,L]}
% example 4 {'manual',[0, 1e-3*ones(1,L)]};
% example 5 {'manual',[1e-3]}
organizer.lambdaMethod = {'automatic','single','firstLevel','double k-means',1/4}; %{'double k-means','k-means','rayleigh'};

organizer.maxIterations = 600;
organizer.tolerance = 1e-6;
organizer.printFlag = 0;
organizer.kspaceFilter = 1;                               
%organizer.imageFilter = 1;                              

reconcs = OrganizerObj(organizer);

% OrganizerObj.prep prepares the data and then Organizer.run runs the
% actual reconstruction process. "inscs" and "outscs" are structures that contain information before and after the reconstruction process. 

tic
inscs     = reconcs.prep(ksp,mask,[],coils);
prepTime = toc;     % 170 secs approx

disp(inscs);

tic
outscs    = reconcs.run();   % recon all
outscs.reconTime = toc;
outscs.prepTime = prepTime;
outscs.input = organizer;
disp(outscs)

% this masks out any weird behavior out the area defined by the sensitivity
% coil maps.
M = MaskingObj(single(abs(squeeze(coils(:,:,:,1)))~=0),'image');
outscs.reconstruction = M*outscs.reconstruction;

echo = 5;
reccs = outscs.reconstruction;
figure, imshow([abs(reccs(:,:,105,echo)),abs(reference(:,:,105,echo))],[]);
figure, plot(outscs.opt{echo}.convergence)                              % objective function vs number of iterations
outscs.opt{echo}.regw                                                   % regularization weightings
norm(outscs.reconstruction(:)-reference(:)) / norm(reference(:))
%%
%% Least Squares
organizer = struct;
organizer.acquisitionStyle = 'Cartesian';
organizer.startFromDatFile = 0;
organizer.reconStyle = 'LeastSquares'; % {'CompressedSensing','LowRank','LORAKS','LowPlusSparse'}
organizer.dataReconStyle = '3D'; %{'2D','3D','m2D','p3D'} p3D = parallel 3D
organizer.solverStyle = 'LQSR'; %{'CGNE','mFISTA','LQSR'}
organizer.encodingModel = 'SFC'; %{SF_jkPF_iRMx}
organizer.maxIterations = 13;
organizer.printFlag = 0;

reconls = OrganizerObj(organizer);
%
tic
insls     = reconls.prep(ksp,mask,[],coils);
prepTime = toc;     % 170 secs approx

disp(insls);

tic
outsls    = reconls.run();   % recon all
outsls.reconTime = toc;
outsls.prepTime = prepTime;
outsls.input = organizer;

disp(outsls)

recls = outsls.reconstruction;
figure, imshow([abs(recls(:,:,105,echo)),abs(reference(:,:,105,echo))],[]);
norm(outsls.reconstruction(:)-reference(:)) / norm(reference(:))

%%
figure, imshow([abs(reccs(:,:,105,echo)),abs(reference(:,:,105,echo)),abs(recls(:,:,105,echo))],[]);