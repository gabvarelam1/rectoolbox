Documentation:
-------------
By Gabriel Varela-Mattatall 	gabriel.varela.mattatall@gmail.com

General:
-------
The Recontruction Toolbox is a package that performs different types of reconstruction problems. It provides flexibility to work with MRI data and considers several topics within reconstruction such as optimization problems, encoding models, and  regularization terms.The main optimization problem to solve is,

				\hat{x} = argming_x 1/2 ||Ax-y||_2^2 + \lambda ||\Psi x ||_1,

that is a regularized least-squares optimization but some people might connect it right away with compressed sensing. Here, \hat{x} is the reconstruction, y is the k-space data from the scanner, and A is the encoding model. ||Ax-y||_2^2 is what we know as the data consistency term, and it simply says that we want our reconstruction, x, to be similar to what we acquire in the scanner, after the operator A. || \Psi x ||_1 is the regularization term, that in this case, promotes sparsity from x in a sparsifying transform \Psi. Finally, \lambda balances the two terms.   

References:
-----------
This toolbox is based on the following references:
1- Varela-Mattatall G,Baron CA, Menon RS. Automatic determination ofthe regularization weighting for wavelet-based compressed sensing MRI reconstructions. MagnReson Med. 2021;00:1– 17. https://doi.org/10.1002/mrm.28812. 
2- Varela-Mattatall G*, Dubovan PI*, Santini T, Gilbert KM, Menon RS, Baron CA. Single-shot spiral diffusion-weighted imaging at 7T using expanded encoding with compressed sensing. Magn Reson Med. 2023;1-9. doi: 10.1002/mrm.29666.

Suggested Additional References:
3- Ong F, Cheng JY, Lustig M. General phase regularized reconstruc-tion using phase cycling. Magn Reson Med. 2018;80:112- 125.
4- Millard C, Hess AT, Mailh B, Tanner J. Approximate message passing with a colored aliasing model for variable density Fourier sampled images. IEEE Open J Signal Process.

   
MATLAB Dependencies of the toolbox:
----------------------------------
*** Matlab was chosen as the coding environment due to its prevalence in the MRI community and it only requires some additional libraries which are either [MANDATORY] or [OPTIONAL].
[MANDATORY]
Wavelet Toolbox
logic: Most references are based on the Wavelet transform.  
[OPTIONAL] 
Parallel Computing Toolbox
logic: 3D can be split in 2D parallel reconstructions.
Statistics and Machine Learning Toolbox [kmeans function]** -> can be replaced by assuming a Rayleigh distribution.
logic: you don't want to assume any distribution on the coefficients of the sparsifying transform, so we
       apply kmeans under the assumption that in this certain distribution we are characterizing noise/artifacts 
       vs signal and this two cluster can be separated by euclidean distance, for example. 

How to start:
-------------
Include all paths related to the toolbox. You may use the start.m function, or just type addpath(genpath('./')) in the main directory of the toolbox. 
There are 3 demos to run:
1- demo_data1:		runs 2D reconstruction.
2- demo_data2_p3D:	runs multiple 2D reconstructions in parallel
3- demo_data2_3D:	runs 3D reconstructions

Technical details:
------------------
0- vdPoisMex needs to be compiled for your Operative system (type "mex vdPoisMex.c"). 
1- The toolbox was designed as object-oriented programming. This means that objects (instances of a class) perform the operations.
2- The encoding model, A, can accept many operators, for example, A=SFC (appears in the demos), please learn how SFC operates to include your own operators.
3- All classes inside the encoding model should have the member adjoint (obj.adjoint). This means that these operators should have both a forward and an inverse operation indicated by the member adjoint.
2- All classes inside the encoding model should have the method mtimes to define those forward and inverse operations. (obj.mtimes()).

For inquiries, recommendations, and to request additional documentation, please send an email to Gabriel. 
